FROM php:7-fpm-alpine

RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN apk update && apk add nginx && mkdir -p /run/nginx

RUN ln -sf /var/log/nginx/access.log /dev/stdout
RUN ln -sf /var/log/nginx/error.log /dev/stderr

COPY ./build/nginx_config.conf /etc/nginx/conf.d/default.conf
COPY ./build/setup.sh /run/start.sh
COPY . /var/www

RUN curl -sS https://getcomposer.org/installer | php -- \
--install-dir=/usr/bin --filename=composer

WORKDIR /var/www

RUN composer install
RUN php artisan storage:link
RUN mkdir -p storage/framework/{sessions,views,cache}
RUN chown -R www-data:www-data .

EXPOSE 80

RUN chmod +x /run/start.sh

CMD [ "/run/start.sh" ]