<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, PresentableTrait, SoftDeletes;

    protected $primaryKey = 'cid';
    protected $presenter = 'App\Presenter\UserPresenter';    
    public $incrementing = false;
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cid',
        'fname',
        'lname',
        'email',
        'password',
        'rating',
        'oi',
        'bio',
        'photo',
        'atm',
        'datm',
        'ta',
        'ec',
        'fe',
        'wm',
        'zab',
        'p50app',
        'p50twr',
        'p50gnd',
        'app',
        'twr',
        'gnd',
        'mtr',
        'vis',
        'training_milestone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function news() {
    
        return $this->hasMany('App\News');
        
    }

    public function event() {
    
        return $this->hasMany('App\Event');
        
    }

    public function avail() {
    
        return $this->hasMany('App\Avail');
        
    }

    public function insAvail() {
    
        return $this->hasMany('App\InsAvail', 'ins_id');
        
    }

    public function training() {
    
        return $this->hasMany('App\Training', 'user_id')->orderBy("time_start", "desc");
        
    }

    public function activity() {
        return $this->hasMany('App\ControllerHours', 'cid', 'cid')->orderBy("time_start", "desc");
    }

    public function trainingIns() {
    
        return $this->hasMany('App\Training', 'ins_id');
        
    }

    public function scopeLike($query, $field, $value) {
    
        return $query->where($field, 'LIKE', "%$value%");
        
    }

    public static function isStaff(User $user) {
    
        if($user->atm || $user->datm || $user->ta || $user->ec || $user->fe || $user->wm) {
            return true;
        }

        return false;
        
    }

    public static function isIns(User $user) {
    
        if($user->rating == 8 || $user->rating == 10 || $user->mtr || $user->atm || $user->datm || $user->cid == 995625) {
            return true;
        }

        return false;
    }
}
