<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class File extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    protected $fillable = [
        'name',
        'description',
        'location',
        'user_cid',
        'category',
        'sort'
    ];

    public function category() {
    
    	return $this->belongsTo('App\FileCategory', 'category');
    	
    }
    
    public function user() {
    
    	return $this->belongsTo('App\User', 'user_cid')->withTrashed();
    	
    }
    
    ///Hook into deleting event to delete file on disk when deleting DB record.
    protected static function boot() {
        parent::boot();
        
        static::deleting(function($file) {
            Storage::delete('public/'.$file->location);
        });
    }
}
