<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Avail extends Model
{

    use SoftDeletes;

	protected $table = 'avail';

    protected $fillable = [
        'user_id',
        'time_start',
        'time_end',
        'milestone_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'time_start',
        'time_end'
    ];

	public function user() {
	
		return $this->belongsTo('App\User', 'user_id', 'cid')->withTrashed();
		
	}

    public function milestone() {

        return $this->belongsTo('App\TrainingMilestone');

    }
}
