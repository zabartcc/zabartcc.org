<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileCategory extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
    	'sort',
    	'name',
    	'access',
    ];
    
    public function file() {
    
        return $this->hasMany('App\File', 'category')->orderBy('sort', 'asc');
        
	}
	
    public function document() {
    
        return $this->hasMany('App\Document', 'category')->orderBy('sort', 'asc');
        
    }
    
    ///Hook into deleting event to delete related files when deleting a category.
    protected static function boot() {
        parent::boot();
        
        static::deleting(function($category) {
            $category->file()->delete();
            $category->document()->delete();
        });
    }
}
