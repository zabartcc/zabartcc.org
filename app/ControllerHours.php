<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ControllerHours extends Model
{
    protected $table = 'controller_hours';
    public $timestamps = false;

    protected $guarded = [];

    protected $dates = [
        'time_start',
        'time_end',
    ];

     public function user() {
    	$this->belongsTo('\App\User', 'cid');
    }   
}
