<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class PilotsOnline extends Model
{
    use PresentableTrait;
    protected $table = 'pilots_online';
    public $timestamps = false;
    protected $presenter = 'App\Presenter\UserPresenter';
    
    
    public function origin() {
        return $this->belongsTo('\App\Airport', 'dep', 'icao');
    }

    public function destination() {
        return $this->belongsTo('\App\Airport', 'dest', 'icao');
    }
}
