<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $fillable = [
        'title',
        'body',
        'user_cid'
    ];

    public function user() {
    
    	return $this->belongsTo('App\User', 'user_cid')->withTrashed();
    	
    }
}
