<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Training extends Model
{
    use PresentableTrait, SoftDeletes;

	protected $table = 'training';
    protected $presenter = 'App\Presenter\UserPresenter';  
	// protected $fillable = [
 //        'time_start',
 //        'time_end',
 //        'ins_id',
 //        'user_id',
 //        'position'
 //    ];

    protected $guarded = [];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'time_start',
        'time_end'
    ];

    public function user() {
    
        return $this->belongsTo('App\User', 'user_id')->withTrashed();
        
    }

    public function ins() {
    
        return $this->belongsTo('App\User', 'ins_id')->withTrashed();
        
    }

    public function milestone() {
        return $this->belongsTo('App\TrainingMilestone');
    }
}
