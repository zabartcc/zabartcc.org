<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'content',
        'time_start',
        'time_end',
        'image',
        'user_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'time_start',
        'time_end'
    ];

    public function user() {
    
    	return $this->belongsTo('App\User', 'user_id')->withTrashed();
    	
    }
}
