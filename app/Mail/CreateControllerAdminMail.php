<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateControllerAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    public $cid;
    public $rating;
    public $fname;
    public $lname;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cid, $rating, $fname, $lname, $email)
    {
        $this->cid = $cid;
        $this->rating = $this->textRating($rating);
        $this->fname = $fname;
        $this->lname = $lname;
        $this->email = $email;
    }

    public function textRating($rating) {
    
        $ratings = ['UNK', 'OBS', 'S1', 'S2', 'S3', 'C1', 'C2', 'C3', 'I1', 'I2', 'I3', 'SUP', 'ADM'];

        return $ratings[$rating];
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('New Member at ZAB - '.$this->fname.' '.$this->lname);
        return $this->view('emails.newUserAdmin');
    }
}
