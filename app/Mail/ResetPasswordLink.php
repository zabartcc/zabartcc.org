<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordLink extends Mailable
{
    use Queueable, SerializesModels;

    public $token;
    public $fname;
    public $lname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $fname, $lname)
    {
        $this->token = $token;
        $this->fname = $fname;
        $this->lname = $lname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Albuquerque ARTCC Password Reset - '.$this->fname.' '.$this->lname);
        return $this->view('emails.resetlink');
    }
}
