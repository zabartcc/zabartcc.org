<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApprovedFeedback extends Mailable
{
	use Queueable, SerializesModels;
	
	public $feedback;
	public $submitter_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Feedback $feedback)
    {
		$this->feedback = $feedback;
		$this->submitter_name = $this->feedback->anonymous ? 'Anonymous' : $this->feedback->submitter_name;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		$this->subject('New Feedback Received');
        return $this->view('emails.approvedFeedback');
    }
}
