<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvailTakenMail extends Mailable
{
    use Queueable, SerializesModels;

    public $fname;
    public $lname;
    public $session_start;
    public $session_end;
    public $ins_name;
    public $milestone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fname, $lname, $session_start, $session_end, $ins_name, $milestone)
    {
        $this->fname = $fname;
        $this->lname = $lname;
        $this->session_start = $session_start;
        $this->session_end = $session_end;
        $this->ins_name = $ins_name;
        $this->milestone = $milestone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->cc('ta@zabartcc.org');
        $this->subject('Session Request Taken - '.$this->milestone);
        return $this->view('emails.availtaken');
    }
}
