<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateAvailMail extends Mailable
{
    use Queueable, SerializesModels;

    public $student;
    public $time_start;
    public $time_end;
    public $milestone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\User $student, $time_start, $time_end, \App\TrainingMilestone $milestone)
    {
        $this->student = $student;
        $this->time_start = $time_start;
        $this->time_end = $time_end;
        $this->milestone = $milestone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("New Training Request - ".$this->student->lname.", ".$this->student->fname." - ".$this->milestone->name);
        return $this->view('emails.newAvail');
    }
}
