<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $newpass;
    public $fname;
    public $lname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($newpass, $fname, $lname)
    {
        $this->newpass = $newpass;
        $this->fname = $fname;
        $this->lname = $lname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('New Albuquerque ARTCC Password - '.$this->fname.' '.$this->lname);
        return $this->view('emails.newpass');
    }
}
