<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateControllerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $cid;
    public $password;
    public $fname;
    public $lname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($cid, $password, $fname, $lname)
    {
        $this->cid = $cid;
        $this->password = $password;
        $this->fname = $fname;
        $this->lname = $lname;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Albuquerque ARTCC Login Details - '.$this->fname.' '.$this->lname);
        return $this->view('emails.newUser');
    }
}
