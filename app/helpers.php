<?php

use App\User;

function sec2hms ($sec) {
	$hms = "";
    $hours = intval(intval($sec) / 3600); 
    $hms .= str_pad($hours, 2, "0", STR_PAD_LEFT). ":";
    $minutes = intval(($sec / 60) % 60); 
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";
    $minutes. ":";
    $seconds = intval($sec % 60); 
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT). "";
    $seconds. "";
    return $hms;
    
  }

function ca_sec2hm ($sec, $zero = false) {
	$hms = "";
    $hours = intval(intval($sec) / 3600); 
    $hms .= str_pad($hours, 2, "0", STR_PAD_LEFT). ":";
    $minutes = intval(($sec / 60) % 60); 
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT);
    if(!$zero && !$sec) {
        return '';
    }
    return $hms;    
  }



function nameOrCid($item, $rel='user', $col='user_id') {

    if(!$item->$rel) {
        return $item->$col;
    }
    else {
        return $item->$rel->fname." ".$item->$rel->lname;
    }
}

function nameByCid($cid) {
	$user = \App\User::find($cid);

	if(!$user) {
		return $cid;
	}
	else {
		return $user->fname." ".$user->lname;
	}
}

function br2nl( $input ) {
    return preg_replace('/<br\s?\/?>/ius', "\n", str_replace("\n","",str_replace("\r","", htmlspecialchars_decode($input))));
}

function shortPos2Long ($pos) {

    $airports = array(
    "PHX" => "Phoenix", 
    "ABQ" => "Albuquerque", 
    "TUS" => "Tucson", 
    "AMA" => "Amarillo", 
    "ROW" => "Roswell", 
    "ELP" => "El Paso", 
    "SDL" => "Scottsdale", 
    "CHD" => "Chandler", 
    "FFZ" => "Falcon", 
    "IWA" => "Gateway", 
    "DVT" => "Deer Valley", 
    "GEU" => "Glendale", 
    "GYR" => "Goodyear", 
    "LUF" => "Luke", 
    "RYN" => "Ryan", 
    "DMA" => "Davis-Monthan",
    "FLG" => "Flagstaff", 
    "PRC" => "Prescott", 
    "AEG" => "Double Eagle", 
    "BIF" => "Biggs", 
    "HMN" => "Holoman", 
    "SAF" => "Santa Fe",
    "FHU" => "Libby");

    $posArray = [
        'DEL' => 'Delivery',
        'GND' => 'Ground',
        'TWR' => 'Tower',
        'DEP' => 'Departure',
        'APP' => 'Approach',
        'CTR' => 'Center'
    ];

    $postemp = explode('_', $pos);

    $pos1 = $airports[$postemp[0]];
    $pos2 = $posArray[$postemp[1]];

    return $pos1." ".$pos2;

}

function getRemoteIPAddress() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];

    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    return $_SERVER['REMOTE_ADDR'];
}


?>