<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsAvail extends Model
{

    use SoftDeletes;

	protected $table = 'insavail';
	protected $fillable = [
        'time_start',
        'time_end',
        'ins_id',
        'session_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'time_start',
        'time_end'
    ];

    public function user() {
    
    	return $this->belongsTo('App\User', 'ins_id');
    	
    }

    public function training() {
    
        return $this->hasOne('App\Training', 'id', 'session_id');
        
    }
}
