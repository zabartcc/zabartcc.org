<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordToken extends Model
{
    use SoftDeletes;
    public $timestamps = false;

    protected $fillable = [
        'cid',
        'token',
        'deleted_at',
        'expire',
    ];

    public function user() {
    
    	return $this->belongsTo('App\User', 'cid');
    	
    }
}
