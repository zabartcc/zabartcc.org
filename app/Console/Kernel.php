<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Http\Controllers\CronController@getDataFile')->cron('*/2 * * * *');
        $schedule->call('App\Http\Controllers\CronController@updateControllersOnline')->cron('*/2 * * * *');
        $schedule->call('App\Http\Controllers\CronController@updatePilotsOnline')->cron('*/2 * * * *');
        $schedule->call('App\Http\Controllers\CronController@archiveEvents')->cron('0 */1 * * *');
        $schedule->call('App\Http\Controllers\CronController@getEntries')->cron('*/2 * * * *');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
