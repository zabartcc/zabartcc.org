<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckStaff
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!$user) {
            return redirect('/');
        }

        if($user->atm || $user->datm || $user->ta || $user->ec || $user->fe || $user->wm) {
            return $next($request);
        }

        return redirect('/');
    }
}
