<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIns
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(!$user) {
            return redirect('/');
        }

        if($user->rating == 8 || $user->rating == 10 || $user->mtr || $user->atm || $user->datm || $user->cid == 995625) {
            return $next($request);
        }

        return redirect('/');
    }
}
