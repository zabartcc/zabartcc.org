<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentController extends Controller
{
	public function index() {
    
		$category = \App\FileCategory::where('access', 1)->withCount('document')->get();

		return view('documents.index', compact('category'));
		
	}

	public function show($document) {
		$doc = \App\Document::where('uri_slug', $document)->first();

		$pd = new \ParsedownExtra();

		$doc_content = $pd->text($doc->content);

		return view('documents.show', compact('doc', 'doc_content'));
	}
}
