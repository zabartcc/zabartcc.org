<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\FileCategory;
use Validator;

use Illuminate\Support\Facades\Log;
use Auth;

class AdminFilesController extends Controller
{

    public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

    public function index() {
        
        $files = File::all();
        $categories = FileCategory::orderBy('name', 'ASC')->get();
        return view('admin.file.index')->with(compact('files', 'categories'));
    }
    
    public function store(Request $request) {
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'file' => 'required|file',
            ],
            [
            'name.required' => 'You did not enter a Name.',
            'description.required' => 'You did not enter a Description.',
            'category.required' => 'You did not specify a category for the file.',
            'file.required' => 'You did not select a file.',
            'file.file' => 'The file you specified was not recognized as valid.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/files')
            ->withInput()
            ->withErrors($valid);
        }

        $file = $request->file->store('public/files');
        $file = str_replace('public/', '', $file);

        $filetmp = File::create([
            'location' => $file,
            'name' => $request->name,
            'description' => $request->description,
            'user_cid' => Auth::user()->cid,
            'category' => $request->category,
            ]);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created file '.$request->name);
        
        $request->session()->flash('success', 'File '.$request->name.' was successfully uploaded!');
    
        return back();
    }
    
    public function replaceFile(Request $request, $id) {
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'file' => 'required|file|max:500000',
            ],
            [
            'file.required' => 'You did not select a file.',
            'file.file' => 'The file you specified was not recognized as valid.'
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/files/edit/'.$id)
            ->withErrors($valid);
        }

        $file = $request->file->store('public/files');
        $file = str_replace('public/', '', $file);

        $filetmp = File::find($id);

        $filetmp->location = $file;

        $filetmp->save();

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' replaced the file for '.$filetmp->name);
        
        $request->session()->flash('success', 'File successfully updated.');

        return redirect()->to('/admin/files/edit/'.$id);
    }

    public function delete(Request $request) {
    
        $file = File::find($request->fileid);

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted file '.$file->name);
        
        $file->delete();

        return back();
        
    }

    public function edit($id) {

        $file = File::find($id);

        return view('admin.file.edit')->with(compact('file'));
    }

    public function update(Request $request, $id) {

        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            ],
            [
            'name.required' => 'You did not enter a Name.',
            'description.required' => 'You did not enter a Description.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/files/edit/'.$id)
            ->withErrors($valid);
        }

        $file = File::find($id);

        $file->name = $request->name;
        $file->description = $request->description;
        $file->timestamps = false;
        $file->save();
        $file->timestamps = true;

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated file '.$request->name);
        
        $request->session()->flash('success', 'File '.$request->name.' was successfully updated!');

        return back();
    }
}
