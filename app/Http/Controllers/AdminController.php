<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Validator;
use App\Http\Requests;
use App\User;

class AdminController extends Controller
{
	public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }
    public function index() {
    	
    	return view('admin.index');    	
    
    }
}
