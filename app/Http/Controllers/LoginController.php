<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use App\PasswordToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Log;
use App\Mail\ResetPasswordLink;
use App\Mail\SendNewPassword;

class LoginController extends Controller
{
    public function __construct()
    {
        //Require user to be guest to access all methods on this controller except what is defined here
        $this->middleware('guest', ['except' => ['logout', 'forumsSso']]);
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request) {
        //Create validator and set rules
        $valid = Validator::make($request->all(), [
            'cid' => 'required',
            'password' => 'required'
            ],
            [
            'cid.required' => 'You did not enter a Controller ID',
            'password.required' => 'You did not enter a password'
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/login')
            ->withInput()
            ->withErrors($valid);
        }

        //Attempt to log in the user
        $auth = Auth::attempt(['cid' => $request->input('cid'), 'password' => $request->input('password')], true);
        
        //If Auth fails
        if(!$auth) {
            return redirect()->to('/login')
            ->withInput()
            ->withErrors([
            'cid' => 'The Controller ID/Password you entered do not match.',
            'password' => 'The Controller ID/Password you entered do not match.'
            ]);
        }
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' logged in');
        
        //If auth is successful and user logged in via navbar, redirect to previous page
        if($request->input('url'))
            return redirect()->to($request->input('url'));
        
        //If auth is successful and user logged in from /login, redirect to user dashboard
        return redirect()->to('/dashboard');
    }

    public function logout() {
        //call logut on Auth facade to log user out, redirect to home
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' logged out');
        
        Auth::logout();
        return redirect()->to('/');
    }

    public function forgotpass() {
    
        return view('auth.forgotpass');
        
    }

    public function generateResetToken(Request $request) {
    
        $user = User::find($request->cid);

        if(!$user) {
            $request->session()->flash('success', 'An email has been dispatched to the email associated with the CID you entered.');
            return back();
        }
        
        $token = str_random(24);

        $oldTokens = PasswordToken::where('cid', $request->cid)->delete();

        $reset = PasswordToken::create([
                'cid' => $request->cid,
                'token' => $token,
                'expire' => time()+3600,
            ]);

        Mail::to($user->email)->send(new ResetPasswordLink($token, $user->fname, $user->lname));

        $request->session()->flash('success', 'An email has been dispatched to the email associated with the CID you entered.');

        return back();

    }

    public function sendNewPassword(Request $request, $token) {

        $reset = PasswordToken::where('token', $token)->first();

        if(!$reset) {
            return redirect()->to('/forgotpass')
            ->withErrors([
                'Invalid reset token.',
            ]);
        }

        $newpass = str_random(16);
        $newpasshash = bcrypt($newpass);

        Mail::to($reset->user->email)->send(new SendNewPassword($newpass, $reset->user->fname, $reset->user->lname));

        $user = User::find($reset->cid)
            ->update([
                'password' => $newpasshash,
                ]);

        $reset->delete();
    
        $request->session()->flash('success', 'Your password was successfully reset. Please check your email for your new password.');

        return redirect()->to('/login');
        
    }

    public function forumsSso() {
        $sso = new \Cviebrock\DiscoursePHP\SSOHelper();

        // this should be the same in your code and in your Discourse settings:
        $secret = 'jWgx8F4MMwgFjJJxkY0Z';
        $sso->setSecret( $secret );

        // load the payload passed in by Discourse
        $payload = $_GET['sso'];
        $signature = $_GET['sig'];

        // validate the payload
        if (!($sso->validatePayload($payload,$signature))) {
            // invaild, deny
            header("HTTP/1.1 403 Forbidden");
            echo("Bad SSO request");
            die();
        }

        $nonce = $sso->getNonce($payload);

        // dd(Auth::guest());

        if(Auth::guest()) {
            return redirect('/login')->withErrors([
                'You must be logged into the website in order to login to the forums. Please log in here, then retry your forum login.',
            ]);
        } else {

            $user = Auth::user();

            $userId = $user->cid;
    
            // Required and must be consistent with your application
            $userEmail = $user->email;
    
            // Optional - if you don't set these, Discourse will generate suggestions
            // based on the email address
    
            $extraParameters = array(
                'username' => strtolower($user->fname)."_".strtolower($user->lname),
                'name'     => $user->fname." ".$user->lname
            );
    
            // build query string and redirect back to the Discourse site
            $query = $sso->getSignInString($nonce, $userId, $userEmail, $extraParameters);
            header('Location: http://community.zabartcc.org/session/sso_login?' . $query);
            exit(0);
        }
    }

}
