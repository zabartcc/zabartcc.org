<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\InsAvail;
use App\User;
use App\Training;
use Illuminate\Support\Facades\Log;
use Auth;

use App\Http\Requests;

class InsTrainingController extends Controller
{
	public function __construct()
    {
        //Require user to be ins to access all methods on this controller except what is defined here
        $this->middleware('ins', ['except' => '']);
    }
    
    public function last20() {
        $sessions = Training::where('completed', 1)
        					->orderBy('time_start', 'desc')
        					->with('user', 'ins')
        					->take(20)
        					->get();

        return view('ins.training.last20')->with(compact('sessions'));
    }

    public function viewSessions(User $user) {

    	$sessions = $user->training;
    
    	return view('ins.training.viewsessions')->with(compact('user','sessions'));
    	
    }
    
	public function index() {

		$trainingopen = Training::where('ins_id', '=', Auth::user()->cid)
									->where('completed', '=', 0)
									->orderby('time_start', 'desc')
									->with('user', 'ins')
									->get();
		$trainingcomplete = Training::where('ins_id', '=', Auth::user()->cid)
									->where('completed', '=', 1)
									->orderby('time_start', 'desc')
									->with('user', 'ins')
									->get();
	
		return view('ins.training.index', compact('trainingopen', 'trainingcomplete'));
		
	}

	public function details($id) {
	
		$training = Training::find($id);

		$training->notes = br2nl($training->notes);

		$related = Training::where('time_start', '=', $training->time_end)
							->orWhere('time_end', '=', $training->time_start)
							->get();

		return view('ins.training.details', compact('training', 'related'));
		
	}

	public function delete(Request $request) {
    
        $training = Training::find($request->session);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted training session '.$training->id);

        $training->delete();

        return back();
        
    }

	public function merge($id1, $id2) {

		$session1 = Training::find($id1);
		$session2 = Training::find($id2);

		$session1->time_start = (strtotime($session1->time_start) > strtotime($session2->time_start)) ? $session2->time_start : $session1->time_start;
		$session1->time_end = (strtotime($session1->time_end) < strtotime($session2->time_end)) ? $session2->time_end : $session1->time_end;

		$session1->save();
		$session2->delete();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' merged training sessions '.$id1.'|'.$id2);

		return back();

	}

	public function update($id, Request $request) {

		$training = Training::find($id);

		$training->notes = $request->notes;

		$training->save();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated training session '.$id);

		return back();

	}

	public function finalize($id, Request $request) {
	
		$training = Training::find($id);

		$training->notes = $request->notes;

		$training->completed = 1;

		$training->save();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' completed training session '.$id);

		return redirect('/ins/training');
		
	}
}
