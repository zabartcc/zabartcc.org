<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Log;
use Auth;

class AdminDocumentController extends Controller
{
	public function __construct() {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

    public function index() {
        
        $categories = \App\FileCategory::orderBy('name', 'ASC')->withCount('document')->get();
        return view('admin.document.index')->with(compact('categories'));
    }
    
    public function store(Request $request) {
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'content' => 'required',
            ],
            [
            'name.required' => 'You did not enter a Name.',
            'description.required' => 'You did not enter a Description.',
            'category.required' => 'You did not specify a category for the document.',
            'file.required' => 'You did not specify any content.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/documents')
            ->withInput()
            ->withErrors($valid);
		}

		$filetmp = \App\Document::create([
			'name' => $request->name,
			'uri_slug' => str_replace([' ', '_'], ['-', '-'], strtolower($request->name)),
            'description' => $request->description,
            'content' => $request->content,
            'last_update_user' => Auth::user()->cid,
            'category' => $request->category,
            ]);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created document '.$request->name);
        
        $request->session()->flash('success', 'Document '.$request->name.' was successfully created!');
    
        return back();
    }
    
    public function delete(Request $request) {
    
        $document = \App\Document::find($request->fileid);

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted document '.$document->name);
        
        $document->delete();

        return back();
        
    }

    public function edit($document) {

		$doc = \App\Document::where('uri_slug', $document)->first();

        return view('admin.document.edit')->with(compact('doc'));
    }

	public function update(Request $request, \App\Document $document) {

        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'content' => 'required',
            ],
            [
            'name.required' => 'You did not enter a Name.',
            'description.required' => 'You did not enter a Description.',
            'content.required' => 'You did not enter any Content.',
            ]);
		
        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/documents/edit/'.$document->uri_slug)
            ->withErrors($valid);
        }

		if($request->name !== $document->name) { // regenerate slug
			$document->uri_slug = str_replace([' ', '_'], ['-', '-'], strtolower($request->name));
		}

        $document->name = $request->name;
        $document->description = $request->description;
        $document->content = $request->content;
        $document->save();

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated document '.$request->name);
        
        $request->session()->flash('success', 'Document '.$request->name.' was successfully updated!');

        return redirect()->to('/admin/documents/edit/'.$document->uri_slug);
    }
}
