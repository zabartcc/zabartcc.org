<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Http\Requests;
use Auth;
use Log;

class InsControllersController extends Controller
{

	public function __construct()
    {
        //Require user to be ins to access all methods on this controller except what is defined here
        $this->middleware('ins', ['except' => '']);
    }

    public function search() {
    
        return view('ins.controller.search');
        
    }

    public function searchPost(Request $request) {

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'search' => 'required'],
            [
            'search.required' => 'You did not enter a search term.',]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/ins/controllers/search')
            ->withErrors($valid);
        }

        return redirect('ins/controllers/search/'.$request->search) ;
        
    }

    public function searchResults($data) {

        $controllers = User::where('cid', 'LIKE', '%'.$data.'%')
                            ->orWhere('fname', 'LIKE', '%'.$data.'%')
                            ->orWhere('lname', 'LIKE', '%'.$data.'%')
                            ->orWhere('email', 'LIKE', '%'.$data.'%')
                            ->orWhere('oi', 'LIKE', '%'.$data.'%')
                            ->get();
    
        return view('ins.controller.search', compact('controllers'))->with('search', 1);
        
    }

    public function edit(User $user) {

        $ratings = ['Observer', 'Student 1', 'Student 2', 'Student 3', 'Controller 1', '', 'Controller 3', 'Instructor', '', 'Senior Instructor', 'Supervisor', 'Administrator'];
        $milestones = \App\TrainingMilestone::all();

        return view('ins.controller.edit', compact('user', 'ratings', 'milestones'));
        
    }

    public function update(Request $request) {

        $cid = $request->cid;

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'rating' => 'required',
            ],
            [
            'rating.required' => 'You did not enter a Rating.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->back()
            ->withErrors($valid);
        }

        $requestMod = $request->except('_token');
        $tm = 0;

        foreach ($requestMod as $key => $request1) {
            if(strpos($key, 'tm_') !== FALSE) {
                $tm = $tm | $request1;
                unset($requestMod[$key]);
            }
            elseif($request1 == -1) {
                $requestMod[$key]= '0';
            }
        }

        $requestMod['training_milestone'] = $tm;

        $controller = User::find($cid);

        $controller->update($requestMod);

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' edited controller: '.$cid);
        
        $request->session()->flash('success', 'Controller '.$request->fname.' '.$request->lname.' successfully updated.');

        return redirect('/ins/controllers/edit/'.$requestMod['cid']);
        
    }
}
