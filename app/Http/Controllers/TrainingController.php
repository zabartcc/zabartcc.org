<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Training;
use App\Avail;
use Auth;

use App\Http\Requests;

class TrainingController extends Controller
{
    public function index() {

    	$otraining = Training::where('user_id', Auth::user()->cid)->where('time_start', '>=', date('Y-m-d'))->orderby('time_start', 'asc')->get();

        $training = Training::where('user_id', '=', Auth::user()->cid)->where('completed', 1)->orderBy('time_start', 'desc')->get();

        $requests = Avail::where('user_id', '=', Auth::user()->cid)->where('deleted_at', NULL)->where('time_start', '<', date("Y-m-d H:i:s"))->orderBy('time_start', 'desc')->get();
    
    	return view('user.training.index', compact('training', 'otraining', 'requests'));
    	
    }

    public function show($id) {

    	$training = Training::find($id);

    	return view('user.training.show', compact('training'));
    }
}
