<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Feedback;
use App\User;
use Log;

class FeedbackController extends Controller
{
    public function index() {
    	$controllers = \App\User::orderBy('lname', 'asc')->get();
        return view('feedback.index')->with(compact('controllers'));
    }

    public function show(Feedback $feedback) {
    
        $ratings = ['Unknown', 'Poor', 'Below Average', 'Average', 'Above Average', 'Excellent'];

        return view('user.feedback.index')->with(compact('feedback', 'ratings'));
        
    }

    public function store(Request $request) {
    
    	//Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'scid' => 'required|integer',
            'email' => 'required|email',
            'rating' => 'required',
            'controller' => 'required',
            'comments' => 'required',
            // 'g-recaptcha-response' => 'required|recaptcha',
            ],
            [
            'fname.required' => 'You did not enter your first name.',
            'lname.required' => 'You did not enter your last name.',
            'scid.required' => 'You did not enter your CID.',
            'scid.integer' => 'The CID you entered is not valid.',
            'email.required' => 'You did not enter your email.',
            'email.email' => 'The email you entered did not look like an email.',
            'controller.required' => 'You did not choose a controller.',
            'rating.required' => 'You did not choose a rating for the controller.',
            'comments.required' => 'You did not leave any comments for the controller.',
            // 'g-recaptcha-response.required' => 'CAPTCHA verification failed. Please try again!',
            // 'g-recaptcha-response.recaptcha' => 'CAPTCHA verification failed. Please try again!',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/feedback')
            ->withInput()
            ->withErrors($valid);
        }

        if($request->anonymous == -1)
            $anon = 0;
        else
            $anon = 1;

        $feedback = Feedback::create([
                'user_cid' => $request->controller,
                'submitter_cid' => $request->scid,
                'submitter_name' => $request->fname." ".$request->lname,
                'submitter_email' => $request->email,
                'submitter_ip' => getRemoteIPAddress(),
                'rating' => $request->rating,
                'comments' => $request->comments,
                'anonymous' => $anon,
            ]);

        $controller = User::find($request->controller);
        
        Log::info('Website guest submitted feedback for '.$request->controller);
        
        $request->session()->flash('success', 'Feedback for '.$controller->fname.' '.$controller->lname.' successfully submitted. Thank you!');
        
        return back();
    	
    }
}
