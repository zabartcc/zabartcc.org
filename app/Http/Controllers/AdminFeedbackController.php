<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Feedback;
use App\User;
use Log;
use Auth;

class AdminFeedbackController extends Controller
{

   public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

   public function index() {

   		$feedback = Feedback::where('approved', 0)->get();
   		$ratings = ['Unknown', 'Poor', 'Below Average', 'Average', 'Above Average', 'Excellent'];
   	
   		return view('admin.feedback.index')->with(compact('feedback', 'ratings'));
   	
   }

   public function show(User $user) {
   
         $feedback = Feedback::where('user_cid', $user->cid)
                              ->where('approved', 1)
                              ->orderBy('created_at', 'desc')
                              ->get();
         $ratings = ['Unknown', 'Poor', 'Below Average', 'Average', 'Above Average', 'Excellent'];

         return view('admin.feedback.controller')->with(compact('feedback', 'ratings', 'user'));
      
   }

   public function approve(Feedback $feedback, Request $request) {
   
   		$feedback->approved = 1;
   		$feedback->save();

		Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' approved feedback item '.$feedback->id.' for '.$feedback->user->fname.' '.$feedback->user->lname);
		
		Mail::to($feedback->user->email)->queue(new \App\Mail\ApprovedFeedback($feedback));

   		$request->session()->flash('success', 'Feedback for '.$feedback->user->fname.' '.$feedback->user->lname.' approved.');

   		return back();
   	
   }

   public function delete(Request $request) {
   
   		$feedback = Feedback::find($request->feedbackid);

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted feedback item '.$feedback->id.' for '.$feedback->user->fname.' '.$feedback->user->lname);

   		$request->session()->flash('success', 'Feedback for '.$feedback->user->fname.' '.$feedback->user->lname.' deleted.');

   		$feedback->delete();

   		return back();

   }
}
