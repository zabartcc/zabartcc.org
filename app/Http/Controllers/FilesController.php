<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FileCategory;
use App\File;

use App\Http\Requests;

class FilesController extends Controller
{
    
    public function isPdf($path) {
        $extension = substr($path, -3);
        if($extension == 'pdf')
            return true;
        else  
            return false;
        
    }
    
    public function getExt($path) {
        $extension = substr($path, -3);
        return $extension;
        
    }
    
    public function index() {
    
    	$category = FileCategory::where('access', 1)->with('file')->orderBy('sort', 'asc')->get();

    	return view('files.index', compact('category'));
    	
    }
    
    public function get($id) {
        $file = File::find($id);
        if(!$file) {
            return "File not found!";
        }
        if($this->isPdf($file->location))
            return response()->file('storage/'.$file->location);
        else {
            $filename = str_replace(' ', '_', $file->name).'.'.$this->getExt($file->location);
            $filename = str_replace('/', '-', $filename);
            $filename = str_replace('\\', '-', $filename);
            
            return response()->download('storage/'.$file->location, $filename);
        }
    }
}
