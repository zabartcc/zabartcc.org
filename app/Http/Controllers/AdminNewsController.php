<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Illuminate\Support\Facades\Log;
use Auth;

use App\Http\Requests;
use Validator;

class AdminNewsController extends Controller
{

	public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

    public function create() {
    
    	return view('admin.news.create');

    }

    public function index() {

    	$news = News::orderBy('created_at', 'desc')->get();
    
    	return view('admin.news.index', compact('news'));
    	
    }

    public function store(Request $request) {
    
    	//Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
            ],
            [
            'title.required' => 'You did not enter a Title.',
            'body.required' => 'You did not enter any Content',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/news/create')
            ->withInput()
            ->withErrors($valid);
        }

        $user = News::create([
                'title' => $request->title,
                'body' => $request->body,
                'user_cid' => Auth::user()->cid,
            ]);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created news item '.$request->title);
        
        $request->session()->flash('success', 'News item '.$request->title.' was successfully created!');
        
        return back();
    }

    public function delete(Request $request) {
    
        $news = News::find($request->newsid);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted news item '.$news->title);
        
        $news->delete();

        return back();
        
    }

    public function edit($id) {
    
        $news = News::find($id);

        return view('admin.news.edit', compact('news'));
        
    }

    public function update(Request $request, $id) {

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
            ],
            [
            'title.required' => 'You did not enter a Title.',
            'body.required' => 'You did not enter any Content',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/news/edit/'.$id)
            ->withInput()
            ->withErrors($valid);
        }

        $item = News::find($id);

        $item->update($request->except('_token'));

        $request->session()->flash('success', 'News item '.$request->title.' successfully updated.');

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated news item '.$request->title);
        
        return redirect('/admin/news/edit/'.$id);
        
    }
}
