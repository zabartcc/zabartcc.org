<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class InstructorController extends Controller
{

	public function __construct()
    {
        //Require user to be ins to access all methods on this controller except what is defined here
        $this->middleware('ins', ['except' => '']);
    }

    public function index() {
    
    	return view('ins.index');
    	
    }
}
