<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\FileCategory;
use Illuminate\Support\Facades\Log;
use Auth;
use Validator;

class AdminCategoriesController extends Controller
{
    public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

    public function index() {

    	$categoryPublic = FileCategory::where('access', 1)->orderBy('sort', 'asc')->get();
    	$categoryIns = FileCategory::where('access', 2)->orderBy('sort', 'asc')->get();
    	$categoryAdmin = FileCategory::where('access', 3)->orderBy('sort', 'asc')->get();

    	return view('admin.category.index')->with(compact('categoryAdmin', 'categoryIns', 'categoryPublic'));
    }

    public function create(Request $request) {
        //create validator and define the rules and custom error messages
        $valid = Validator::make($request->all(),[
            'name' => 'required',
            ],
            [
            'cid.required' => 'You did not enter a name for the category.',
        ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/category')
            ->withErrors($valid);
        }

        FileCategory::create([
            'name' => $request->name,
            'access' => $request->access
            ]);

        $request->session()->flash('success', 'The category "'.$request->name.'" has been successfuly created!');

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created category: '.$request->name);
        
        return back();
        
    }

    public function delete(Request $request) {
    
        $cat = FileCategory::find($request->catid);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted category: '.$cat->name);

        $cat->delete();

        return back();
        
    }
}
