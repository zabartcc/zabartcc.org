<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Auth;
use Validator;
use App\Training;
use App\InsAvail;
use App\Avail;
use Mail;
use Illuminate\Http\Request;
use App\Mail\AvailTakenMail;
use App\Mail\CreateAvailMail;

use Carbon\Carbon;

use App\Http\Requests;

class AvailController extends Controller
{
    public function index() {
    
    	$avail = Avail::where('user_id', Auth::user()->cid)->orderby('time_start', 'asc')->get();

    	return view('user.avail.index', compact('avail'));
    	
    }

    public function create() {

        $milestones = \App\TrainingMilestone::all();
        $um = Auth::user()->training_milestone;

        // foreach($ms as $m) {
        //     if(($m->prereq & $user_milestone) && !($m->bitwise_id & $user_milestone)) echo $m->code." ".decbin($m->bitwise_id)." - ".decbin($m->prereq)."<br />";
        // }
        return view('user.avail.create', compact('milestones', 'um'));

	}
	
	public function delete(Avail $avail, Request $request) {
		Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted availability with ID '.$avail->id);
		$avail->delete();
		$request->session()->flash('success', 'Successfully deleted training request.');
		return back();
	}

    public function store(Request $request) {

        Validator::extend('unique_slot', function($attribute, $value, $parameters, $validator) {

            $avail = Avail::where('user_id', $parameters[0])->get();
            foreach($avail as $a) {
                if($value >= $a->time_start && $value < $a->time_end) {
                    return 0;
                }
            }
            return 1;
            //convert to timestamp so we can add an hour 
            // $time_start = strtotime($value);
            //Convert back to a format the database can compare to
            // $d_time_start = date('Y-m-d H:i:s',$time_start);
            // $query = Avail::where('user_id',$parameters[0])->where($attribute, '>=', $d_time_start)->where($attribute, '<', $d_time_end)->withTrashed();
            // return $query->count() == 0;          
        });


        $valid = Validator::make($request->all(), [
            'time_start' => 'required|after:now|unique_slot:'.Auth::user()->cid,
            'time_end' => 'required|after:time_start|unique_slot:'.Auth::user()->cid,
            'milestone' => 'required',
            ],
            [
            'time_start.required' => 'You did not specify a start time.',
            'time_start.after' => 'The start time must be in the future.',
            'time_start.unique_slot' => 'You already have a request during this timeslot.',
            'time_end.required' => 'You did not specify an end time.',
            'time_end.after' => "The request cannot start after it ends.",
            'time_end.unique_slot' => "You already have a request during this timeslot",
            'milestone.required' => 'You did not select a milestone.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/dashboard/avail/create')
            ->withInput()
            ->withErrors($valid);
        }

        $avail = Avail::create([
            'user_id' => Auth::user()->cid,
            'time_start' => strtotime($request->time_start),
            'time_end' => strtotime($request->time_end),
            'milestone_id' => $request->milestone,
            ]);

        // print_r($avail->milestone);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' requested training for '.$avail->milestone->name);
        
        $request->session()->flash('success', 'Training for milestone '.$avail->milestone->name.' was successfully requested!');

        Mail::to('instructors@zabartcc.org')->queue(new CreateAvailMail(Auth::user(), $request->time_start, $request->time_end, \App\TrainingMilestone::find($request->milestone)));
    
        return back();

    }

    public function take($id) {
    
    	$session = InsAvail::find($id);

    	return view('user.avail.take', compact('session'));
    	
    }

    public function convertToSession($id, Request $request) {
    
    	$avail = InsAvail::find($id);

    	//Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'position' => 'required'],
            [
            'position.required' => 'You did not enter a position.',]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->back()
            ->withErrors($valid);
        }

        //Create a training session
        $training = Training::create([
                'user_id' => Auth::user()->cid,
                'ins_id' => $request->ins,
                'time_start' => $request->time_start,
                'time_end' => $request->time_end,
                'position' => $request->position
            ]);

        //Update avail record with ID of session
        $avail->update(['session_id' => $training->id]);

        //Soft delete avail record
        $avail->delete();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' took avail '.$avail->id);

        //Flash a success message to session and return to session offers
        $request->session()->flash('success', 'Session successfully taken.');

        //Send email to INS
        Mail::to($training->ins->email)->send(new AvailTakenMail(
                        $training->ins->fname, 
                        $training->ins->lname, 
                        $request->time_start, 
                        $request->time_end, 
                        $training->user->fname.' '.$training->user->lname,
                        $request->position));

        return redirect('/dashboard/avail');
    }
}
