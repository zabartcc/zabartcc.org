<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\CreateControllerMail;
use App\Mail\CreateControllerAdminMail;
use App\Mail\CreateMassEmail;

use Validator;
use App\Http\Requests;
use App\User;
use App\ControllerHours;

use Illuminate\Support\Facades\Log;
use Auth;

class AdminControllersController extends Controller
{
    
    public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }

    public function create() {
    
    	return view('admin.controller.create');
    }

    public function search() {
    
        return view('admin.controller.search');
        
    }

    public function searchPost(Request $request) {

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'search' => 'required'],
            [
            'search.required' => 'You did not enter a search term.',]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/controllers/search')
            ->withInput()
            ->withErrors($valid);
        }

        return redirect('admin/controllers/search/'.$request->search) ;
        
    }

    public function searchResults($data) {

        $controllers = User::where('cid', 'LIKE', '%'.$data.'%')
                            ->orWhere('fname', 'LIKE', '%'.$data.'%')
                            ->orWhere('lname', 'LIKE', '%'.$data.'%')
                            ->orWhere('email', 'LIKE', '%'.$data.'%')
                            ->orWhere('oi', 'LIKE', '%'.$data.'%')
                            ->get();
    
        return view('admin.controller.search', compact('controllers'))->with('search', 1);
        
    }

    public function delete(Request $request) {
    
        $user = User::find($request->cid);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted controller '.$request->cid);

        $user->delete();

        return back();
        
    }

    public function edit($cid) {
    
        $controller = User::find($cid);

        $ratings = ['Observer', 'Student 1', 'Student 2', 'Student 3', 'Controller 1', '', 'Controller 3', 'Instructor', '', 'Senior Instructor', 'Supervisor', 'Administrator'];

        return view('admin.controller.edit', compact('controller', 'ratings'));
        
    }

    public function update(Request $request) {

        $cid = $request->cid;

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'rating' => 'required',
            'oi' => [
                'required',
                Rule::unique('users')->ignore($cid, 'cid')
                ]
            ],
            [
            'fname.required' => 'You did not enter a First Name.',
            'lname.required' => 'You did not enter a Last Name.',
            'email.required' => 'You did not enter an Email.',
            'rating.required' => 'You did not enter a Rating.',
            'oi.required' => 'You did not enter Operating Initials.',
            'oi.unique' => 'Those Operating Initals already exist.',
            'email.email' => 'The email you entered does not look like an email.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->back()
            ->withErrors($valid);
        }

        $requestMod = $request->except('_token');

        foreach ($requestMod as $key => $request1) {
            if($request1 == -1) {
                $requestMod[$key]= '0';
            }
        }

        $controller = User::find($cid);

        $controller->update($requestMod);

        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' edited controller: '.$cid);
        
        $request->session()->flash('success', 'Controller '.$request->fname.' '.$request->lname.' successfully updated.');

        return redirect('/admin/controllers/edit/'.$requestMod['cid']);
        
    }

    public function store(Request $request) {

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'cid' => 'required|unique:users|integer',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'rating' => 'required',
            'oi' => 'required|unique:users',
            ],
            [
            'cid.required' => 'You did not enter a Controller ID.',
            'cid.integer' => 'The CID you entered was not an number.',
            'fname.required' => 'You did not enter a First Name.',
            'lname.required' => 'You did not enter a Last Name.',
            'email.required' => 'You did not enter an Email.',
            'rating.required' => 'You did not enter a Rating.',
            'oi.required' => 'You did not enter Operating Initials.',
            'cid.unique' => 'That CID already exists.',
            'oi.unique' => 'Those Operating Initals already exist.',
            'email.email' => 'The email you entered does not look like an email.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/controllers/create')
            ->withInput()
            ->withErrors($valid);
        }

        //Gen temp pass for user
        $pass = str_random(16);

        //Encrypt password
        $password = bcrypt($pass);

        //Insert user into DB
        $user = User::create([
                'cid' => $request->cid,
                'fname' => $request->fname,
                'lname' => $request->lname,
                'email' => $request->email,
                'rating' => $request->rating,
                'oi' => strtoupper($request->oi),
                'password' => $password
            ]);
        
        //Send email to user
        Mail::to($request->email)->queue(new CreateControllerMail($request->cid, $pass, $request->fname, $request->lname));
        //Send email to staff
        Mail::to('staff@zabartcc.org')->queue(new CreateControllerAdminMail($request->cid, $request->rating, $request->fname, $request->lname, $request->email));
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created controller: '.$request->cid);
        
        $request->session()->flash('success', 'Controller '.$request->fname.' '.$request->lname.' was successfully created!');
        return back();
    }

    public function verifyRestore($cid) {

        $user = User::onlyTrashed()->find($cid);

        $ratings = ['Unknown', 'Observer', 'Student 1', 'Student 2', 'Student 3', 'Controller 1', '', 'Controller 3', 'Instructor', '', 'Senior Instructor', 'Supervisor', 'Administrator'];
    
        return view('admin.controller.restore')->with(compact('user', 'ratings'));
        
    }

    public function restore(Request $request, $cid) {
    
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'email' => 'required|email',
            'rating' => 'required',
            'oi' => [
                'required',
                Rule::unique('users')->ignore($cid, 'cid')
                ]
            ],
            [
            'email.required' => 'You did not enter an Email.',
            'rating.required' => 'You did not enter a Rating.',
            'oi.required' => 'You did not enter Operating Initials.',
            'oi.unique' => 'Those Operating Initals already exist.',
            'email.email' => 'The email you entered does not look like an email.',
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/controllers/restore/'.$cid)
            ->withErrors($valid);
        }

        $user = User::onlyTrashed()->find($cid);

        //Gen temp pass for user
        $password = str_random(16);

        Mail::to($request->email)->send(new CreateControllerMail($user->cid, $password, $user->fname, $user->lname));

        //Encrypt password
        $password = bcrypt($password);

        //Gen email for staff
        Mail::to('staff@zabartcc.org')->send(new CreateControllerAdminMail($user->cid, $user->rating, $user->fname, $user->lname, $user->email));

        $user->password = $password;
        $user->created_at = time();
        $user->save();

        $user->restore();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' restored controller: '.$cid);
        
        $request->session()->flash('success', 'Controller '.$request->fname.' '.$request->lname.' was successfully restored!');
        return redirect()->to('admin/controllers/create');
        
    }

    public function massEmail() {
    
        return view('admin.controller.email');
        
    }

    public function sendMassEmail(Request $request) {
    
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'subject' => 'required',
            'email' => 'required'],
            [
            'subject.required' => 'You did not enter a subject.',
            'email.required' => 'You did not enter any content.',]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/controllers/email')
            ->withInput()
            ->withErrors($valid);
        }

        $users = User::all();

        foreach($users as $u) {
            Mail::to($u->email)->queue(new CreateMassEmail($u, $request->subject, $request->email));
        }
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' sent a mass email.');

        $request->session()->flash('success', 'Mass email successfully sent.');
        return redirect()->to('admin/controllers/email');

    }

    public function getControllerActivity(User $user) {
        return view('admin.controller.sessions', compact('user'));
    }

    public function getActivity() {
        $users = User::all();
        // $activity = [];
        $chkdate = date('Y-m-d 00:00:00', time()-5184000);
        foreach($users as $u) {
            $sess = ControllerHours::selectRaw('unix_timestamp(controller_hours.time_end) - unix_timestamp(controller_hours.time_start) as duration')
                                    ->where('cid', $u->cid)
                                    ->where('controller_hours.time_start', '>', $chkdate)
                                    ->orderBy('duration', 'asc')
                                    ->get();
            $controlling_time[$u->cid]['duration'] = 0;
            $controlling_time[$u->cid]['vis'] = $u->vis;
            $controlling_time[$u->cid]['fname'] = $u->fname;
            $controlling_time[$u->cid]['lname'] = $u->lname;
            $controlling_time[$u->cid]['protected'] = ($u->protected || $u->atm || $u->datm || $u->ta || $u->ec || $u->wm || $u->fe);
            $controlling_time[$u->cid]['rating'] = $u->present()->textRating;
            $controlling_time[$u->cid]['created_at'] = $u->created_at;
            $controlling_time[$u->cid]['bad'] = 0;

            foreach($sess as $s) {
                $controlling_time[$u->cid]['duration'] += $s->duration;
            }
            if(
                $controlling_time[$u->cid]['duration'] < 7200 &&
                $u->created_at < $chkdate
            )
                $controlling_time[$u->cid]['bad'] = 1;
        }

        return view('admin.controller.activity')->with(compact('controlling_time', 'chkdate'));
    }
}
