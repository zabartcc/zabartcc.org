<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\User;

class ControllersController extends Controller
{
    public function index() {
    	//Show full controller list
	
		//Get all controllers, sorted by rating
		$controllers = User::where('vis', '=', 0)->where('honorary', 0)->orderBy('rating', 'desc')->orderBy('lname', 'asc')->orderBy('fname', 'asc')->get();
		$vcontrollers = User::where('vis', '=', 1)->where('honorary', 0)->orderBy('rating', 'desc')->orderBy('lname', 'asc')->orderBy('fname', 'asc')->get();
		$hcontrollers = User::where('honorary', 1)->orderBy('rating', 'desc')->orderBy('lname', 'asc')->orderBy('fname', 'asc')->get();

		//Show controllers.index view with the controllers data from above passed in
		return view('controllers.index', compact('controllers', 'vcontrollers', 'hcontrollers'));
		
	}

	public function emailList() {
	
		$controllers = User::all();

		$email = "";

		foreach($controllers as $c) {
			$email .= $c->email.", ";
		}

		echo $email;
		
	}

	public function ins() {
	
		$ta = User::where('ta', 1)->first();
		$ins = User::where('rating', 8)->get();
		$mtr = User::where('mtr', 1)->get();

		return view('controllers.ins')->with(compact('ta', 'ins', 'mtr'));
		
	}

	public function staff() {

		//Define staff positions, eventually move this to database
		$staffPos = array("atm" => "Air Traffic Manager", 
			"datm" => "Deputy Air Traffic Manager", 
			"ta" => "Training Administrator", 
			"ec" => "Events Coordinator", 
			"wm" => "Webmaster", 
			"fe" => "Facility Engineer");

		//Define staff duties, eventually move this to database
		$staffDuties = array(
			"atm" => array(
				"Oversees the day-to-day operations of the ARTCC.",
				"Creates, maintains and removes ARTCC policies and procedures in accordance with VATUSA and VATSIM policies.",
				"Serves as the first point of contact for inter-ARTCC issues.",
				"Maintains the ARTCC roster and handles all transfer and visiting requests."
				),
			"datm" => array(
				"Is appointed by the Air Traffic Manager.",
				"Serves as an assistant to the Air Traffic Manager.",
				"Acts as the Air Traffic Manager when necessary.",
				"Performs other duties as assigned by the Air traffic Manager."
				),
			"ta" => array(
				"Is appointed by VATUSA3, as recommended by the Air Traffic Manager.",
				"Reports to the Air Traffic Manager.",
				"Is responsible for overseeing and managing the training department and it's members.",
				"Creates, maintains and removes training policies and procedures in accordance with VATUSA and VATSIM policies.",
				"Maintains a roster of qualified instructors and mentors."
				),
			"ec" => array(
				"Is appointed by the Deputy Air Traffic Manager",
				"Reports to the Deputy Air Traffic Manager",
				"Is responsible for planning and executing ARTCC sponsered events.",
				"Coordinates with neighboring ARTCCs to provide support staffing for their events.",
				"Coordinates with virtual airlines to provide staffing for their events."
				),
			"wm" => array(
				"Is appointed by the Air Traffic Manager",
				"Reports to the Air Traffic Manager",
				"Maintains the ARTCC website and forums.",
				"Coordinates with other ARTCC staff members to allow the website to deliver content as required by that department.",
				"Maintains any voice server sponsored by the ARTCC."
				),
			"fe" => array(
				"Is appointed by the Deputy Air Traffic Manager",
				"Reports to the Deputy Air Traffic Manager",
				"Maintains all relevant tools and files used for controlling.",
				"Provides timely updates to any Sector File, vSTARS profile, vATIS profile or vERAM profile."
				),
			);

		//get associated users, eventually need to figure out how to dynamically load from data above
		$atm = User::where('atm', '=', 1)->get();
		$datm = User::where('datm', '=', 1)->get();
		$ta = User::where('ta', '=', 1)->get();
		$ec = User::where('ec', '=', 1)->get();
		$wm = User::where('wm', '=', 1)->get();
		$fe = User::where('fe', '=', 1)->get();

		//Show controllers.staff view, with all relevant data passed in
		return view('controllers.staff', compact('atm', 'datm', 'ta', 'ec', 'wm', 'fe', 'staffDuties', 'staffPos'));
		
	}

	public function show(User $user) {
        $hours = [
            'gtyear' => [
                'del' => 0,
                'gnd' => 0,
                'twr' => 0,
                'app' => 0,
                'ctr' => 0,
                'total' => 0
            ],
            'total' => [
                'del' => 0,
                'gnd' => 0,
                'twr' => 0,
                'app' => 0,
                'ctr' => 0,
                'total' => 0
            ],
            'session_count' => $user->activity->count(),
            'session_avg' => 0
        ];
        $posdef = [
            'del' => 'del',
            'gnd' => 'gnd',
            'twr' => 'twr',
            'dep' => 'app',
            'app' => 'app',
            'ctr' => 'ctr',
        ];
        $months = [];
        $today = Carbon::now();
        for($i = 1; $i <= 12; $i++) {
            $months[] = $today->copy();
            $hours[$today->format('Ym')] = [
                'del' => 0,
                'gnd' => 0,
                'twr' => 0,
                'app' => 0,
                'ctr' => 0,
                'total' => 0
            ];
            $today->subMonth();
        }   


        foreach($user->activity as $sess) {
            $ts = new Carbon($sess->time_start);
            $te = new Carbon($sess->time_end);
            $sl = $te->timestamp - $ts->timestamp;
            $sm = $ts->format('Ym');
            $posarray = explode('_', $sess->position);
            $postype = strtolower(end($posarray));
            if(array_key_exists($postype, $posdef)) {
                $postype = $posdef[strtolower(end($posarray))];
            } else {
                continue;
            }

            if(!array_key_exists($sm, $hours)) {
                $sm = 'gtyear';
            }

            $hours[$sm][$postype] += $sl;
            $hours[$sm]['total'] += $sl;
            $hours['total'][$postype] += $sl;
            $hours['total']['total'] += $sl;
        }

        if($hours['session_count']) {
            $hours['session_avg'] = (int)round($hours['total']['total']/$hours['session_count']);
        }
		return view('controllers.profile', compact('user', 'months', 'hours'));	
	}
}
