<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

class PagesController extends Controller
{
    public function index() {

        $news = News::where('created_at', '>', Carbon::now()->subMonth(3))->orderby('created_at', 'desc')->get();
        $events = Event::where('archive', 0)->orderBy('time_start', 'asc')->take(2)->get();
        $pilots = \App\PilotsOnline::all();

    	return view('pages.index')->with(compact('news', 'events', 'pilots'));
    }

    public function test() {
        
        dd(\App\Training::where('user_id', 999230)->orderBy('time_start','desc')->first()->time_start);

    }

    public function credits() {
    
    	return view('pages.credits');
    	
    }

    public function visiting() {
        return view('pages.visiting');
    }

}
