<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Auth;

use App\Http\Requests;
use App\User;
use App\Feedback;
use App\ControllerHours;

class UsersController extends Controller
{

    public function __construct()
    {
        //Require user to be logged in to access all methods on this controller except what is defined here
        $this->middleware('loggedin', ['except' => '']);
    }
   	public function index() {

      $feedback = Feedback::where('user_cid', Auth::user()->cid)
                          ->where('approved', 1)
                          ->orderBy('created_at', 'desc')
                          ->get();

      $ratings = ['Unknown', 'Poor', 'Below Average', 'Average', 'Above Average', 'Excellent'];

      $hours = ControllerHours::where('cid', Auth::user()->cid)
                          ->orderBy('time_start', 'desc')
                          ->take(10)
                          ->get();

   		return view('user.dashboard')->with(compact('feedback', 'hours', 'ratings'));
   	}

   	public function editProfile() {

   		$user = Auth::user();
   	
   		return view('user.editProfile', compact('user'));
   		
   	}

   	public function updateProfile(Request $request) {
   		
   		$user = Auth::user();

   		//Create a validator and define the rules and custom error messages
      $valid = Validator::make($request->all(), [
            'photo' => 'url'],
            [
            'photo.url' => 'The profile photo you entered does not look like a URL.']);

        if($valid->fails()) {
            return back()
            ->withInput()
            ->withErrors($valid);
        }

        $qry = User::where('cid', $user->cid)->update($request->except('_method', '_token'));
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated their profile');

        $request->session()->flash('success', 'Profile updated successfully.');
        return back();

   	}

    public function changePass() {
    
      return view('user.changepass');
      
    }

    public function updatePass(Request $request) {

      //Create a validator and define the rules and custom error messages
      $valid = Validator::make($request->all(), [
        'oldpass' => 'required',
        'newpass' => 'required|confirmed',
        ],
        [
        'oldpass.required' => 'You did not enter your current password.',
        'newpass.required' => 'You did not enter a new password.',
        'newpass.confirmed' => 'The new passwords you entered did not match.',
        ]);

      //Check to see if the validator fails
      if($valid->fails()) {
          return redirect()->back()
          ->withErrors($valid);
      }

      //Check to see that the user entered the correct current password
      $user = User::find(Auth::user()->cid);

      //No matchy
      if(!Hash::check($request->oldpass, $user->password)) {
        $request->session()->flash('error', 'The current password you entered was incorrect.');
        return back();
      }

      //Matchy

      //Hash entered password and update password property on controller
      $user->password = Hash::make($request->newpass);

      $user->save();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' changed their password');

      $request->session()->flash('success', 'Password successfully changed.');

      return back();
      
    }
}
