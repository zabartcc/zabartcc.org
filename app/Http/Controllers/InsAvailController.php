<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\InsAvail;
use App\User;
use App\Avail;
use App\Training;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\AvailTakenMail;
use Auth;

use Carbon\Carbon;

use App\Http\Requests;

class InsAvailController extends Controller
{
    public function __construct()
    {
        //Require user to be ins to access all methods on this controller except what is defined here
        $this->middleware('ins', ['except' => '']);
    }

    public function index() {

        $d = Carbon::now();
        if(!$d->isSunday())
            $d = $d->previous(Carbon::SUNDAY);

        $sessions = Avail::where('time_start', '>=', $d)->get();

        $dates = [];

        for($i = 0; $i < 21; $i++) {
            $dates[$i] = [];
            $dates[$i]['string'] = $d->toFormattedDateString();
            $dates[$i]['hasSession'] = 0;
            foreach($sessions as $s) {
                if($s->time_start->isSameDay($d))
                    $dates[$i]['hasSession']++;
            }
            if($d->isToday())
                $dates[$i]['today'] = 1;
            else
                $dates[$i]['today'] = 0;
            if($d->isPast())
                $dates[$i]['past'] = 1;
            else
                $dates[$i]['past'] = 0;
            $dates[$i]['key'] = $d->format('Ymd');
            $d->addDay();
        }

        //dd($dates);

    	return view('ins.avail.index', compact('dates'));
    	
    }

    public function show($date) {

        $d = Carbon::parse($date);
        $d->subDay();
        $d->endOfDay();
        $t = $d->copy()->addDay();

        $sessions = Avail::whereBetween('time_start', [$d, $t])->get();

        return view('ins.avail.show')->with(compact('sessions', 't'));

    }

    public function take(Avail $avail) {
        return view('ins.avail.take')->with(compact('avail'));
    }

    public function create() {

    	return view('ins.avail.create');
    	
    }

    public function store2($id, Request $request) {     

        $avail = Avail::find($id);  

        $training = Training::create([
                'user_id' => $avail->user->cid,
                'ins_id' => Auth::user()->cid,
                'time_start' => strtotime($request->time0),
                'time_end' => strtotime($request->time1),
                'position' => -1,
                'milestone_id' => $avail->milestone_id,
            ]);

        $to_emails = [Auth::user()->email, $avail->user->email];

        $avail->delete();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' took training session for '.$avail->user->fname.' '.$avail->user->lname.' for milestone '.$avail->milestone->name);

        Mail::to($to_emails)->send(new AvailTakenMail(
                        $avail->user->fname,
                        $avail->user->lname, 
                        $request->time0, 
                        $request->time1, 
                        Auth::user()->fname.' '.Auth::user()->lname,
                        $avail->milestone->name));

        $request->session()->flash('success', 'Training session taken. An email has been sent to all involved for confirmation.');

        return redirect()->to('/ins/avail');
    }

    public function delete($id) {
    
        $user = InsAvail::find($id);

        $user->forceDelete();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted avail '.$user->id);

        return back();
        
    }
    
    public function store(Request $request) {
        
        /* unique_slot returns true if a start_time does not fall in the time slot of another
         * training session the user owns
         */
        Validator::extend('unique_slot', function($attribute, $value, $parameters, $validator) {
            //convert to timestamp so we can add an hour 
            $time_start = strtotime($value);
            $time_end = $time_start + 3600;
            //Convert back to a format the database can compare to
            $d_time_start = date('Y-m-d H:i:s',$time_start);
            $d_time_end = date('Y-m-d H:i:s',$time_end);
            $query = InsAvail::where('ins_id',$parameters[0])->where('time_start', '>=', $d_time_start)->where('time_start', '<', $d_time_end)
                ->orwhere('time_end','>', $d_time_start)->where('time_end','<', $d_time_end)->withTrashed();
            return $query->count() == 0;          
        });
        
        
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'time_start' => 'required|unique_slot:'.Auth::user()->cid,
            ]
            ,
            [
            'time_start.required' => 'You did not enter a start time.',
            'time_start.unique_slot' => 'You already have a session for this time slot!']);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/ins/avail/create')
            ->withErrors($valid);
        }

        $time_start = strtotime($request->time_start);
        $time_end = $time_start+3600;

        $avail = InsAvail::create([
                'time_start' => $time_start,
                'time_end' => $time_end,
                'ins_id' => Auth::user()->cid,
            ]);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' posted avail '.$avail->id);

        $request->session()->flash('success', 'Availability added.');

        return back();
    }
}
