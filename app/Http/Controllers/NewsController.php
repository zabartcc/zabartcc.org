<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

use App\Http\Requests;

class NewsController extends Controller
{
    public function index() {

    	$news = News::orderBy('created_at', 'desc')->get();
    
    	return view('news.index', compact('news'));
    	
    }

    public function show(News $news) {
    
    	return view('news.show', compact('news'));
    	
    }
}
