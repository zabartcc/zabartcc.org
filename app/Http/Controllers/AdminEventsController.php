<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Validator;
use Illuminate\Support\Facades\Log;
use Auth;

use App\Http\Requests;

class AdminEventsController extends Controller
{

	public function __construct()
    {
        //Require user to be admin to access all methods on this controller except what is defined here
        $this->middleware('admin', ['except' => '']);
    }


    public function index() {

    	$events = Event::orderBy('time_start', 'asc')->where('archive', 0)->get();

    	return view('admin.events.index')->with(compact('events'));
    }

    public function create() {
    	return view('admin.events.create');
    }

    public function edit($id) {

        $event = Event::find($id);

        return view('admin.events.edit')->with(compact('event'));
    }

    public function delete(Request $request) {
        $event = Event::find($request->eventid);
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' deleted event '.$event->title);
        $event->delete();
        return back();

    }

    public function replaceFile(Request $request, $id) {
        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'img' => 'required|image',
            ],
            [
            'img.required' => 'You did not select an image.',
            'img.image' => 'The file you specified was not an image.'
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/events/edit/'.$id)
            ->withErrors($valid);
        }

        $img = $request->img->store('public/eventimages');
        $img = str_replace('public/', '', $img);

        $event = Event::find($id);

        $event->image = $img;

        $event->save();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' replaced the file for event '.$event->title);
        
        $request->session()->flash('success', 'Image successfully updated.');
    
        return redirect()->to('/admin/events/edit/'.$id);
    }

    public function store(Request $request) {

        //Create a validator and define the rules and custom error messages
        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
            'time_start' => 'required|after:now',
            'time_end' => 'required|after:time_start',
            'img' => 'required|image',
            ],
            [
            'title.required' => 'You did not enter a Title.',
            'body.required' => 'You did not enter any Content.',
            'time_start.required' => 'You did not specify a start time.',
            'time_start.after' => 'The start time must be in the future.',
            'time_end.required' => 'You did not specify an end time.',
            'time_end.after' => "The event cannot start after it ends.",
            'img.required' => 'You did not select an image.',
            'img.image' => 'The file you specified was not an image.'
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/events/create')
            ->withInput()
            ->withErrors($valid);
        }

        $img = $request->img->store('public/eventimages');
        $img = str_replace('public/', '', $img);

        $event = Event::create([
            'time_start' => strtotime($request->time_start),
            'time_end' => strtotime($request->time_end),
            'image' => $img,
            'title' => $request->title,
            'content' => $request->body,
            'user_id' => Auth::user()->cid,
            ]);
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' created event '.$request->title);
        
        $request->session()->flash('success', 'Event '.$request->title.' was successfully created!');
    
        return back();

    }

    public function update(Request $request, $id) {

        $valid = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
            'time_start' => 'required|after:now',
            'time_end' => 'required|after:time_start',
            ],
            [
            'title.required' => 'You did not enter a Title.',
            'content.required' => 'You did not enter any Content.',
            'time_start.required' => 'You did not specify a start time.',
            'time_start.after' => 'The start time must be in the future.',
            'time_end.required' => 'You did not specify an end time.',
            'time_end.after' => "The event cannot start after it ends.",
            ]);

        //Check to see if the validator fails
        if($valid->fails()) {
            return redirect()->to('/admin/events/edit/'.$id)
            ->withErrors($valid);
        }

        $event = Event::find($id);

        $event->title = $request->title;
        $event->content = $request->content;
        $event->time_start = strtotime($request->time_start);
        $event->time_end = strtotime($request->time_end);

        $event->save();
        
        Log::info('User '. Auth::user()->fname.' '.Auth::user()->lname.' updated event '.$request->title);
        
        $request->session()->flash('success', 'Event '.$request->title.' was successfully updated!');
    
        return back();
    }
}
