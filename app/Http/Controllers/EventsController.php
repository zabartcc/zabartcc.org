<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use DB;
use App\Http\Requests;

class EventsController extends Controller
{
    public function index() {

    	$events = Event::where('archive', 0)->orderBy('time_start', 'asc')->get();
    	$eventsarc = Event::where('archive', 1)->orderBy('time_start', 'desc')->get();

    	return view('events.index', compact('events', 'eventsarc'));
    	
    }	
}
