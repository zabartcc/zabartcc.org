<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\FileCategory;
use App\File;
use App\User;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function fetchController($cid, Request $request) {

    	$user = User::onlyTrashed()->find($cid);

    	
    	$restore = 0;

    	if($user) {
    		$userarray['fname'] = $user->fname;
		    $userarray['lname'] = $user->lname;
		    $userarray['rating'] = $user->rating;
		    $userarray['email'] = $user->email;
		    $userarray['srctext'] = "This controller is a past ZAB member. Retrieving old data.";
		    $userarray['restore'] = 1;
    	}

		elseif($cid)
		{	
			$roster = $this->doAPI("GET", "/roster");
    		$check = false;
		    $rosterArr = json_decode($roster);
		    foreach($rosterArr->facility->roster as $controller)
		    {
		        if($controller->cid == $cid)
		        {
		            $userarray['fname'] = (string)$controller->fname;
		            $userarray['lname'] = (string)$controller->lname;
		            $userarray['rating'] = (int)$controller->rating;
		            $userarray['email'] = (string)$controller->email;
		            $userarray['srctext'] = "This information is from the VATUSA server. Everything should be automatically populated.";
		    		$userarray['restore'] = 0;
		            $check = true;
		        }
		    }
		    if(!$check)
		    {
		        $xml = simplexml_load_file("https://cert.vatsim.net/cert/vatsimnet/idstatusint.php?cid=".$cid);
		        foreach($xml as $data)
		        {
		            $userarray['fname'] = (string)$data->name_first;
		            $userarray['lname'] = (string)$data->name_last;
		            $userarray['rating'] = (int)$data->rating;
		    		$userarray['restore'] = 0;
		            $userarray['srctext'] = "This information is from the VATSIM CERT server. As such, the email is obscured, and must be entered manually. The rating may also be changed (as this may be a visiting controller with an instructor rating).";
    	    	}
    	    	if(!$userarray['fname']) {
    	    		header("HTTP/1.1 500 Internal Server Error");
    	    		die();
    	    	}
    		}
		}
    	
    	return json_encode($userarray);
	}	

    public function doAPI($method, $call) {
    	$url = "https://api.vatusa.net/fHen45TiK5cO0Grn".$call;
    	$curl = curl_init($url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	$result = curl_exec($curl);
    	return $result;
	}

	public function fetchOi($oi, Request $request) {
		$checkoi = User::where('oi', $oi)->count();
		$oilength = strlen($oi);
		if($checkoi == 0 && $oilength == 2) {
			return 1;
		}
		else {
			return 0;
		}
	}

	public function updateSort() {
		$sort = Input::get('sort');
		foreach($sort as $key => $value) {
			$id = substr($value, 5);
			FileCategory::find($id)->update(['sort' => $key+1]);
		}
	}
    
    public function fileSort() {
		$sort = Input::get('sort');
		foreach($sort as $key => $value) {
			$id = substr($value, 5);
			$file = File::find($id);
            $file->timestamps = false;
            $file->update(['sort' => $key+1]);
            $file->timestamps = true;
		}
	}

    public function documentSort() {
		$sort = Input::get('sort');
		foreach($sort as $key => $value) {
			$id = substr($value, 5);
			$file = \App\Document::find($id);
            $file->timestamps = false;
            $file->update(['sort' => $key+1]);
            $file->timestamps = true;
		}
	}
}