<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

class CronController extends Controller
{
    public function getDataFile() {
    
    	Storage::delete('vatsim-data.txt');

    	$servers = array(
            "http://us.data.vatsim.net/vatsim-data.txt",
            "http://eu.data.vatsim.net/vatsim-data.txt",
            "http://apac.data.vatsim.net/vatsim-data.txt",
		);

		do
		{
   			$randServer = array_rand($servers);
   			echo "Trying to copy data file from: ".$servers[$randServer];
		}
		while(!Storage::put('vatsim-data.txt', file_get_contents($servers[$randServer])));
    }

    public function updateControllersOnline() {

        error_reporting(E_ALL ^ E_NOTICE);

        \App\AtcOnline::truncate();

    	$airports = array("PHX", "ABQ", "TUS", "AMA", "ROW", "ELP", "SDL", "CHD", "FFZ", "IWA", "DVT", "GEU", "GYR", "LUF", "RYN", "DMA", "FLG", "PRC", "AEG", "BIF", "HMN", "SAF", "FHU");
		$ratings = array("Unknown", "OBS", "S1", "S2", "S3", "C1", "C2", "C3", "I1", "I2", "I3", "SUP", "ADM");

    	$datafile = \Storage::get('vatsim-data.txt');

    	$dataarray = explode("\n", $datafile);

    	foreach($dataarray as $line) {
    		if(preg_match('/^[A-Z0-9]/', $line)) {
    			list($position, $cid, $name, $clienttype, $frequency, , , , , , , , , , , , $rating, , $facility_type, , , , , , , , , , , , , , , , , $atis, , $time_logon, ) = explode(":", $line);
    			if($clienttype == "ATC") {
    				if(in_array(substr($position, 0, 3), $airports) && $facility_type && substr($position, -3) != "FSS") {
						//Generate Info String for Popover
                        $info = "<b>Frequency:</b> ".$frequency;
			            $info .= "<br /><b>Time Online:</b> ".date("H:i:s", (time() - strtotime($time_logon)));
                        //Create insert info into DB table
						$c = new \App\AtcOnline;

    					$c->cid = $cid;
    					$c->name = $name;
    					$c->rating = $rating;
    					$c->pos = $position;
    					$c->time_start = strtotime($time_logon);
    					$c->info = $info;

    					$c->save();

                        //Update controller hours table

                        //Find if this controlling session has been logged yet
                        $hours = \App\ControllerHours::where('cid', $cid)
                                    ->where('time_start', date('Y-m-d H:i:s', strtotime($time_logon)))
                                    ->first();

                        if(!$hours) {
                            //Controlling session not yet logged, create it and set it's initial values
                            $h = new \App\ControllerHours;
                            $h->cid = $cid;
                            $h->time_start = strtotime($time_logon);
                            $h->time_end = time();
                            $h->position = $position;
                            $h->save();
                        }
                        else {
                            //Controlling session has been logged, update end time to current time.
                            $hours->time_end = time();
                            $hours->save();
                        }
    				}
    			}
    		}
    	}
    }

    public function updatePilotsOnline() {
    
    	error_reporting(E_ALL ^ E_NOTICE);

        \App\PilotsOnline::truncate();

    	$airports = array("KPHX", "KABQ", "KTUS", "KAMA", "KROW", "KELP", "KSDL", "KCHD", "KFFZ", "KIWA", "KDVT", "KGEU", "KGYR", "KLUF", "KRYN", "KDMA", "KFLG", "KPRC", "KAEG", "KBIF", "KHMN", "KSAF", "KFHU");
        $ratings = array("Unknown", "OBS", "S1", "S2", "S3", "C1", "C2", "C3", "I1", "I2", "I3", "SUP", "ADM");
        
        // $artcc = new \League\Geotools\Polygon\Polygon([
        //     [37.041667, -102.183333],
        //     [36.5, -101.75],
        //     [36.397222, -101.472222],
        //     [36.275, -101.133333],
        //     [35.9125, -100.211667],
        //     [35.829167, -100],
        //     [35.678611, -100],
        //     [35.333333, -100],
        //     [35.129167, -100.141667],
        //     [34.866667, -100.316667],
        //     [34.466667, -100.75],
        //     [34.491667, -101],
        //     [34.55, -101.541667],
        //     [34.6, -102],
        //     [34.55, -102.325],
        //     [34.388889, -102.6625],
        //     [34.316667, -102.8],
        //     [33.775, -103.366667],
        //     [33.6375, -103.4875],
        //     [33.402778, -103.691667],
        //     [33.383333, -103.8],
        //     [33.05, -103.8],
        //     [33, -103.8],
        //     [32.845833, -103.840278],
        //     [32.466667, -103.933333],
        //     [32.033333, -103.8],
        //     [31.808333, -103.529167],
        //     [31.65, -103.333333],
        //     [31.583333, -103.116667],
        //     [31.425, -102.216667],
        //     [31.283333, -102.15],
        //     [29.733611, -102.675556],
        //     [29.5225, -102.800556],
        //     [29.400278, -102.817222],
        //     [29.350278, -102.883889],
        //     [29.266944, -102.900556],
        //     [29.2225, -102.867222],
        //     [29.166944, -103.000556],
        //     [28.950278, -103.150556],
        //     [28.991944, -103.283889],
        //     [29.016944, -103.383889],
        //     [29.066944, -103.450556],
        //     [29.150278, -103.550556],
        //     [29.183611, -103.683889],
        //     [29.185278, -103.708889],
        //     [29.266944, -103.783889],
        //     [29.316944, -104.000556],
        //     [29.400278, -104.150556],
        //     [29.483611, -104.217222],
        //     [29.533611, -104.350556],
        //     [29.648333, -104.517778],
        //     [29.758611, -104.567222],
        //     [30.000278, -104.700556],
        //     [30.150278, -104.683889],
        //     [30.266667, -104.75],
        //     [30.366944, -104.833889],
        //     [30.550278, -104.900556],
        //     [30.600278, -104.967222],
        //     [30.683611, -104.983889],
        //     [30.683611, -105.050556],
        //     [30.787778, -105.200556],
        //     [30.833333, -105.317222],
        //     [31, -105.550556],
        //     [31.1, -105.650556],
        //     [31.166667, -105.783889],
        //     [31.283333, -105.883889],
        //     [31.341667, -105.951667],
        //     [31.383333, -106.000556],
        //     [31.466667, -106.200556],
        //     [31.666667, -106.333333],
        //     [31.733333, -106.383889],
        //     [31.75, -106.500556],
        //     [31.783333, -106.533889],
        //     [31.784335, -106.571657],
        //     [31.788324, -106.71252],
        //     [31.78947, -106.774294],
        //     [31.804254, -107.528889],
        //     [31.816667, -108.2],
        //     [31.333333, -108.2],
        //     [31.333333, -108.5],
        //     [31.333333, -109.352778],
        //     [31.333333, -110.75],
        //     [31.333307, -111.05],
        //     [31.333303, -111.100039],
        //     [31.368044, -111.186097],
        //     [31.516667, -111.641667],
        //     [31.633333, -112],
        //     [31.973719, -113.092358],
        //     [32.1, -113.508333],
        //     [32.7375, -113.684722],
        //     [32.683333, -114],
        //     [32.866667, -114],
        //     [33.083333, -114],
        //     [33.4, -114],
        //     [34.666667, -114],
        //     [34.916667, -113.616667],
        //     [35.379722, -112.666667],
        //     [35.417778, -112.153056],
        //     [35.438056, -112],
        //     [35.766667, -111.841667],
        //     [35.7, -110.233333],
        //     [35.85, -109.316667],
        //     [36.033333, -108.216667],
        //     [36.2, -107.466667],
        //     [36.626944, -106.35],
        //     [36.716667, -106.083333],
        //     [36.716667, -105.341667],
        //     [36.716667, -105],
        //     [37.045278, -104],
        //     [37.1625, -103.619444],
        //     [37.5, -102.55],
        //     [37.041667, -102.183333],
        // ]);

    	$datafile = \Storage::get('vatsim-data.txt');

    	$dataarray = explode("\n", $datafile);

    	foreach($dataarray as $line) {
    		if(preg_match('/^[A-Z0-9]/', $line)) {
    			list($position, $cid, $name, $clienttype, $frequency, $lat, $lon, , , $planned_aircraft, , $planned_depairport, , $planned_destairport, , , , , , , , , , , , , , , , $remarks, $route, , , , , , , , $heading) = explode(":", $line);
    			if($clienttype == "PILOT") {
                    // $in_artcc = $artcc->pointInPolygon(new \League\Geotools\Coordinate\Coordinate([$lat, $lon]));
                    $is_departure = in_array($planned_depairport, $airports);
                    $is_arrival = in_array($planned_destairport, $airports);
    				if(($is_departure || $is_arrival) && ($lat && $lon)) {
			            $info = "<b>Route:</b> ".$route;
			            $info .= "<hr style='margin:5px 0; border-color: #aaa;' /><b>Remarks:</b> ".$remarks;

			            $c = new \App\PilotsOnline;

    					$c->cid = $cid;
    					$c->name = $name;
    					$c->lat = $lat;
    					$c->lon = $lon;
    					$c->heading = $heading;
                        $c->pos = $position;
    					$c->overflight = ($is_departure || $is_arrival) ? 0 : 1;
    					$c->aircraft = substr($planned_aircraft,0,20);
    					$c->dep = $planned_depairport;
    					$c->dest = $planned_destairport;
    					$c->info = $info;

    					$c->save();
			        }
    			}
    		}
    	}
    	
    }

    public function archiveEvents() {
    
        $events = \App\Event::where('archive', 0)->get();

        foreach($events as $event) {
            if(strtotime($event->time_start) < time()-86400) {
                $event->archive = 1;
                $event->save();
            }
        }
        
    }

    public function getEntries() {
        error_reporting(E_ALL ^ E_NOTICE);

        $datafile = \Storage::get('vatsim-data.txt');

        $dataarray = explode("\n", $datafile);

        foreach($dataarray as $line) {
            if(preg_match('/^[A-Z0-9]/', $line)) {
                list($position, $cid, $name, $clienttype, $frequency, , , , , $planned_aircraft, , $planned_depairport, , $planned_destairport, , , , , , , , , , , , , , , , $remarks, $route, , , , , , ,$time_logon) = explode(":", $line);
                if($clienttype == "PILOT") {
                    if(strpos($remarks, 'ENTRY/ZABFNO')) {
                        echo "Found pilot ".$name."<br />";
                        $e = \App\Entry::where('cid', $cid)
                                        ->where('time_start', date('Y-m-d H:i:s', strtotime($time_logon)))
                                        ->first();

                        if(!$e) {
                            //New entry, enter into table
                            $entry = new \App\Entry;
    
                            $entry->cid = $cid;
                            $entry->name = $name;
                            $entry->callsign = $position;
                            $entry->time_start = strtotime($time_logon);
                            $entry->time_end = time();
                            $entry->dep = $planned_depairport;
                            $entry->dest = $planned_destairport;
                            $entry->route = $route;
    
                            $entry->save();
                        } else {
                            //Update end time (want to make sure the winners dont just log on for 2 minutes, that they actually fly :D)
                            $e->time_end = time();
                            $e->save();
                        }
                    }
                }
            }
        }
    }

}
