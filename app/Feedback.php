<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use SoftDeletes;

    protected $guarded = [
    	'id',
    	'approved'
    ];

    public function user() {
    
    	return $this->belongsTo('App\User', 'user_cid')->withTrashed();
    	
    }
}
