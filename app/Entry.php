<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    public $timestamps = false;

    protected $dates = [
        'time_start',
        'time_end',
    ];
}
