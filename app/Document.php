<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $dates = [
		'created_at',
		'updated_at',
	];
	protected $fillable = [
		'name',
		'description',
		'content',
		'last_update_user',
		'category',
		'uri_slug',
		'sort'
	];

	public function category() {

		return $this->belongsTo('App\FileCategory', 'category');
		
	}

	public function user() {

		return $this->belongsTo('App\User', 'user_cid')->withTrashed();
		
	}
}
