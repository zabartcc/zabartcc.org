<?php

namespace App\Presenter;

use Laracasts\Presenter\Presenter;

class UserPresenter extends Presenter {

	public function genglyph($pos) {
	
		if($this->$pos)
			return '<span style="color: #04B404;" class="glyphicon glyphicon-ok"></span>';
		else
			return '<span style="color: #B40404;" class="glyphicon glyphicon-remove"></span>';
		
	}

	public function displayzab() {
		return $this->genglyph('zab');
	}
	public function displayp50app() {
		return $this->genglyph('p50app');
	}
	public function displayp50twr() {
		return $this->genglyph('p50twr');
	}
	public function displayp50gnd() {
		return $this->genglyph('p50gnd');
	}
	public function displayapp() {
		return $this->genglyph('app');
	}
	public function displaytwr() {
		return $this->genglyph('twr');
	}
	public function displaygnd() {
		return $this->genglyph('gnd');
	}

	public function textRating() {
	
		$ratings = ['UNK', 'OBS', 'S1', 'S2', 'S3', 'C1', 'C2', 'C3', 'I1', 'I2', 'I3', 'SUP', 'ADM'];

		return $ratings[$this->rating];
		
	}

	public function textRatingLong() {
	
		$ratings = ['Unknown', 'Observer', 'Student 1', 'Student 2', 'Student 3', 'Controller 1', 'Controller 2', 'Controller 3', 'Instructor', 'Instructor', 'Senior Instructor', 'Supervisor', 'Administrator'];

		return $ratings[$this->rating];
		
	}

	public function trainingPositions() {

		if($this->position === -1) {
			return $this->milestone->name;
		} else {
			$pos = ['Unknown', 'Clearance Delivery', 'Ground', 'Tower', 'Approach/Departure', 'Center'];
	
			return $pos[$this->position];
		}
	
		
	}


}