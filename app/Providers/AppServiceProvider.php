<?php

namespace App\Providers;
use View;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(\App::environment('production')) {
            \URL::forceScheme('https');
        }

         \View::composer('layouts.header', function ($view) {
            $timeplus7 = time()+604800;
            $avail = \App\InsAvail::where('time_start', '>=', date('Y-m-d'))
                            ->where('time_start', '<', date('Y-m-d', $timeplus7))
                            ->get();
            $count = $avail->count();
            $view->with(compact('count'));
        });

         \View::composer('layouts.admin', function ($view) {
            $feedback = \App\Feedback::where('approved', 0)
                            ->get();
            if($feedback->count() == 0) {
                $fcount = NULL;
            } else {
                $fcount = $feedback->count();
            }
            $view->with(compact('fcount'));
        }); 

        \View::composer('sidebar.atconline', function ($view) {
            $atc = \App\AtcOnline::all();
            $view->with('atc', $atc);
        });

        \View::composer('sidebar.pilotsonline', function ($view) {
            $pilots = \App\PilotsOnline::where('overflight', 0)->get();
            $overflights = \App\PilotsOnline::where('overflight', 1)->get();
            $view->with(compact('pilots', 'overflights'));
        });

        \View::composer('sidebar.topcontrollers', function ($view) {
            error_reporting(E_ALL ^ E_NOTICE);
        
            $days30 = strtotime(date('Y-m-01'));
            $month = date('F', $days30);
			$sessions = \App\ControllerHours::where('time_start', '>', date('Y-m-d H:i:s', $days30))
						->where('position', 'not like', '%_M_%')
						->where('position', 'not like', '%_I_%')
						->get();

            $controllerTimes = [];

            foreach($sessions as $s) {
                $sessionTimeTemp = $s->time_end->timestamp - $s->time_start->timestamp;

                $controllerTimes[$s->cid] += $sessionTimeTemp;

            }

            arsort($controllerTimes);

            foreach($controllerTimes as $k => $c) {
                if(is_numeric(nameByCid($k))) {
                    unset($controllerTimes[$k]);
                }
            }

            $times = array_slice($controllerTimes, 0, 5, true);

            $view->with('times', $times)->with('month', $month);
        });

        \View::composer('sidebar.toppositions', function($view) {
            error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
            $days30 = strtotime(date('Y-m-01'));
            $month = date('F', $days30);
            $sessions = \App\ControllerHours::where('time_start', '>', date('Y-m-d H:i:s', $days30))->get();
            $positions = ['ABQ_CTR' => '', 'ABQ_APP' => '', 'ABQ_TWR' => '', 'ABQ_GND' => '', 'PHX_APP' => '', 'PHX_TWR' => '', 'PHX_GND' => '', 'TUS_APP' => '', 'ELP_APP' => ''];
            foreach($sessions as $s) {
                //Position Logic:
                $pos = explode('_', $s->position);
    
                $pos1 = $pos[0];
                $pos2 = end($pos);
    
                $posfinal = $pos1."_".$pos2;
    
                $sessionTimeTemp = $s->time_end->timestamp - $s->time_start->timestamp;
                $positions[$posfinal] += $sessionTimeTemp;
            }
            arsort($positions);
            $times = array_slice($positions, 0, 5, true);
    
            $view->with('times', $times)->with('month', $month);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
