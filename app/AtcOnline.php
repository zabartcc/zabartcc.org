<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class AtcOnline extends Model
{
    //
    use PresentableTrait;
    protected $table = 'atc_online';
    public $timestamps = false;
    protected $presenter = 'App\Presenter\UserPresenter'; 
}
