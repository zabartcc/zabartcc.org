<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/visiting', 'PagesController@visiting');
Route::get('/credits', 'PagesController@credits');

//User specific stuff
Route::get('login', 'LoginController@showLoginForm');
Route::post('login', 'LoginController@login');
Route::get('/forums/sso', 'LoginController@forumsSso');
Route::get('logout', 'LoginController@logout');
Route::get('dashboard', 'UsersController@index');
Route::get('dashboard/feedback/{feedback}', 'FeedbackController@show');
Route::get('dashboard/changepass', 'UsersController@changePass');
Route::post('dashboard/changepass', 'UsersController@updatePass');
Route::get('dashboard/profile/edit', 'UsersController@editProfile');
Route::patch('dashboard/profile', 'UsersController@updateProfile');
Route::get('dashboard/avail', 'AvailController@index');
Route::post('dashboard/avail', 'AvailController@store');
Route::get('dashboard/avail/create', 'AvailController@create');
Route::get('dashboard/avail/delete/{avail}', 'AvailController@delete');
Route::get('forgotpass', 'LoginController@forgotpass');
Route::post('forgotpass', 'LoginController@generateResetToken');
Route::get('forgotpass/{token}', 'LoginController@sendNewPassword');

//Controllers
Route::get('controllers', 'ControllersController@index');
Route::get('controllers/staff', 'ControllersController@staff');
Route::get('controllers/ins', 'ControllersController@ins');
Route::get('controllers/{user}', 'ControllersController@show');
Route::get('admin/controllers/create', 'AdminControllersController@create');
Route::get('admin/controllers/search', 'AdminControllersController@search');
Route::get('admin/controllers/search/{data}', 'AdminControllersController@searchResults');
Route::get('ins/controllers/search', 'InsControllersController@search');
Route::get('ins/controllers/search/{data}', 'InsControllersController@searchResults');
Route::post('ins/controllers/search/', 'InsControllersController@searchPost');
Route::get('ins/controllers/edit/{user}', 'InsControllersController@edit');
Route::post('ins/controllers/edit', 'InsControllersController@update');
Route::post('admin/controllers/delete/', 'AdminControllersController@delete');
Route::get('admin/controllers/edit/{cid}', 'AdminControllersController@edit');
Route::post('admin/controllers/edit/', 'AdminControllersController@update');
Route::post('admin/controllers', 'AdminControllersController@store');
Route::get('admin/controllers/restore/{cid}', 'AdminControllersController@verifyRestore');
Route::post('admin/controllers/restore/{cid}', 'AdminControllersController@restore');
Route::post('admin/controllers/search/', 'AdminControllersController@searchPost');
Route::get('admin/controllers/email/', 'AdminControllersController@massEmail');
Route::post('admin/controllers/email/', 'AdminControllersController@sendMassEmail');
Route::get('admin/controllers/activity', 'AdminControllersController@getActivity');
Route::get('admin/controllers/activity/{user}', 'AdminControllersController@getControllerActivity');
Route::get('emaillistputo', 'ControllersController@emailList');

//Events
Route::get('events', 'EventsController@index');
Route::get('admin/events', 'AdminEventsController@index');
Route::get('admin/events/create', 'AdminEventsController@create');
Route::post('admin/events', 'AdminEventsController@store');
Route::get('admin/events/edit/{id}', 'AdminEventsController@edit');
Route::post('admin/events/edit/{id}', 'AdminEventsController@update');
Route::post('admin/events/edit/file/{id}', 'AdminEventsController@replaceFile');
Route::post('admin/events/delete', 'AdminEventsController@delete');

//News
Route::get('/news', 'NewsController@index');
Route::get('/news/{news}', 'NewsController@show');
Route::get('/admin/news', 'AdminNewsController@index');
Route::get('/admin/news/create', 'AdminNewsController@create');
Route::post('admin/news', 'AdminNewsController@store');
Route::post('admin/news/delete/', 'AdminNewsController@delete');
Route::get('admin/news/edit/{id}', 'AdminNewsController@edit');
Route::post('admin/news/edit/{id}', 'AdminNewsController@update');

//INS
Route::get('/ins', 'InstructorController@index');

//Avail
Route::get('dashboard/avail/take/{id}', 'AvailController@take');
Route::post('dashboard/avail/take/{id}', 'AvailController@convertToSession');
Route::get('/ins/avail', 'InsAvailController@index');
Route::get('/ins/avail/{date}', 'InsAvailController@show');
Route::get('/ins/avail/take/{avail}', 'InsAvailController@take');
Route::post('/ins/avail/take/{id}', 'InsAvailController@store2');
Route::post('/ins/avail', 'InsAvailController@store');
Route::get('/ins/avail/create', 'InsAvailController@create');
Route::get('/ins/avail/delete/{id}', 'InsAvailController@delete');

//Training
Route::get('/dashboard/training', 'TrainingController@index');
Route::get('/dashboard/training/{id}', 'TrainingController@show');
Route::get('ins/training', 'InsTrainingController@index');
Route::get('ins/training/last20', 'InsTrainingController@last20');
Route::get('ins/training/{id}', 'InsTrainingController@details');
Route::post('ins/training/delete/', 'InsTrainingController@delete');
Route::post('ins/training/{id}', 'InsTrainingController@update');
Route::post('ins/training/finalize/{id}', 'InsTrainingController@finalize');
Route::get('ins/training/merge/{id1}/{id2}', 'InsTrainingController@merge');
Route::get('ins/controllers/training/{user}', 'InsTrainingController@viewSessions');

//Files
Route::get('files', 'FilesController@index');
Route::get('files/{id}', 'FilesController@get');
Route::get('admin/category', 'AdminCategoriesController@index');
Route::post('/admin/category', 'AdminCategoriesController@create');
Route::post('/admin/category/delete/', 'AdminCategoriesController@delete');
Route::get('admin/files', 'AdminFilesController@index');
Route::get('admin/files/create', 'AdminFilesController@create');
Route::post('admin/files', 'AdminFilesController@store');
Route::post('/admin/files/delete/', 'AdminFilesController@delete');
Route::get('admin/files/edit/{id}', 'AdminFilesController@edit');
Route::post('admin/files/edit/{id}', 'AdminFilesController@update');
Route::post('admin/files/edit/file/{id}', 'AdminFilesController@replaceFile');

//Documents
Route::get('/documents', 'DocumentController@index');
Route::get('/documents/{document}', 'DocumentController@show');
Route::get('/admin/documents', 'AdminDocumentController@index');
Route::post('/admin/documents', 'AdminDocumentController@store');
Route::post('/admin/documents/delete', 'AdminDocumentController@delete');
Route::get('/admin/documents/edit/{document}', 'AdminDocumentController@edit');
Route::post('/admin/documents/edit/{document}', 'AdminDocumentController@update');

//Feedback
Route::get('feedback', 'FeedbackController@index');
Route::post('feedback', 'FeedbackController@store');
Route::get('admin/feedback', 'AdminFeedbackController@index');
Route::post('admin/feedback/delete/', 'AdminFeedbackController@delete');
Route::get('admin/feedback/approve/{feedback}', 'AdminFeedbackController@approve');
Route::get('admin/feedback/{user}', 'AdminFeedbackController@show');

//Admin
Route::get('/admin', 'AdminController@index');

//Special AJAX stuff
Route::post('/admin/fetchcontroller/{cid}', 'AjaxController@fetchController');
Route::post('/admin/fetchoi/{oi}', 'AjaxController@fetchOi');
Route::post('/admin/category/sort', 'AjaxController@updateSort');
Route::post('/admin/file/sort', 'AjaxController@fileSort');
Route::post('/admin/document/sort', 'AjaxController@documentSort');


Route::get('/test', 'CronController@getDataFile');
Route::get('/test2', 'CronController@updatePilotsOnline');


// Route::get('/map', function() {
//     $pilots = \App\PilotsOnline::all();
//     return view('modules.map')->with(compact('pilots'));
// });