@extends('layouts.master')

@section('title', 'Instructional Staff Roster')

@section('content')

<style type="text/css">
    th#list {
        border-top: 0px;
    }
</style>


<div class="panel panel-default">
    <div class="panel-heading"><h4>Albuquerque ARTCC Instructional Staff</h4></div>
    <div class="panel-body">
    @if($ta)
        <span style="display: block; text-align: center; font-size: 1.7em;">{{ $ta->fname }} {{ $ta->lname }} - Training Administrator</span>
    @else
        <span style="display: block; text-align: center; font-size: 1.7em;">Vacant - Training Administrator</span>
    @endif
        <span style="display: block; text-align: center; font-size: 1.4em;"><a href="mailto: ta@zabartcc.org">ta@zabartcc.org</a></span>
        <h4 style="display: block; border-bottom: 1px solid #ddd; font-style: italic;">Instructors</h4>
        <table class="table table-condensed table-hover">
            <tr>
                <th id="list">Name</th>
                <th id="list">CID</th>
                <th id="list">Rating</th>
                <th id="list">Email</th>
            </tr>
            @foreach($ins as $m)
            <tr>
                <td>{{ $m->fname }} {{ $m->lname }}</td>
                <td>{{ $m->cid }}</td>
                <td>{{ $m->present()->textRating }}</td>
                <td>{{ $m->email }}</td>
            </tr>
            @endforeach
        </table>
        <h4 style="display: block; border-bottom: 1px solid #ddd; font-style: italic;">Mentors</h4>
        <table class="table table-condensed table-hover">
            <tr>
                <th id="list">Name</th>
                <th id="list">CID</th>
                <th id="list">Rating</th>
                <th id="list">Email</th>
            </tr>
            @foreach($mtr as $m)
            <tr>
                <td>{{ $m->fname }} {{ $m->lname }}</td>
                <td>{{ $m->cid }}</td>
                <td>{{ $m->present()->textRating }}</td>
                <td>{{ $m->email }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>

<script>
    $('#controllersNav').addClass("active");
</script>

@stop