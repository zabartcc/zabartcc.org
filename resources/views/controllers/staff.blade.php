@extends('layouts.master')

@section('title', 'Staff Roster')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading"><h4>Albuquerque ARTCC Staff</h4></div>
    <div class="panel-body">
    @foreach ($staffPos as $poss => $posl)
        <h5>
        <a href="mailto:{{ $poss }}@zabartcc.org">{{ $posl }}</a>
            @if($$poss->isEmpty())
                <span class="pull-right">Vacant</span>
            @else
                @foreach ($$poss as $key => $staff)
                    @if($key == 0)
                        <span class="pull-right" style="font-weight: bold"><a href="{{ url('controllers/'.$staff->cid) }}">{{ $staff->fname }} {{ $staff->lname }}</a></span>
                    @else
                        <span class="pull-right" style="font-weight: bold"><a href="{{ url('controllers/'.$staff->cid) }}">{{ $staff->fname }} {{ $staff->lname }}</a>,&nbsp;</span>
                    @endif
                @endforeach
            @endif
        </h5>
        <div class='row'>
        <div class="col-xs-12">
        <span style="font-size: 1.2em; display: block; padding-bottom: 5px;">The {{ $posl }}</span>
            <ul>
                @foreach($staffDuties[$poss] as $duty)
                    <li>{{ $duty }}</li>
                @endforeach
            </ul>
        </div>
        </div>
    @endforeach

    </div>
</div>

<script>
    $('#controllersNav').addClass("active");
</script>

@stop