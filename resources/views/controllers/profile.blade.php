@extends('layouts.master')

@section('title', 'Controller Profile')

@section('content')


<div class="panel panel-default">
	@if(!$user)
		@include('layouts.404')
	@else
	<div class="panel-heading">
		@if($user->atm)
			<h5 class="pull-right"><span class="label label-success">Air Traffic Manager</span>&nbsp;</h5>
		@endif
		@if($user->datm)
			<h5 class="pull-right"><span class="label label-success">Deputy Air Traffic Manager</span>&nbsp;</h5>
		@endif
		@if($user->ta)
			<h5 class="pull-right"><span class="label label-success">Training Administrator</span>&nbsp;</h5>
		@endif
		@if($user->ec)
			<h5 class="pull-right"><span class="label label-success">Events Coordinator</span>&nbsp;</h5>
		@endif
		@if($user->wm)
			<h5 class="pull-right"><span class="label label-success">Webmaster</span>&nbsp;</h5>
		@endif
		@if($user->fe)
			<h5 class="pull-right"><span class="label label-success">Facility Engineer</span>&nbsp;</h5>
		@endif
		@if($user->ins)
			<h5 class="pull-right"><span class="label label-primary">Instructor</span>&nbsp;</h5>
		@endif
		@if($user->mtr)
			<h5 class="pull-right"><span class="label label-primary">Mentor</span>&nbsp;</h5>
		@endif
		<h3>{{$user->fname}} {{$user->lname}} ({{$user->oi}})</h3>
		<h4><strong>{{$user->present()->textRatingLong}}</strong> - {{$user->cid}}</h4>
	</div>
	<div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <img src="{{ $user->photo }}" class="img-thumbnail" style="max-width: 200px;">
            </div>
            <div class="col-md-8">
                <div>
                    @if($user->zab)
                        <span class="label label-danger">Albuquerque Center</span>&nbsp;
                    @endif
                    @if($user->p50app)
                        <span class="label label-warning">P50 Approach</span>&nbsp;
                    @endif
                    @if($user->p50twr)
                        <span class="label label-warning">Phoenix Tower</span>&nbsp;
                    @endif
                    @if($user->p50gnd)
                        <span class="label label-warning">Phoenix Ground</span>&nbsp;
                    @endif
                    @if($user->app)
                        <span class="label label-info">Minor Approach</span>&nbsp;
                    @endif
                    @if($user->twr)
                        <span class="label label-info">Minor Tower</span>&nbsp;
                    @endif
                    @if($user->gnd)
                        <span class="label label-info">Minor Ground</span>&nbsp;
                    @endif
                </div>
            @if($user->bio)
            <div class="panel panel-default" style="margin-top: 10px;">
                <div class="panel-body">{!! nl2br(e($user->bio)) !!}</div>
            </div>
            @endif
            </div>
        </div>
        <div class="row" id="profile_hours">
            <table style="text-align: center;" class="table table-condensed table-striped table-hover">
                <tr>
                    <th style="text-align: center;"></th>
                    <th style="text-align: center;">DEL</th>
                    <th style="text-align: center;">GND</th>
                    <th style="text-align: center;">TWR</th>
                    <th style="text-align: center;">APP</th>
                    <th style="text-align: center;">CTR</th>
                    <th class="total_cell" style="text-align: center;">Total</th>
                </tr>
                @foreach($months as $month)
                    <tr>
                        <td class="bold">{{$month->format('M Y')}}</td>
                        <td>{{ca_sec2hm($hours[$month->format('Ym')]['del'])}}</td>
                        <td>{{ca_sec2hm($hours[$month->format('Ym')]['gnd'])}}</td>
                        <td>{{ca_sec2hm($hours[$month->format('Ym')]['twr'])}}</td>
                        <td>{{ca_sec2hm($hours[$month->format('Ym')]['app'])}}</td>
                        <td>{{ca_sec2hm($hours[$month->format('Ym')]['ctr'])}}</td>
                        <td class="total_cell">{{ca_sec2hm($hours[$month->format('Ym')]['total'], true)}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td class="bold">>1 Year</td>
                    <td>{{ca_sec2hm($hours['gtyear']['del'])}}</td>
                    <td>{{ca_sec2hm($hours['gtyear']['gnd'])}}</td>
                    <td>{{ca_sec2hm($hours['gtyear']['twr'])}}</td>
                    <td>{{ca_sec2hm($hours['gtyear']['app'])}}</td>
                    <td>{{ca_sec2hm($hours['gtyear']['ctr'])}}</td>
                    <td class="total_cell">{{ca_sec2hm($hours['gtyear']['total'], true)}}</td>
                </tr>
                <tr>
                    <td class="bold total_row">Total</td>
                    <td class="total_row">{{ca_sec2hm($hours['total']['del'], true)}}</td>
                    <td class="total_row">{{ca_sec2hm($hours['total']['gnd'], true)}}</td>
                    <td class="total_row">{{ca_sec2hm($hours['total']['twr'], true)}}</td>
                    <td class="total_row">{{ca_sec2hm($hours['total']['app'], true)}}</td>
                    <td class="total_row">{{ca_sec2hm($hours['total']['ctr'], true)}}</td>
                    <td class="total_cell total_row">{{ca_sec2hm($hours['total']['total'], true)}}</td>
                </tr>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div>
                    <strong class="bold">Total Sessions:</strong>
                    <span>{{$hours['session_count']}}</span>
                </div>
                <div>
                    <strong class="bold">Average Session Time:</strong>
                    <span>{{ca_sec2hm($hours['session_avg'], true)}}</span>
                </div>
            </div>
        </div>
	</div>
	@endif
</div>
<script>
    $('#controllersNav').addClass("active");
</script>

@stop