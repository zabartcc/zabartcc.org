@extends('layouts.master')

@section('title', 'Controller Roster')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Controller Roster</h4>
    </div>
    {{-- <div style="padding: 5px;" class="panel-body"> --}}

	<div class="table-responsive">
	<table class="table table-condensed table-striped table-hover">
		<thead class="thead-inverse">
				<tr>
        		    <th rowspan="2">Name</th>
        		    <th rowspan="2">CID</th>
        		    <th rowspan="2">Rating</th>
                    <th rowspan="2">Initials</th>
        		    <th colspan="4">Major (P50)</th>
        		    <th colspan="3">Minor</th>
        		</tr>
        		<tr>
        		    <th>ZAB</th>
        		    <th>APP</th>
        		    <th>TWR</th>
        		    <th>GND</th>
        		    <th>APP</th>
        		    <th>TWR</th>
        		    <th>GND</th>
        		</tr>
       		</thead>
       		<tbody>
       			@foreach ($controllers as $controller)
					<tr>
						<td><a href='{{ URL::to('/controllers', $controller->cid) }}'>{{ $controller->fname }} {{ $controller->lname }}</a></td>
						<td>{{ $controller->cid }}</td>
                        <td>{{ $controller->present()->textRating }}</td>
                        <td>{{ strtoupper($controller->oi) }}</td>
                        <td>{!! $controller->present()->displayzab !!}</td>
                        <td>{!! $controller->present()->displayp50app !!}</td>
                        <td>{!! $controller->present()->displayp50twr !!}</td>
                        <td>{!! $controller->present()->displayp50gnd !!}</td>
                        <td>{!! $controller->present()->displayapp !!}</td>
                        <td>{!! $controller->present()->displaytwr !!}</td>
                        <td>{!! $controller->present()->displaygnd !!}</td>
					</tr>
				@endforeach
            </tbody>
		</table>
	</div>
    {{-- </div> --}}
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Visitor Roster</h4>
    </div>
    {{-- <div style="padding: 5px;" class="panel-body"> --}}

    <div class="table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <thead class="thead-inverse">
                <tr>
                    <th rowspan="2">Name</th>
                    <th rowspan="2">CID</th>
                    <th rowspan="2">Rating</th>
                    <th rowspan="2">Initials</th>
                    <th colspan="4">Major (P50)</th>
                    <th colspan="3">Minor</th>
                </tr>
                <tr>
                    <th>ZAB</th>
                    <th>APP</th>
                    <th>TWR</th>
                    <th>GND</th>
                    <th>APP</th>
                    <th>TWR</th>
                    <th>GND</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vcontrollers as $controller)
                    <tr>
                        <td><a href='{{ URL::to('/controllers', $controller->cid) }}'>{{ $controller->fname }} {{ $controller->lname }}</a></td>
                        <td>{{ $controller->cid }}</td>
                        <td>{{ $controller->present()->textRating }}</td>
                        <td>{{ strtoupper($controller->oi) }}</td>
                        <td>{!! $controller->present()->displayzab !!}</td>
                        <td>{!! $controller->present()->displayp50app !!}</td>
                        <td>{!! $controller->present()->displayp50twr !!}</td>
                        <td>{!! $controller->present()->displayp50gnd !!}</td>
                        <td>{!! $controller->present()->displayapp !!}</td>
                        <td>{!! $controller->present()->displaytwr !!}</td>
                        <td>{!! $controller->present()->displaygnd !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{-- </div> --}}
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Honorary ZAB Controllers</h4>
    </div>
    {{-- <div style="padding: 5px;" class="panel-body"> --}}
    <p style="display: block; padding: 10px 20px;">The following controllers are no longer active with ZAB, however they are recognized here because of their significant contributions to the ARTCC in the past.</p>
    <div class="table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <thead class="thead-inverse">
                <tr>
                    <th rowspan="2">Name</th>
                    <th rowspan="2">CID</th>
                    <th rowspan="2">Rating</th>
                    <th rowspan="2">Initials</th>
                    <th colspan="4">Major (P50)</th>
                    <th colspan="3">Minor</th>
                </tr>
                <tr>
                    <th>ZAB</th>
                    <th>APP</th>
                    <th>TWR</th>
                    <th>GND</th>
                    <th>APP</th>
                    <th>TWR</th>
                    <th>GND</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($hcontrollers as $controller)
                    <tr>
                        <td><a href='{{ URL::to('/controllers', $controller->cid) }}'>{{ $controller->fname }} {{ $controller->lname }}</a></td>
                        <td>{{ $controller->cid }}</td>
                        <td>{{ $controller->present()->textRating }}</td>
                        <td>{{ strtoupper($controller->oi) }}</td>
                        <td>{!! $controller->present()->displayzab !!}</td>
                        <td>{!! $controller->present()->displayp50app !!}</td>
                        <td>{!! $controller->present()->displayp50twr !!}</td>
                        <td>{!! $controller->present()->displayp50gnd !!}</td>
                        <td>{!! $controller->present()->displayapp !!}</td>
                        <td>{!! $controller->present()->displaytwr !!}</td>
                        <td>{!! $controller->present()->displaygnd !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{-- </div> --}}
</div>

<script>
    $('#controllersNav').addClass("active");
</script>

@stop