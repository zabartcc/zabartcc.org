@extends('layouts.master')

@section('title', 'Events')

@section('content')

<style type="text/css">
    .label {
        font-size: 1.0em;
    }
</style>

<div class="panel panel-default">
    @if($events->isEmpty())
        <div class="panel-heading">
            <h4>
                No Upcoming Events
            </h4>
        </div>
        <div class="panel-body">
            <p>We're sorry, but at this time, Albuquerque ARTCC is not planning any upcoming events. Please check back later.</p>
        </div>
    @endif
    @foreach ($events as $item)
    <div class="panel-heading" @unless($loop->first) style="border-top: 1px solid #ddd;" @endif>
        <h3>            
            <strong>{{ $item->title }}</strong><span style="font-size: 0.7em;" class="pull-right">{{$item->created_at->format('F j, Y')}}</span>
        </h3>
        <h4>
            <b>Event Time: </b>{{$item->time_start->format('F j, Y H:i')}} - {{$item->time_end->format('F j, Y H:i')}}<br />
            <span style="font-size: 0.8em;"><strong>Posted by:</strong> {{$item->user->fname}} {{$item->user->lname}}</span>
        </h4>
    </div>
    <div class="panel-body">
        <img class="center-block img-responsive" src="storage/{{ $item->image }}" alt="{{ $item->title }}" style="margin-bottom: 15px;" />
        <p>{!! nl2br(e($item->content)) !!}</p>
    </div>
    @endforeach
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4>Archived Events</h4>
    </div>
    <div class="list-group">
        @foreach($eventsarc as $item)
            <li class="list-group-item">{{ $item->title }}<span class="label label-info pull-right">{{$item->time_start->format('F j, Y - H:i')}}</span></li>
        @endforeach
    </div>
</div>

<script>
    $('#eventsNav').addClass("active");
</script>

@stop