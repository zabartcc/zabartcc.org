@extends('layouts.ins')

@section('title', 'Your Training Sessions')

@section('content')

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Cancellation</h4>
      </div>
      <div class="modal-body">
        <div>
            Are you sure you wish to cancel this training session?<br /><br />
            This should only be done if either you or the student cannot attend the session.
            <br /><br />
            This action is reversable.<br /><br />
            <form action="{{ URL::to('ins/training/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="sessionDelete" name='session' value='' />
                <button type="submit" class="btn btn-danger">I understand, cancel this session</button>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without cancelling</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Open Training Sessions</h4>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
            <th>Student</th>
            <th>Milestone</th>
        		<th>Start Time</th>
        		<th>End Time</th>
        		<th>Options</th>
        	</tr>
       			</thead>
       			<tbody>
            @if($trainingopen->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No open training sessions to display.</td>
              </tr>
            @endif
       			@foreach ($trainingopen as $item)
       				<tr>
                <td>@if($item->user) 
                      {{ $item->user->fname }} {{ $item->user->lname }} 
                    @else 
                      {{ $item->user_id }} 
                    @endif
                </td>
                <td>{{ $item->milestone_id == -1 ? $item->present()->trainingPositions : $item->milestone->name }}</td>
       					<td>{{$item->time_start->format('F j, Y, H:i')}}</td>
                <td>{{$item->time_end->format('F j, Y, H:i')}}</td>
                <td style="text-align: right;">
                  <a class="nohover" title="Add training notes" href="{{ url('/ins/training', $item->id) }}">
                    <span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-edit"></span>
                  </a>
                  <a class="nohover cursor" title="Cancel this training session" data-session='{{$item->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
                    <span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-ban-circle"></span>
                  </a>
                </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h4>Completed Training Sessions</h4>
  </div>
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-condensed table-striped table-bordered">
        <thead class="thead-inverse">
          <tr>
            <th>Student</th>
            <th>Milestone</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Options</th>
          </tr>
            </thead>
            <tbody>
            @if($trainingcomplete->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No completed training sessions to display.</td>
              </tr>
            @endif
            @foreach ($trainingcomplete as $item)
              <tr>
                <td>{{ nameOrCid($item) }}</td>
                <td>{{ $item->milestone_id == -1 ? $item->present()->trainingPositions : $item->milestone->name }}</td>
                <td>{{$item->time_start->format('F j, Y, H:i')}}</td>
                <td>{{$item->time_end->format('F j, Y, H:i')}}</td>
                <td style="text-align: right;">
                  <a class="nohover" title="View training notes" href="{{ url('/ins/training', $item->id) }}">
                    <span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-search"></span>
                  </a>
                </td>
              </tr>
            @endforeach
              </tbody>
      </table>
    </div>
  </div>
</div>

<script>
    
    //Modals
    $('#modalButton').click( function (e) {
       e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var session = $(this).data('session');
        $('#sessionDelete').val(session);
        
    });

</script>

@stop