@extends('layouts.ins')

@section('title', 'Training Session Details')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Training Session Details</h4>
	</div>
	<div class="panel-body">
    @if(!$training->completed)
    <form class="form-horizontal" action="{{ url('ins/training', $training->id) }}" method="POST">
      {{ csrf_field() }}
      @if ($errors->all())
        <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
      <div class="col-sm-6">
        <div style="margin-bottom: 0;" class="form-group">
          <label class="control-label">Student:</label><br />
          <div class="well form-control-static" style="padding: 10px;">{{ nameOrCid($training) }}</div>
        </div>
        <div style="margin-bottom: 0;" class="form-group">
          <label class="control-label">Position:</label><br />
          <div class="well form-control-static" style="padding: 10px;">{{ $training->milestone_id == -1 ? $training->present()->trainingPositions : $training->milestone->name }}</div>
        </div>
        <div style="margin-bottom: 0;" class="form-group">
          <label class="control-label">Session Time:</label><br />
          <div class="well form-control-static" style="padding: 10px;">{{$training->time_start->format('F j, Y, H:i')}} - {{$training->time_end->format('F j, Y, H:i')}}</td></div>
        </div>
        <div class="form-group">
          <label for='notes' class="control-label">Notes</label>
          @if(!$training->notes)
            <textarea name="notes" class="form-control" rows="10" placeholder="Please enter training notes here..."></textarea>
          @else
            <textarea name="notes" class="form-control" rows="10">{{$training->notes}}</textarea>
          @endif
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="submit" formaction="{{ url('ins/training/finalize', $training->id) }}" class="btn btn-primary">Submit and Finalize</a>
        </div>
      </div>
      <div class="col-sm-6">
      <blockquote>
        <p>Any notes entered here are visible to the student attached to the session.
          Please use these to address the student directly about the training session. <br /><br />
          {{-- Any notes that you would like to add that are INS only (such as recommendations for next session, info about student, etc.) can be added with the "INS Notes" option to the left. Those notes can be attached to this session.<br /><br /> --}}
          Clicking on the 'Save' button will save your progress with the training notes. They will not be visible to the student until you click the 'Submit and Finalize' button. </p>
      </blockquote>
    </div>
    @else
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Student:</label><br />
      <div class="well form-control-static" style="padding: 10px;">@if($training->user) 
                      {{ $training->user->fname }} {{ $training->user->lname }} 
                    @else 
                      {{ $training->user_id }} 
                    @endif</div>
    </div>
    <div class="form-group">
      <label class="control-label">Position:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{ $training->milestone_id == -1 ? $training->present()->trainingPositions : $training->milestone->name }}</div>
    </div>
    <div class="form-group">
      <label class="control-label">Session Time:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{$training->time_start->format('F j, Y, H:i')}} - {{$training->time_end->format('F j, Y, H:i')}}</td></div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Notes:</label><br />
      @if(!$training->notes)
        <div class="well form-control-static" style="padding: 10px;">No training notes.</div>
      @else
        <div class="well form-control-static" style="padding: 10px;">{!! nl2br(e($training->notes)) !!}</div>
      @endif
    </div>
  </div>
    @endif
    </form>
	</div>
</div>

@stop