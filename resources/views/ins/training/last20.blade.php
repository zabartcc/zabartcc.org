@extends('layouts.ins')

@section('title', 'Last 20 Training Sessions')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Last 20 Training Sessions</h4>
	</div>
	{{-- <div class="panel-body"> --}}
		{{-- <div class="table table-responsive"> --}}
			<table class="table table-condensed table-striped table-hover">
				<thead class="thead-inverse">
				<tr>
                    <th>Student</th>
                    <th>Instructor</th>
                    <th>Milestone</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Notes</th>
                </tr>
       			</thead>
       			<tbody>
            @if($sessions->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No training sessions to display.</td>
              </tr>
            @endif
       			@foreach ($sessions as $item)
       				<tr>
                <td>{{ nameOrCid($item) }}</td>
                <td>{{ nameOrCid($item, 'ins', 'ins_id') }}</td>
                <td>{{ $item->milestone_id == -1 ? $item->present()->trainingPositions : $item->milestone->name }}</td>
       					<td>{{$item->time_start->format('F j, Y, H:i')}}</td>
                <td>{{$item->time_end->format('F j, Y, H:i')}}</td>
                <td style="text-align: center;">
                  <a class="nohover" data-toggle='popover' data-html='true' data-placement='left' data-trigger='hover' data-content='{{ $item->notes }}' title="Notes" href="{{ url('/ins/training', $item->id) }}">
                    <span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-folder-open"></span>
                  </a>
                </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		{{-- </div> --}}
	{{-- </div> --}}
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>

@stop