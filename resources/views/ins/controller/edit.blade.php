@extends('layouts.ins')

@section('title', 'Edit Controller')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Edit Controller - {{$user->fname}} {{$user->lname}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('ins/controllers/edit') }}" method="POST">
			{{ csrf_field() }}
			<!-- Workaround for unchecked checkboxes not existing in POST array. -1 is a workaround for the method used to parse the $_POST variable, as in checking if a field is empty, the 0 value for a non-checked box causes the script to think the box is empty and exits. -->
            <input type="hidden" name="zab" value="-1" />
            <input type="hidden" name="p50app" value="-1" />
            <input type="hidden" name="app" value="-1" />
            <input type="hidden" name="p50twr" value="-1" />
            <input type="hidden" name="twr" value="-1" />
            <input type="hidden" name="p50gnd" value="-1" />
            <input type="hidden" name="gnd" value="-1" />
			@if ($errors->all())
            	<div class="alert fadeAlert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert fadeAlert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-5">
				<div class="form-group">
					<label for="cid" class="control-label">Controller ID</label>
					<input type="text" name="cid" id="cid" value="{{$user->cid}}" class="form-control" disabled="" />
					<input type="hidden" name="cid" id="cid" value="{{$user->cid}}" />
				</div>
				<br />
				<div class="form-group">
					<label for="fname" class="control-label">Controller Name</label>
					<input type="text" name="fname" id="fname" disabled='' value='{{$user->fname}}' placeholder="First" class="form-control"><br/>
					<input type="text" name="lname" id="lname" disabled="" value='{{$user->lname}}' placeholder="Last" class="form-control">
				</div>
				<div class="form-group">
					<label for="email" class="control-label">Email</label>
					<input type="text" name="email" id="email" disabled="" value="{{$user->email}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="oi" class="control-label">Operating Initials</label>
					<input type="text" disabled="" name="oi" id="oi" value="{{ $user->oi }}" class="form-control">
				</div>
				<div class="form-group">
					<label for="rating" class="control-label">Rating</label><br />
					<select name="rating" id="rating" class="form-control">
					@foreach ($ratings as $key => $rating)
						@if ($rating)
							@if($key+1 == $user->rating)
								<option value="{{$key+1}}" selected="">{{$rating}}</option>
							@else
								<option value="{{$key+1}}">{{$rating}}</option>
							@endif
						@endif
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label>Certifications</label>
					<table style="font-size: 0.8em;" class="table table-condensed table-striped table-bordered">
						<thead class="thead-inverse">
							<tr>
        					    <th colspan="4">Major (P50)</th>
        					    <th colspan="3">Minor</th>
        					</tr>
        					<tr>
        					    <th>ZAB</th>
        					    <th>APP</th>
        					    <th>TWR</th>
        					    <th>GND</th>
        					    <th>APP</th>
        					    <th>TWR</th>
        					    <th>GND</th>
        					</tr>
       					</thead>
       					<tbody style="text-align: center;">
       						<td><input type="checkbox" value="1" name="zab" @if ($user->zab) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50app" @if ($user->p50app) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50twr" @if ($user->p50twr) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50gnd" @if ($user->p50gnd) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="app" @if ($user->app) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="twr" @if ($user->twr) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="gnd" @if ($user->gnd) checked="checked" @endif ></td>
            			</tbody>
					</table>
				</div>
				<div class="form-group">
					<label>Traning Milestones</label>
					<table style="font-size: 0.8em;" class="table table-condensed table-striped table-bordered">
						<thead class="thead-inverse">
        					<tr>
        						@foreach($milestones as $m)
        					    	<th>{{ $m->code }}</th>
        					    @endforeach
        					</tr>
       					</thead>
       					<tbody style="text-align: center;">
       						@foreach($milestones as $m)
        					    <td><input type="checkbox" value="{{ $m->bitwise_id }}" name="tm_{{ $m->code }}" @if ($user->training_milestone & $m->bitwise_id) checked="checked" @endif ></td>
        					@endforeach
            			</tbody>
					</table>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-6 col-sm-offset-1">
				<blockquote>
					<p>Here you can edit any property of a controller. If you need to update the CID, please email the <a href="mailto:web@zabartcc.org">webmaster</a>.</p>
				</blockquote>
			</div>	
	</div>
</div>

@stop