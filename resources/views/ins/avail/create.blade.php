@extends('layouts.ins')

@section('title', 'Post Availablity')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Post Your Availablity</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('ins/avail') }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
                	<label for="time_start" class="control-label">Availability Start</label>
                	<div class="input-group date form_datetime" data-link-field="time_start">
                    	<input class="form-control" name='time_start' type="text">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>Availability is submitted in 1 hour timeblocks. If you wish to post more than 1 hour, please submit this form again for each hour.</p>
				</blockquote>
			</div>	
	</div>
</div>

<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 0,
        format: 'yyyy-mm-dd hh:ii'
    });
</script>
@stop