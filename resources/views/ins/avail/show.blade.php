@extends('layouts.ins')

@section('title', 'Student Training Requests')

@section('content')

<style type="text/css">
    
    table.calendar {
        text-align: center;
        vertical-align: top;
    }

    table.calendar td {
        height: 80px;
        width: 14%;
    }

    table.calendar td div {
        display: block;
        vertical-align: bottom;
        color:red;
    }

</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Student Training Requests - {{ $t->format('F j, Y') }}</h4>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<tr>
        		  	<th>Student</th>
                    <th>Milestone</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Options</th>
        		</tr>
                @if($sessions->isEmpty())
                    <tr><td style="text-align: center;" colspan="5">No student requests for {{ $t->format('F j, Y') }}.</td></tr>
                @endif
                @foreach($sessions as $s)
                    <tr>
                        <td>{{ $s->user->fname }} {{ $s->user->lname }}</td>
                        <td>{{ $s->milestone->name }}</td>
                        <td>{{ $s->time_start }}</td>
                        <td>{{ $s->time_end }}</td>
                        <td><a href="{{ url('/ins/avail/take', $s->id) }}" class="btn btn-xs btn-primary">Take Session</button></td>
                    </tr>
                @endforeach
			</table>
		</div>
	</div>
</div>

@stop