@extends('layouts.ins')

@section('title', 'Student Training Requests')

@section('content')

<style type="text/css">
    
    table.calendar {
        text-align: center;
        vertical-align: top;
    }

    table.calendar td {
        height: 80px;
        width: 14%;
    }

    table.calendar td div {
        display: block;
        vertical-align: bottom;
        color:red;
    }

</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Student Training Requests</h4>
	</div>
	<div class="panel-body">
        @if(Session::has('success'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
        @endif
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered calendar">
				<tr>
        		  	<th>Sunday</th>
        		  	<th>Monday</th>
        		  	<th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
        		</tr>
                @foreach($dates as $k => $d)
                    @if($k == 0 || $k == 7 || $k == 14)
                        <tr>
                    @endif
                        <td>
                            <a href="{{ url('/ins/avail', $d['key']) }}">
                            @if($d['today'])
                                <strong>{{ $d['string'] }}</strong>
                            @elseif($d['past'])
                                <span style="text-decoration: line-through;">{{ $d['string'] }}</span>
                            @else
                                {{ $d['string'] }}
                            @endif
                            </a>
                            @if($d['hasSession'])
                                <br /> <br />
                                <div>{{ $d['hasSession'] }} {{ ($d['hasSession'] == 1) ? 'session' : 'sessions' }}</div>
                            @endif
                        </td>                    
                    @if($k == 6 || $k == 13 || $k == 20)
                        </tr>
                    @endif
                @endforeach
			</table>
		</div>
	</div>
</div>

@stop