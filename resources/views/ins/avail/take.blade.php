@extends('layouts.ins')

@section('title', 'Training Request')

@section('content')

<style type="text/css">
    
    table.calendar {
        text-align: center;
        vertical-align: top;
    }

    table.calendar td {
        height: 80px;
        width: 14%;
    }

    table.calendar td div {
        display: block;
        vertical-align: bottom;
        color:red;
    }

</style>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/nouislider.css') }}">
<script type="text/javascript" src="{{ URL::asset('/js/nouislider.min.js') }}"></script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Training Request</h4>
	</div>
	<div class="panel-body">
        <form class="form-horizontal" action="{{ url('/ins/avail/take', $avail->id) }}" method="POST">
        {{ csrf_field() }}
        <div class="col-sm-6">
            <div class="form-group">
                <label for="student">Student</label>
                <input type="text" class="form-control" name="student" id="student" value="{{ $avail->user->fname }} {{ $avail->user->lname }}" disabled>
            </div>
            <div class="form-group">
                <label for="milestone">Milestone</label>
                <input type="text" class="form-control" name="milestone" id="milestone" value="{{ $avail->milestone->name }}" disabled>
            </div>
            <div class="form-group">
                <label for="time">Select Time</label>
                <div style="padding: 0 20px;" id="slider-datetime"></div><br />
                <label for="time">Start Time</label>
                <input type="text" class="form-control" name="time0" id="time0" value="{{ $avail->time_start }}" readonly>
                <label for="time">End Time</label>
                <input type="text" class="form-control" name="time1" id="time1" value="{{ $avail->time_end }}" readonly>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </div>
        </form>
    </div>
</div>

<script type="text/javascript">
function timestamp(str){
    return new Date(str).getTime();   
}

var dateSlider = document.getElementById('slider-datetime');

noUiSlider.create(dateSlider, {
    range: {
        min: timestamp('{{ $avail->time_start }}'),
        max: timestamp('{{ $avail->time_end }}')
    },
    step: 1000 * 60 * 15,
    connect: true,
    start: [ timestamp('{{ $avail->time_start }}'), timestamp('{{ $avail->time_end }}') ],
})

dateSlider.noUiSlider.on('update', function( values, handle ) {
    //console.log(values)
    $('#time'+handle).val(moment(new Date(+values[handle])).format("YYYY-MM-DD HH:mm"))
});

</script>

@stop