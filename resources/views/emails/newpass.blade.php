<!DOCTYPE html>
<html>
<head>
	<title>Your New Albuquerque ARTCC Password</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>Your New Albuquerque ARTCC Password</h1>
		<p>Hello {{ $fname }} {{ $lname }},<br />
		<br />
		Your Albuquerque ARTCC password has been successfully changed. Your new password is below. You may now log in with it and change it to something more memorable.<br />
		<br />
		New Password: {{ $newpass }}<br />
		<br />
		If this was not you initiating this change, please email the webmaster at wm@zabartcc.org.
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>