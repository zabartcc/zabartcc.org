<!DOCTYPE html>
<html>
<head>
	<title>Password Reset Link</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>Password Reset Link</h1>
		<p>Hello {{ $fname }} {{ $lname }},<br />
		<br />
		The reset password function was triggered on your account with http://zabartcc.org. If this was you initiating this change, then click on the link below to reset your password. If you cannot click the link below, please copy and paste it into your browser. This link expires in 1 hour.<br />
		<br />
		Password Reset Link: https://zabartcc.org/forgotpass/{{ $token }}<br />
		<br />
		If this was not you initiating this change, then simply ignore this email. Once the link expires, it will no longer work.
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>