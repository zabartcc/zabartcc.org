<!DOCTYPE html>
<html>
<head>
	<title>Welcome to Albuquerque ARTCC</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>Welcome to Albuquerque ARTCC!</h1>
		<p>Hello {{ $fname }} {{ $lname }},<br />
		<br />
		Welcome to Albuquerque ARTCC. Your accounts on the Albuquerque ARTCC website (https://zabartcc.org) and Forums (https://forum.zabartcc.org) have been created, and are ready for login. Both the main website and the forums use the same credentials. You will need these accounts to schedule training sessions, sign up for positions for events, view your feedback and more. Once logged in, you will have the option to change your password to something more memorable.<br />
		<br />
		Username: Your VATSIM ID ({{ $cid }})<br />
		Password: {{ $password }}<br />
		<br />
		On behalf of the staff and all of our controllers, welcome to Albuquerque ARTCC. We hope you excel!<br />
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>