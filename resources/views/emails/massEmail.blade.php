<!DOCTYPE html>
<html>
<head>
	<title>A Message From Albuquerque ARTCC</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>A Message From Albuquerque ARTCC</h1>
		<p>Hello {{ $user->fname }} {{ $user->lname }},<br />
		<br />
		{{ $content }}<br />
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. You were sent this email because you are a memeber of the Albuquerque ARTCC on VATSIM. This email was sent through an automated system. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>