<!DOCTYPE html>
<html>
<head>
	<title>Welcome to Albuquerque ARTCC</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>New ZAB Memeber</h1>
		<p>Hello,<br />
		<br />
		Just thought you'd like to know that a new user has been added to the ZAB roster. Please find his/her info below:<br />
		<br />
		CID: {{$cid}}<br />
		Name: {{$fname}} {{$lname}}<br />
		Rating: {{$rating}}<br />
		Email: {{$email}}<br />
 		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>