<!DOCTYPE html>
<html>
<head>
	<title>Session Request Taken</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>Session Request Taken</h1>
		<p>Hello {{ $fname }} {{ $lname }},<br />
		<br />
		Your requested for training for milestone {{ $milestone }} has been taken by {{ $ins_name }} from {{ $session_start }} to {{ $session_end }}.<br />
		<br />
		If you need to cancel this session, please notify your Instructor and TA ASAP. Thank you!
		<br />
		-The ZAB Instructional Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>