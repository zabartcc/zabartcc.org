<!DOCTYPE html>
<html>
<head>
	<title>New Feedback</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>New Feedback!</h1>
		<p>Hello {{ $feedback->user->fname }} {{ $feedback->user->lname }},<br />
		<br />
		You have recieved new feedback on your controller profile from {{ $submitter_name }}. Please visit <a href="https://zabartcc.org/dashboard">your dashboard</a> to view it.<br />
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>