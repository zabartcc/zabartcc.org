<!DOCTYPE html>
<html>
<head>
	<title>New Training Request</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<div style="margin: 20px; padding: 20px;">
	<h1>New Training Request</h1>
		<p>Hello,<br />
		<br />
		A new training session has been requested by {{ $student->fname }} {{ $student->lname }} for milestone {{ $milestone->name }}<br />
		<br />
		Request Start Time: {{ $time_start }}<br />
		Request End Time: {{ $time_end }}<br />
		<br />
		If you are available and would like to train this student, please log into the Instructor Panel and take the session.<br />
		<br />
		-The ZAB Staff Team<br />
		<br />
		<i>Bleep bloop, I am a robot. This is an automated email. Replies to this message will not be delivered.</i></p>
</div>
</body>
</html>