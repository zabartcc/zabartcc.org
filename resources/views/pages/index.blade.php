@extends('layouts.master')

@section('title', 'Home')

@section('content')

<style type="text/css">
	.label {
		font-size: 1.0em;
	}
</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Welcome to the Albuquerque ARTCC</h3>
		<span>An introduction by Alexandra Robison, ATM</span>
	</div>
	<div class="panel-body">
		<p>&nbsp;&nbsp;&nbsp;Hello, and welcome to the Albuquerque ARTCC! ZAB covers almost all of Arizona, as well as New Mexico, and parts of Texas. We are home to many popular airports, including Phoenix-Sky Harbor (KPHX), Albuquerque Sunport (KABQ), Tucson (KTUS) and El Paso (KELP). If you are interested in becoming a controller here, please head on over to <a href="http://vatusa.net">VATUSA</a>, and follow the process to become a home controller at one of the most interesting ARTCCs in the US. If you would like to be a visiting controller click <a href="{{ url('/visiting') }}">here</a>. Our excellent instructor team will ensure that you recieve the best training possible, so that you can become the best virtual controller you can be!</p>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Recent News Items</h4>
	</div>
	<div class="list-group">
		@if($news->isEmpty())
			<li class="list-group-item"><em>No news items posted in the last 3 months.</em></li>
    	@endif
		@foreach($news as $item)
  			<a href="{{ url('news', $item->id) }}" class="list-group-item">{{ $item->title }}<span class="label label-info pull-right">{{$item->created_at->format('F j, Y')}}</span></a>
		@endforeach
	</div>
</div>

<div class="panel panel-default">
    @if($events->isEmpty())
        <div class="panel-heading">
            <h4>
                No Upcoming Events
            </h4>
        </div>
        <div class="panel-body">
            <p>We're sorry, but at this time, Albuquerque ARTCC is not planning any upcoming events. Please check back later.</p>
        </div>
    @endif
    @foreach ($events as $item)
    <div class="panel-heading" @unless($loop->first) style="border-top: 1px solid #ddd;" @endif>
        <h3>            
            <strong>{{ $item->title }}</strong><span style="font-size: 0.7em;" class="pull-right">{{$item->created_at->format('F j, Y')}}</span>
        </h3>
        <h4>
            <b>Event Time: </b>{{$item->time_start->format('F j, Y H:i')}} - {{$item->time_end->format('F j, Y H:i')}}<br />
            <span style="font-size: 0.8em;"><strong>Posted by:</strong> {{$item->user->fname}} {{$item->user->lname}}</span>
        </h4>
    </div>
    <div class="panel-body">
        <img class="center-block img-responsive" src="storage/{{ $item->image }}" alt="{{ $item->title }}" style="margin-bottom: 15px;" />
        <p>{!! nl2br(e($item->content)) !!}</p>
    </div>
    @endforeach
</div>

<div class="panel panel-default">
    <div class="panel-heading"><h4>Flight Map</h4></div>
    <div class="panel-body" id="map_container">
        <!-- <span>Click on a plane to view more information. Click on it again, or click on the ARTCC shape to go back to the default view. White airplanes are departures/arrivals, blue airplanes are overflights currently within the airspace.</span> -->
        @include('modules.map')
    </div>
</div>

<script>
	$('#indexNav').addClass("active");
</script>

@stop