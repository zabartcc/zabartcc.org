@extends('layouts.master')

@section('title', 'Visiting Controllers')

@section('content')

<style type="text/css">
	ul#credits li {
		font-size: 1.2em;
		font-weight: bold;
	}
</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Visiting</h3>
	</div>
	<div class="panel-body">
		<p>Thank you for your interest in visiting Albuquerque ARTCC. To apply for visiting status, please email the DATM at datm@zabartcc.org, and CC atm@zabartcc.org with the following information:</p>
		<ul>
			<li>Full Name</li>
			<li>VATSIM ID</li>
			<li>Home ARTCC/FIR/ACC</li>
		</ul>
		Thank you for your interest!
	</div>
</div>

@stop