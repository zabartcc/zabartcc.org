@extends('layouts.master')

@section('title', 'Website Credits')

@section('content')

<style type="text/css">
	ul#credits li {
		font-size: 1.2em;
		font-weight: bold;
	}
</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Credits</h3>
	</div>
	<div class="panel-body">
		<p>Congrats, you have landed on my credits page. This is where I give credit where credit is due with regards to this website. I could not have done this without any of the following:</p>
		<ul id="credits">
			<li><a href='https://laravel.com/' target="_blank">Laravel</a> - By Taylor Otwell and GitHub contibutors</li>
			<p>This is the framework that the entire website relies on. This allowed me to develop MUCH faster than before. Laravel takes care of certain tasks that most, if not all websites use (authentication, routing, database relationships, CSRF, etc.), so I did not have to worry about implementing them on my own.</p>
			<li><a href="https://getbootstrap.com" target="_blank">Twitter Boostrap</a> - By @mdo, @fat and GitHub contibutors</li>
			<p>This is the base for all of the styling that you see. Bootstrap is the defacto web standard these days for professional, functional websites. Gone are the days where I had to write ever piece of CSS by hand to make things look good.</p>
			<li>Jeffrey Way of <a href="https://laracasts.com" target="_blank">Laracasts</a></li>
			<p>Laracasts is where I learned Laravel. Jeffrey presents Laravel in a really simple and understandable way. In addition, the Laracasts forums have been the destination of many search results because of errors I could not figure out myself. Jeffrey has also written a couple of quick plugins for Laravel that I use.</p>
			<li>You, the ZAB Controller</li>
			<p>If you did not exist, I would not have anyone to write this website for. Your suggestions and feedback from the user level made this website what it is today.</p>
			<li>Tabitha, My Wife</li>
			<p>Throughout this development process, she put up with my frustrations and anger. She also was able to give a non-programmers perspective on a couple of problems that I was attempting to solve. I am forever grateful.</p>
		</ul>
	</div>
</div>

@stop