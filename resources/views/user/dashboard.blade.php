@extends('layouts.user')

@section('title', 'Home')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Controller Dashboard</h4>
	</div>
	<div class="panel-body">
		@if(Auth::user()->iep)
		<h5>New Email Password - </h5>{{ Auth::user()->iep }}
		@endif
		<div class="col-md-12">
			<h5>Your Feedback</h5>
			<table class="table table-bordered table-striped">
				<tr>
					<th>Date/Time</th>
					<th>Rating</th>
					<th>Details</th>
				</tr>
				@foreach ($feedback as $item)
				<tr>
					<td>{{$item->created_at->format('F j, Y')}}<br />{{$item->created_at->format('H:i:s')}}</td>
       				<td>{{$ratings[$item->rating]}}</td>
       				<td>
						<a class="nohover pull-right" title="View feedback details" href="{{ url('dashboard/feedback', $item->id) }}">
                        	<span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-file"></span>
                        </a>
       				</td>
				</tr>
				@endforeach
			</table>
		</div>
		<div class="col-md-12">
			<h5>Your Last 10 Controlling Sessions</h5>
			<table class="table table-bordered table-condensed table-striped">
				<tr>
					<td>Position</td>
					<td>Sign On</td>
					<td>Sign Off</td>
					<td>Duration</td>
				</tr>
				@foreach ($hours as $item)
				<tr>
					<td>{{ $item->position }}</td>
					<td>{{$item->time_start->format('F j, Y')}}<br />{{$item->time_start->format('H:i:s')}}</td>
					<td>{{$item->time_end->format('F j, Y')}}<br />{{$item->time_end->format('H:i:s')}}</td>
					<td>{{ sec2hms($item->time_end->timestamp - $item->time_start->timestamp) }}</td>
				</tr>
				@endforeach
			</table>
		</div>
	</div>
</div>

@stop