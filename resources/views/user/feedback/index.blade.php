@extends('layouts.user')

@section('title', 'Feedback Details')

@section('content')

<div class="panel panel-default">
  <div class="panel-heading">
    <h4>Feedback Details</h4>
  </div>
  <div class="panel-body">
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Submitter:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{ $feedback->anonymous ? 'Anonymous' : $feedback->submitter_name }}</div>
    </div>
    <div class="form-group">
      <label class="control-label">Date/Time:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{$feedback->created_at->format('F j, Y')}}<br />{{$feedback->created_at->format('H:i:s')}}</td></div>
    </div>
    <div class="form-group">
      <label class="control-label">Rating:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{ $ratings[$feedback->rating] }}</div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Comments:</label><br />
      @if(!$feedback->comments)
        <div class="well form-control-static" style="padding: 10px;">No comments.</div>
      @else
        <div class="well form-control-static" style="padding: 10px;">{!! nl2br(e($feedback->comments)) !!}</div>
      @endif
    </div>
  </div>
  </div>
</div>

@stop