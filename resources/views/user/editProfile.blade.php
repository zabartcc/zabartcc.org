@extends('layouts.user')

@section('title', 'Edit Profile')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Profile Editor</h4>
	</div>
	<div class="panel-body">
		@if ($errors->all())
        	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
		@endif
		<div class="col-sm-7">
			<form action="{{ url('dashboard/profile') }}" method="POST">
				{{ method_field('PATCH') }}
				{{ csrf_field() }}
				<div class="form-group">
					<label for="bio" class="control-label">Bio</label>
					<textarea name="bio" class="form-control" rows="10" >{{$user->bio}}</textarea>
				</div>
				<div class="form-group">
					<label for="photo" class="control-label">Profile Photo</label>
					<div class="input-group">
						<input type="text" name="photo" id="photo" class="form-control" value="{{$user->photo}}" />
						<span class="input-group-btn">
        					<button class="btn btn-primary" type="button">&nbsp;<span class="glyphicon glyphicon-refresh" onclick="changeImg('photo', 'profileImg')"></span>&nbsp;</button>
      					</span>
					</div>
				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</form>
		</div>
		<div class="col-sm-5">
			<blockquote>
				<p>To update your profile photo, enter a direct link to an image in the box and click the button. The photo will update below when successful.</p>
			</blockquote>
			<img src="{{ $user->photo }}" class="img-thumbnail" id="profileImg">
		</div>
	</div>
</div>


@stop