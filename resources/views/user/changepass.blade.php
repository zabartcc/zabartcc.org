@extends('layouts.user')

@section('title', 'Change My Password')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Change My Password</h4>
	</div>
	<div class="panel-body">
		@if ($errors->all())
        	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
		@endif
        @if(Session::has('error'))
        	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ Session::get('error') }}</div>
		@endif
		<div class="col-sm-7">
			<form action="{{ url('dashboard/changepass') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="oldpass" class="control-label">Current Password</label>
					<input type="password" name="oldpass" class="form-control" />
				</div>
				<hr />
				<div class="form-group">
					<label for="newpass" class="control-label">New Password</label>
					<input type="password" name="newpass" class="form-control" />
				</div>
				<div class="form-group">
					<label for="newpass_confirmation" class="control-label">Verify New Password</label>
					<input type="password" name="newpass_confirmation" class="form-control" />
				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</form>
		</div>
		<div class="col-sm-5">
			<blockquote>
				<p>To update your password, please enter your current password, then enter your new password twice for verification.</p>
			</blockquote>
		</div>
	</div>
</div>


@stop