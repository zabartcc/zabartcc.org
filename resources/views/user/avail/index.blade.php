@extends('layouts.user')

@section('title', 'Training Scheduler')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading" style="padding-bottom: 12px;">
		<a href="{{ url('/dashboard/avail/create') }}" class="btn btn-sm btn-success pull-right"><i class="fa fa-plus-circle"></i> New Request</a><h3>Your Training Requests</h3>
	</div>
	<div class="panel-body">
	<div class="table-responsive">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
        		    	<th>Start Time</th>
        		    	<th>End Time</th>
        		    	<th>Options</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($avail->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="4">No session requests to show.</td>
              </tr>
            @endif
       			@foreach ($avail as $item)
       				<tr>
       					<td style="vertical-align: middle;">{{$item->time_start->format('F j, Y, H:i')}}</td>
                		<td style="vertical-align: middle;">{{$item->time_end->format('F j, Y, H:i')}}</td>
                		<td><a class="btn btn-danger pull-right" href="{{ url('/dashboard/avail/delete', $item->id) }}"><i class="fa fa-trash"></i> Delete Request</a></td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>

@stop