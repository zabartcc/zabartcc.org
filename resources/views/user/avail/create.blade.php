@extends('layouts.user')

@section('title', 'Create Training Request')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Create Training Request</h3>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('dashboard/avail') }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
                	<label for="time_start" class="control-label">Start Time (Zulu)</label>
                	<div class="input-group date form_datetime" data-link-field="time_start">
                    	<input class="form-control" name='time_start' type="text" value="{{ old('time_start') }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
				<div class="form-group">
                	<label for="time_end" class="control-label">End Time (Zulu)</label>
                	<div class="input-group date form_datetime" data-link-field="time_end">
                    	<input class="form-control" name='time_end' type="text" value="{{ old('time_end') }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
            	<div class="form-group">
            		<label for="milestone", class="control-label">Milestone</label>
            		<select class="form-control" name="milestone">
            			@foreach($milestones as $m)
            				@if((($um & $m->prereq) == $m->prereq) && !($m->bitwise_id & $um) || ($m->prereq == 0 && !$um))
            					<option value="{{ $m->id }}">{{ $m->name }} ({{ $m->code }})</option>
            				@endif
            			@endforeach
            		</select>
            	</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
			<div class="col-sm-5 col-sm-offset-1">
					<p>You can currently request training for the following milestones. {{-- Click to view an outline of the milestone. --}}</p>
					<ul>
						@foreach($milestones as $m)
            				@if((($um & $m->prereq) == $m->prereq) && !($m->bitwise_id & $um) || ($m->prereq == 0 && !$um))
            					{{-- <a href="{{ url('/dashboard/milestone', $m->id) }}"><li>{{ $m->code }} - {{ $m->name }}</li></a> --}}
                                <li>{{ $m->code }} - {{ $m->name }}</li>
            				@endif
            			@endforeach
					</ul>
                    <hr />
                    <h4 style="color:red;">Please Note:</h4><br />
                    <p>Training requests are just that - requests. There is no guarantee that your session will be picked up by a member of the training staff. If you make a request and a training staff member picks it up, you are expected to show up. Treat this like your availability - instructors have the option to modify the training times when they pick up the session.</p>
			</div>	
		</form>
	</div>
</div>

<script type="text/javascript">
$('.form_datetime').datetimepicker({
        //language:  'fr',
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 0,
        format: 'yyyy-mm-dd hh:ii:00'
    });
</script>


@stop