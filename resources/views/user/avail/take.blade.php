@extends('layouts.user')

@section('title', 'Take Session')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Take Session</h4>
	</div>
	<div class="panel-body">
    <form class="form-horizontal" action="{{ url('dashboard/avail/take', $session->id) }}" method="POST">
      {{ csrf_field() }}
      @if ($errors->all())
        <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
      <div class="col-sm-6">
        <div class="form-group">
          <label for="ins" class="control-label">Instructor</label>
          <input type="text" name="ins" id="ins" value="{{$session->user->fname}} {{$session->user->lname}}" class="form-control" disabled="" />
          <input type="hidden" name="ins" id="ins" value="{{$session->ins_id}}" />
        </div>
        <div class="form-group">
          <label for="times" class="control-label">Times</label>
          <input type="text" name="times" id="times" value="{{$session->time_start->format('F j, Y, H:i')}} - {{$session->time_end->format('F j, Y, H:i')}}" class="form-control" disabled="" />
          <input type="hidden" name="time_start" id="time_start" value="{{$session->time_start}}" />
          <input type="hidden" name="time_end" id="time_end" value="{{$session->time_end}}" />
        </div>
        <div class="form-group">
          <label for='position' class="control-label">Position</label>
          <select name="position" id="position" class="form-control">
            <option disabled="" selected="">Please Select...</option>
            <option value="1">Clearance Delivery</option>
            <option value="2">Ground</option>
            <option value="3">Tower</option>
            <option value="4">Approach/Departure</option>
            <option value="5">Center</option>
          </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </form>
	</div>
</div>

@stop