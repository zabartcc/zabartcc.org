@extends('layouts.user')

@section('title', 'Training Session')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Training Session</h4>
	</div>
	<div class="panel-body">
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Instructor:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{ nameOrCid($training, 'ins', 'ins_id')}}</div>
    </div>
    <div class="form-group">
      <label class="control-label">Position:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{ $training->milestone_id == -1 ? $training->present()->trainingPositions : $training->milestone->name }}</div>
    </div>
    <div class="form-group">
      <label class="control-label">Session Time:</label><br />
      <div class="well form-control-static" style="padding: 10px;">{{$training->time_start->format('F j, Y, H:i')}} - {{$training->time_end->format('F j, Y, H:i')}}</td></div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Notes:</label><br />
      @if(!$training->notes)
        <div class="well form-control-static" style="padding: 10px;">No training notes.</div>
      @else
        <div class="well form-control-static" style="padding: 10px;">{!! nl2br(e(str_replace('<br />', '', $training->notes))) !!}</div>
      @endif
    </div>
  </div>
	</div>
</div>

@stop