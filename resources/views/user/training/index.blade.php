@extends('layouts.user')

@section('title', 'Your Training Sessions')

@section('content')



<div class="panel panel-default">
  <div class="panel-heading">
  <h4>Your Upcoming Training Sessions</h4>
  </div>
  <div class="panel-body">
  <div class="table-responsive">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
      <table class="table table-condensed table-striped table-bordered">
        <thead class="thead-inverse">
          <tr>
                  <th>Milestone</th>
                  <th>Start Time</th>
                  <th>End Time</th>
                  <th>Instructor</th>
                  <th>Options</th>
              </tr>
            </thead>
            <tbody>
            @if($otraining->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No upcoming training sessions to display.</td>
              </tr>
            @endif
            @foreach ($otraining as $session)
              <tr>
                <td style="vertical-align: middle;">{{ $session->milestone_id == -1 ? $session->present()->trainingPositions : $session->milestone->name }}</td>
                <td style="vertical-align: middle;">{{$session->time_start->format('F j, Y, H:i')}}</td>
                <td style="vertical-align: middle;">{{$session->time_end->format('F j, Y, H:i')}}</td>
                <td style="vertical-align: middle;">{{ nameOrCid($session, 'ins', 'ins_id')}}</td>
                <td style="text-align: right;">
                  <a class="nohover" title="View session details" href="{{ url('/dashboard/training', $session->id) }}">
                    <span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-search"></span>
                  </a>
                </td>
              </tr>
            @endforeach
              </tbody>
      </table>
    </div>
  </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">
	<h4>Your Completed Training Sessions</h4>
	</div>
	<div class="panel-body">
	<div class="table-responsive">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
                  <th>Milestone</th>
        		    	<th>Start Time</th>
        		    	<th>End Time</th>
        		    	<th>Instructor</th>
        		    	<th>Options</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($training->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No completed training sessions to display.</td>
              </tr>
            @endif
       			@foreach ($training as $session)
       				<tr>
                <td style="vertical-align: middle;">{{ $session->milestone_id == -1 ? $session->present()->trainingPositions : $session->milestone->name }}</td>
       					<td style="vertical-align: middle;">{{$session->time_start->format('F j, Y, H:i')}}</td>
                <td style="vertical-align: middle;">{{$session->time_end->format('F j, Y, H:i')}}</td>
                <td style="vertical-align: middle;">{{ nameOrCid($session, 'ins', 'ins_id')}}</td>
                <td style="text-align: right;">
                  <a class="nohover" title="View session details" href="{{ url('/dashboard/training', $session->id) }}">
                    <span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-search"></span>
                  </a>
                </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">
  <h4>Training Requests Not Fulfilled</h4>
  </div>
  <div class="panel-body">
  <div class="table-responsive">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
      <table class="table table-condensed table-striped table-bordered">
        <thead class="thead-inverse">
          <tr>
                  <th>Milestone</th>
                  <th>Start Time</th>
                  <th>End Time</th>
              </tr>
            </thead>
            <tbody>
            @if($requests->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No training requests to display.</td>
              </tr>
            @endif
            @foreach ($requests as $session)
              <tr>
                <td style="vertical-align: middle;">{{ $session->milestone_id == -1 ? $session->present()->trainingPositions : $session->milestone->name }}</td>
                <td style="vertical-align: middle;">{{$session->time_start->format('F j, Y, H:i')}}</td>
                <td style="vertical-align: middle;">{{$session->time_end->format('F j, Y, H:i')}}</td>
              </tr>
            @endforeach
              </tbody>
      </table>
    </div>
  </div>
</div>

@stop