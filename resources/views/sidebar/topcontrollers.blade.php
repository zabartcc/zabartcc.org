<h5>Top Controllers - {{ $month }}</h5>
<table class="table table-condensed table-hover">
    <tr>
    	<th style="border-top: 0px;">Controller</th>
    	<th style="border-top: 0px;">Time</th>
    </tr>
    @if(!$times)
    <tr>
		<td colspan="2" style="text-align: center;"><em>No controlling sessions logged this month. Go control already!</em></td>
	</tr>
    @endif
	@foreach($times as $c => $t)
	<tr>
		<td>{{ nameByCid($c) }}</td>
		<td>{{ sec2hms($t)}}</td>
	</tr>
	@endforeach
</table>