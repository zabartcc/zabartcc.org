<h5>Controllers Online</h5>
<table class="table table-condensed table-hover">
    <tr>
    	<th style="border-top: 0px;">Controller</th>
    	<th style="border-top: 0px;">Position</th>
    </tr>
    @if($atc->isEmpty())
    	<tr>
			<td colspan="2" style="text-align: center;"><em>No controllers online.</em></td>
		</tr>
    @endif
	@foreach($atc as $a)
	<tr data-toggle='popover' data-html='true' data-placement='left' data-trigger='hover' data-content='{!! $a->info !!}'>
		<td><strong>{{$a->name}}</strong>/{{ $a->present()->textRating }}</td>
		<td>{{$a->pos}}</td>
	</tr>
	@endforeach
</table>
