<h5>Top Positions - {{ $month }}</h5>
<table class="table table-condensed table-hover">
    <tr>
    	<th style="border-top: 0px;">Position</th>
    	<th style="border-top: 0px;">Time</th>
    </tr>
    @if(!$times)
    <tr>
		<td colspan="2" style="text-align: center;"><em>No controlling sessions so far this month.</em></td>
	</tr>
    @endif
	@foreach($times as $c => $t)
	<tr>
		<td>{{ shortPos2Long($c) }}</td>
		<td>{{ sec2hms($t)}}</td>
	</tr>
	@endforeach
</table>