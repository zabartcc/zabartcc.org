<h5>Departures/Arrivals</h5>
<table class="table table-condensed table-hover">
    <tr>
    	<th style="border-top: 0px;">Callsign</th>
    	<th style="border-top: 0px;">Aircraft</th>
    	<th style="border-top: 0px;">Flight</th>
    </tr>
    @if($pilots->isEmpty())
    <tr>
		<td colspan="3" style="text-align: center;"><em>No departures/arrivals online.</em></td>
	</tr>
    @endif
	@foreach($pilots as $a)
	<tr data-toggle='popover' data-html='true' data-placement='left' data-trigger='hover' data-content='{{ $a->info }}'>
		<td>{{$a->pos}}</td>
		<td>{{$a->aircraft}}</td>
		<td>{{ $a->dep }} - {{ $a->dest }}</td>
	</tr>
	@endforeach
</table>

{{-- <h5>Overflights in ZAB Airspace</h5>
<table class="table table-condensed table-hover">
    <tr>
    	<th style="border-top: 0px;">Callsign</th>
    	<th style="border-top: 0px;">Aircraft</th>
    	<th style="border-top: 0px;">Flight</th>
    </tr>
    @if($overflights->isEmpty())
    <tr>
		<td colspan="3" style="text-align: center;"><em>No overflights within ZAB.</em></td>
	</tr>
    @endif
	@foreach($overflights as $a)
	<tr data-toggle='popover' data-html='true' data-placement='left' data-trigger='hover' data-content='{{ $a->info }}'>
		<td>{{$a->pos}}</td>
		<td>{{$a->aircraft}}</td>
		<td>{{ $a->dep }} - {{ $a->dest }}</td>
	</tr>
	@endforeach
</table> --}}