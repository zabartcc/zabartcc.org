@extends('layouts.baremaster')

@section('title', '404 - Not Found!')

@section('content')

<div class="panel panel-default">
	@include('layouts.404')
</div>

@stop