@extends('layouts.master')

@section('title', 'Documents')

@section('content')

<style type="text/css">
	.panel-default > .dark {
		background-color: #ddd;
	}

</style>

@foreach($category as $cat)
	@if($cat->document_count)
		<div class="panel panel-default">
			<div class="panel-heading dark">
				<h3>{{$cat->name}}</h3>
			</div>
			<div class="list-group">
				@foreach($cat->document as $document)
					<a class="list-group-item" href="{{ URL::to('documents', $document->uri_slug) }}">
						<span class="pull-right"><em>Last Updated: {{$document->updated_at}}</em></span>
						<h4 class="list-group-item-heading">{{$document->name}}</h4>
						<p class="list-group-item-text">{!! nl2br(e($document->description)) !!}</p>
					</a>
				@endforeach
			</div>
		</div>
	@endif
@endforeach

<script>
	$('#documentsNav').addClass("active");

	$('.list-group-item').hover(
		function () {
			$(this).addClass('active');
		},
		function () {
			$(this).removeClass('active');
		}
		);
</script>

@stop