@extends('layouts.master')

@section('title', $doc->name.' - Documents')

@section('content')

<style>

body {
    counter-reset: h2
}

.document h2 {
    counter-reset: h3;
	font-weight: 700;
}

.document h2:first-of-type {
	margin-top: 0;
	font-size: 32px;
}

.document h3 {
    counter-reset: h4;
	margin-bottom: 10px;
	font-weight: 700;
	font-size: 24px;
}

.document h4 {
    counter-reset: h5;
	margin-bottom: 10px;
	font-weight: 700;
	font-size: 20px;
}

.document h5 {
	margin-bottom: 10px;
	font-weight: 700;
	border-bottom: none;
	font-style: normal;
	font-size: 16px;
}

.document h2:before {
    counter-increment: h2;
    content: counter(h2) ". "
}

.document h3:before {
    counter-increment: h3;
    content: counter(h2) "." counter(h3) ". "
}

.document h4:before {
    counter-increment: h4;
    content: counter(h2) "." counter(h3) "." counter(h4) ". "
}

.document h5:before {
    counter-increment: h5;
    content: counter(h2) "." counter(h3) "." counter(h4) "." counter(h5) ". "
}


</style>

<div class="panel panel-default">
	<div class="panel-heading dark">
		<h3>{{$doc->name}}</h3>
		<span>Last Updated: <strong>{{ $doc->updated_at }}</strong></span>
	</div>
	<div class="panel-body document">
		{!! $doc_content !!}
	</div>
</div>

@stop