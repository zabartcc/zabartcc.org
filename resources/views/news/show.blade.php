@extends('layouts.master')

@section('title', 'News Item')

@section('content')
<div class="panel panel-default">
    @if(!$news)
        @include('layouts.404')
    @else
    <div class="panel-heading">
        <h3>
            {{ $news->title }}
        </h3>
        <h4>
            Posted by: <strong>{{$news->user->fname}} {{$news->user->lname}}</strong><span class="pull-right">{{$news->created_at->format('F j, Y')}}</span>
        </h4>
    </div>
    <div class="panel-body">
        <p>{!! nl2br(e($news->body)) !!}</p>
    </div>
    @endif
</div>

<script>
    $('#newsNav').addClass("active");
</script>

@stop