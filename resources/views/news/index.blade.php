@extends('layouts.master')

@section('title', 'News')

@section('content')
<div class="panel panel-default">
    @if($news->isEmpty())
        <div class="panel-heading">
            <h3>
                Oops! No News Items :(
            </h3>
        </div>
        <div class="panel-body">
            <p>We're sorry, but unfortunately we could not find any news items to display. If you believe that you are receiving this message in error, please email the <a href="mailto:web@zabartcc.org">webmaster</a>.</p>
        </div>
    @endif
    @foreach ($news as $item)
    <div class="panel-heading" @unless($loop->first) style="border-top: 1px solid #ddd;" @endif>
        <h3>            
            <a href="{{ url('/news', $item->id) }}">{{ $item->title }}</a>
        </h3>
        <h4>
            Posted by: <strong>{{$item->user->fname}} {{$item->user->lname}}</strong><span class="pull-right">{{$item->created_at->format('F j, Y')}}</span>
        </h4>
    </div>
    <div class="panel-body">
        <p>{!! nl2br(e($item->body)) !!}</p>
    </div>
    @endforeach
</div>

<script>
    $('#newsNav').addClass("active");
</script>

@stop