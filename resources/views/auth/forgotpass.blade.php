@extends('layouts.master')

@section('title', 'Forgot Password')

@section('content')
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Forgot Password</h4></div>
                <div class="panel-body">
                    @if ($errors->all())
                        <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/forgotpass') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Controller ID</label>

                            <div class="col-md-6">
                                <input id="cid" type="text" class="form-control" name="cid" value="{{ old('cid') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
@endsection
