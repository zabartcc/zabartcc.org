@extends('layouts.master')

@section('title', 'Files')

@section('content')

<style type="text/css">
	.panel-default > .dark {
		background-color: #ddd;
	}

</style>

@foreach($category as $cat)
	<div class="panel panel-default">
		<div class="panel-heading dark">
			<h3>{{$cat->name}}</h3>
		</div>
		<div class="list-group">
			@foreach($cat->file as $file)
				<a class="list-group-item" href="{{ URL::to('files', $file->id) }}">
					<span class="pull-right"><em>Last Updated: {{$file->updated_at}}</em></span>
					<h4 class="list-group-item-heading">{{$file->name}}</h4>
					<p class="list-group-item-text">{!! nl2br(e($file->description)) !!}</p>
				</a>
			@endforeach
		</div>
	</div>
@endforeach

<script>
	$('#filesNav').addClass("active");

	$('.list-group-item').hover(
		function () {
			$(this).addClass('active');
		},
		function () {
			$(this).removeClass('active');
		}
		);
</script>

@stop