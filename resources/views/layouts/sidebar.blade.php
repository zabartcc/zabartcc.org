<div class="panel panel-default">
	<div class="panel-heading">
		<h4><strong>Who's Online?</strong></h4>
	</div>
	<div class="panel-body">
        @include('sidebar.atconline')
        @include('sidebar.pilotsonline')
  </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4><strong>ARTCC Info</strong></h4>
    </div>
    <div class="panel-body">
        @include('sidebar.topcontrollers')
        @include('sidebar.toppositions')
        @include('sidebar.tsviewer')
  </div>
</div>


<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover(); 
});
</script>