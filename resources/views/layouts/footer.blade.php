<script src="{{ URL::asset('js/dateTime.js') }}"></script>

<footer style="margin-bottom: 20px;">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 footertext">
				<hr />
				<span>Copyright &copy; {{date('Y')}} Albuquerque ARTCC</span><br />
				&nbsp;<br />
				All times Zulu | Current date and time (Zulu): <script type="text/javascript">dT();</script><br />
     			This site is not affiliated with the FAA, or any governing aviation body. All content contained herein is approved only for use on the VATSIM network.<br />
     			This site is optimized for <a href="https://mozilla.org/firefox">Mozilla Firefox</a>, <a href="https://chrome.google.com">Google Chrome</a> and (surprisingly) <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">Microsoft Edge</a>. If you are still using Internet Explorer, please consider switching to a more modern browser.<br />
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript">
	$( document ).ready(function() {
    	$('.fadeAlert').delay(5000).fadeOut(500);
    	//$('.footertext').append('Page rendered in '+<?php echo round((microtime(true) - LARAVEL_START)); ?>+'ms.');
	});
</script>
