<div class="jumbotron" style="margin-bottom: -5px; height: 250px; background-image: url({{ URL::asset('img/headerbg.jpg') }});">
	<div class="container">
		@if (!Auth::guest())
			@if($count)
				<div class="notification">
					<a href="{{ URL::to('dashboard/avail') }}" class="nohover" role="alert"><span class="glyphicon glyphicon-info-sign"></span> There {{ ($count == 1) ? 'is' : 'are' }} currently {{$count}} {{ ($count == 1) ? 'hour' : 'hours' }} of instructor availability in the next 7 days.</a>
				</div>
			@endif
		{{-- <div class="notification-red">
			<a href="{{ URL::to('dashboard/bugs') }}" class="nohover" role='alert'><span class="glyphicon glyphicon-info-sign"></span> Found a bug? Click here to report it!</a>
		</div> --}}
		@endif
		<a id="header-link" href="{{ URL::to('/') }}">
			<div class="zab-header-text">
				<h1>Albuquerque ARTCC</h1>
				<span>Air Route Traffic Control Center</span>
			</div>
		</a>
	</div>
</div>