<!DOCTYPE html>
<html>
<head>
	<!-- Boostrap setup -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>@yield('title') - Admin - Albuquerque ARTCC</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">
	<!-- Date/Time picker CSS -->
	<link rel="stylesheet" href="{{ URL::asset('/css/bootstrap-datetimepicker.css') }}">
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Date/Time Picker -->
	<script src='{{ URL::asset('js/bootstrap-datetimepicker.js') }}'></script>
</head>
<body>
	@include('layouts.header')
	@include('layouts.navbar')

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3">
				<div class="panel panel-default">
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin') }}">Admin Home</a>
					</div>
					<div class="panel-heading">
						<h4>Controller Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin/controllers/create') }}">Create New Controller</a>
						<a class="list-group-item" href="{{ url('admin/controllers/search') }}">Search For A Controller</a>
						<a class="list-group-item" href="{{ url('admin/controllers/email') }}">Controller Mass Email</a>
						<a class="list-group-item" href="{{ url('admin/controllers/activity') }}">Controller Activity Report</a>
					</div>
					<div class="panel-heading">
						<h4>Feedback Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin/feedback') }}">View Unapproved Feedback <span class="badge">{{ $fcount }}</span></a>
					</div>
					<div class="panel-heading">
						<h4>News Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin/news/') }}">Manage News</a>
						<a class="list-group-item" href="{{ url('admin/news/create') }}">Create News Item</a>
					</div>
					<div class="panel-heading">
						<h4>Event Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin/events/') }}">Manage Events</a>
						<a class="list-group-item" href="{{ url('admin/events/create') }}">Create Event</a>
					</div>
					<div class="panel-heading">
						<h4>File/Category Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('admin/category') }}">Manage Categories</a>
						<a class="list-group-item" href="{{ url('admin/files') }}">Manage Files</a>
						<a class="list-group-item" href="{{ url('admin/documents') }}">Manage Documents</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9">
				@yield('content')
			</div>
		</div>
	</div>

	@include('layouts.footer')
	<!-- Bootstrap JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>