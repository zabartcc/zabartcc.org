<div class="panel-heading">
    <h3>
        Oops! 404 - Not Found :(
    </h3>
</div>
<div class="panel-body">
    <p>This is one of those 404 thingies.</p>
    <p>We're sorry, but unfortunately we could not find the content that you requested. If you believe that you are receiving this message in error, please email the <a href="mailto:web@zabartcc.org">webmaster</a>. In the meantime, a cute pupper has been dispatched to ease your sorrows.</p>
    <a href="{{ URL::to('/') }}" class="btn btn-lg btn-primary"><span class="glyphicon glyphicon-home"></span>  Back To Home</a><br />

    <img src="{{ asset('img/dog.jpg') }}">
</div>