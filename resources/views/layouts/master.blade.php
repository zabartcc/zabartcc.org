<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130865580-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-130865580-1');
	</script>
	<!-- Boostrap setup -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>@yield('title') - Albuquerque ARTCC</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
{{-- <style type="text/css">
	/* April Fools! */
	body {
		background-color: #ff91f0;
	}
	.panel-heading {
		background-color: #af0095 !important;
		color: #fff !important;
	}
	.panel-body {
		background-color: #ffc9f7;
	}
</style> --}}
<body>
	@include('layouts.header')
	@include('layouts.navbar')

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8">
				@yield('content')
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4">
				@include('layouts.sidebar')
			</div>
		</div>
	</div>

	@include('layouts.footer')
	<!-- Bootstrap JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>