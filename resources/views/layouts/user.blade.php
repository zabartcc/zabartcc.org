<!DOCTYPE html>
<html>
<head>
	<!-- Boostrap setup -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"
>    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>@yield('title') - Controller Dashboard - Albuquerque ARTCC</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">
	<!-- Date/Time picker CSS -->
	<link rel="stylesheet" href="{{ URL::asset('/css/bootstrap-datetimepicker.css') }}">
	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://use.fontawesome.com/ed3556a1bf.js"></script>
	
	<!-- Date/Time Picker -->
	<script src='{{ URL::asset('js/bootstrap-datetimepicker.js') }}'></script>
	<script type="text/javascript">
	function changeImg(form, img)
	{
    	var x = document.getElementById(form).value;
    	document.getElementById(img).src = x;
	}
	</script>
</head>
<body>
	@include('layouts.header')
	@include('layouts.navbar')

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9">
				@yield('content')
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4>Controller Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('dashboard') }}">Dashboard Home</a>
						<a class="list-group-item" href="{{ url('dashboard/profile/edit') }}">Edit My Profile</a>
						<a class="list-group-item" href="{{ url('dashboard/changepass') }}">Change My Password</a>
					</div>
					<div class="panel-heading">
						<h4>Training Functions</h4>
					</div>
					<div class="list-group">
						<a class="list-group-item" href="{{ url('dashboard/avail') }}">Request a Training Session</a>
						<a class="list-group-item" href="{{ url('dashboard/training') }}">View Your Training Sessions</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('layouts.footer')
	<!-- Bootstrap JS -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>