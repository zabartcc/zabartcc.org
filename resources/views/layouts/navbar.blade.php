<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div>
     	<ul class="nav navbar-nav">
        <li id="indexNav"><a href="{{ URL::to('/') }}">Home</a></li>
        <li id="newsNav"><a href="{{ URL::to('news') }}">News</a></li>
        <li id="eventsNav"><a href="{{ URL::to('events') }}">Events</a></li>
        <li id="controllersNav" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Controllers <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ URL::to('controllers') }}">Controller Roster</a></li>
            <li><a href="{{ URL::to('controllers/ins') }}">Instructional Staff</a></li>
            <li><a href="{{ URL::to('controllers/staff') }}">Staff Roster</a></li>
          </ul>
        </li>
        <li id="feedbackNav"><a href="{{ URL::to('feedback') }}">Feedback</a></li>
        <li id="documentsNav"><a href="{{ URL::to('documents') }}">Documents</a></li>
        <li id="filesNav"><a href="{{ URL::to('files') }}">Files</a></li>
        <li><a href="{{ URL::to('https://community.zabartcc.org') }}">Forums</a></li>
        {{-- <li><a href="{{ URL::to('https://tanmac.zabartcc.org') }}">TANMAC</a></li> --}}
     	</ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
            <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-lock"></span> Login <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <div style="padding: 10px 20px; min-width: 300px;">
                  <form method="POST" action="{{ url('/login') }}">
                    <input type="hidden" name="url" value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
                    {{ csrf_field() }}
                    <div class="form-group">  
                      <label for="cid" class="control-label">Controller ID</label>
                      <input type="text" name="cid" id="cid" class="form-control">
                    </div>
                    <div class="form-group">
                      <label for="password" class="control-label">ZAB Password</label>
                      <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">Login</button>
                    <a href="{{ url('/forgotpass') }}" class="pull-right" style="margin-top: 5px;">Forgot Password?</a>
                  </form>
                </div>
              </ul>
            </li>
        @else
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
               <span class="glyphicon glyphicon-user"></span> Logged in as: <strong>{{ Auth::user()->lname }}, {{ Auth::user()->fname }}</strong> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="{{ url('/dashboard') }}">Controller Dashboard</a></li>
              <li><a href="{{ url('/dashboard/changepass') }}">Change My Password</a></li>
              @if ( App\User::isStaff(Auth::user()) || App\User::isIns(Auth::user()) )
                <li class="divider"></li>
                <li><a href="https://sunport.zabartcc.org/mail">Webmail</a></li>
                @if (App\User::isIns(Auth::user()))
                <li><a href="{{ url('/ins') }}">Instructor Dashboard</a></li>
                @endif
                @if (App\User::isStaff(Auth::user()))
                <li><a href="{{ url('/admin') }}">Admin Dashboard</a></li>
                @endif
              @endif
              <li class="divider"></li>
              <li><a href="{{ url('/logout') }}">Logout</a></li>
            </ul>
          </li>
        @endif
      </ul>
   	</div>
	</div>
</nav>