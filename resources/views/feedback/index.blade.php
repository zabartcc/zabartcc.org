@extends('layouts.master')

@section('title', 'Controller Feedback')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4>            
            Submit Feedback
        </h4>
    </div>
    <div class="panel-body">
        <form class="form-horizontal" action="{{ url('feedback') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if ($errors->all())
                <div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
            @endif
            <input type="hidden" name="anonymous" value="-1">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="fname" class="control-label">Your Name</label>
                    <input required class="form-control" type="text" id="fname" name="fname" value="{{ old('fname') }}"><br />
                    <input required class="form-control" type="text" id="lname" name="lname" value="{{ old('lname') }}">
                </div>
                <div class="form-group">
                    <label for="scid" class="control-label">Your CID</label>
                    <input required class="form-control" type="text" id="scid" name="scid" value="{{ old('scid') }}">
                </div>
                <div class="form-group">
                    <label for="email" class="control-label">Your Email</label>
                    <input class="form-control" type="text" id="email" name="email" value="{{ old('email') }}">
                </div>
                <hr />
                <div class="form-group">
                    <label for="controller" class="control-label">Controller</label>
                    <select required name="controller" id="controller" class="form-control">
                        <option selected disabled>Please select a controller...</option>
                        @foreach ($controllers as $controller)
                            <option value="{{ $controller->cid }}">{{ $controller->lname }}, {{ $controller->fname }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="rating" class="control-label">Rating</label>
                    <select required name="rating" id="rating" class="form-control">
                        <option selected disabled>Please select a rating...</option>
                        <option value="5">Excellent</option>
                        <option value="4">Above Average</option>
                        <option value="3">Average</option>
                        <option value="2">Below Average</option>
                        <option value="1">Poor</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="comments" class="control-label">Comments</label>
                    <textarea name="comments" id="comments" class="form-control">{{ old('comments') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="anonymous" class="control-label">Remain Anonymous?</label><br />
                    <input style="margin: 5px;" type="checkbox" id="anonymous" name="anonymous">
                </div>
                <div class="form-group">
                    <label for="comments" class="control-label">CAPTCHA</label>
                    {!! Recaptcha::render() !!}
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#feedbackNav').addClass("active");
</script>

@stop