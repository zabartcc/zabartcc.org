@extends('layouts.admin')

@section('title', 'Manage Categories')

@section('content')

<script src="{{ URL::asset('js/ui.js') }}"></script>
<script src="{{ URL::asset('js/touch.js') }}"></script>

<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
      	<div>
      		Are you sure you wish to delete this category? In addition to the category, all associated files will be deleted as well.<br />
            <br />
            This action is NOT reversable.<br /><br />
            <form action="{{ URL::to('admin/category/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="catidDelete" name='catid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this category</button>
            </form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Manage Categories</h4>
	</div>
	<div class="panel-body">
			@if ($errors->all())
            	<div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
		<div class="row">
			<div class="col-md-6">
				<h5>Add File Category</h5>
				<div class="col-xs-12">
					<form class="form-horizontal" action="{{ URL::to('/admin/category') }}" method="POST">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="name" class="control-label">Name</label>
							<input type="text" name="name" id="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="access" class="control-label">Access Level</label>
							<select name="access" class="form-control">
								<option value="1">Public</option>
								<option value="2">Instructor</option>
								<!--<option value="3">Admin</option>-->
							</select>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Add</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>Any category added will be inserted at the top of the sort order. You can reorder the cateogries by clicking the reorder icon and then dragging them to the order you desire.</p>
				</blockquote>
			</div>
		</div>
		<h5 style="padding-bottom: 2px;">Modify Categories</h5>
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="col-md-6">
			<h6 style="border-bottom: 1px solid #ddd; padding-bottom: 3px;">Public Categories
				<a class="nohover pull-right" title="Enable/Disable Sort Mode" id="sortp" href="">
                    <span id="spansortp" style="color: #000;" class="glyphicon glyphicon-th-list"></span>
                </a>
			</h6>
			<ul class="list-group" id="sortableP">
				@foreach ($categoryPublic as $cat)
					<li id="sort_{{ $cat->id }}" class="list-group-item">
						{{ $cat->name }}
						<a class="pull-right nohover cursor" title="Delete this category" data-catid='{{$cat->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
						  <span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
						</a>
					</li>
				@endforeach
			</ul><button id="updateP" class="btn btn-default">Save</button>
		</div>
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<div class="col-md-6">
			<h6 style="border-bottom: 1px solid #ddd; padding-bottom: 3px;">INS Categories
				<a class="nohover pull-right" title="Enable/Disable Sort Mode" id="sorti" href="">
                    <span id="spansorti" style="color: #000;" class="glyphicon glyphicon-th-list"></span>
                </a>
			</h6>
			<ul class="list-group" id="sortableI">
				@foreach ($categoryIns as $cat)
					<li id="sort_{{ $cat->id }}" class="list-group-item">
						{{ $cat->name }}
						<a class="pull-right nohover cursor" title="Delete this category" data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
						  <span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
						</a>
					</li>
				@endforeach
			</ul><button id="updateI" class="btn btn-default">Save</button>
		</div>
		<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
		<!-- <div class="col-md-4">
			<h6 style="border-bottom: 1px solid #ddd; padding-bottom: 3px;">Admin Categories
				<a class="nohover pull-right" title="Enable/Disable Sort Mode" id="sorta" href="">
                    <span id="spansorta" style="color: #000;" class="glyphicon glyphicon-th-list"></span>
                </a>
			</h6>
			<ul class="list-group" id="sortableA">
				@foreach ($categoryAdmin as $cat)
					<li id="sort_{{ $cat->id }}" class="list-group-item">{{ $cat->name }}</li>
				@endforeach
			</ul><button id="updateA" class="btn btn-default">Save</button>
		</div> -->
	</div>
</div>

<script type="text/javascript">
    //Modals
    $('#modalButton').click( function (e) {
	   e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var catid = $(this).data('catid');
        $('#catidDelete').val(catid);
        
    });
	//Helper functions to allow queuing of class stuffs
	$.fn.queueAddClass = function(className) {
	    this.queue('fx', function(next) {
	        $(this).addClass(className);
	        next();
	    });
	    return this;
	};
	$.fn.queueRemoveClass = function(className) {
	    this.queue('fx', function(next) {
	        $(this).removeClass(className);
	        next();
	    });
	    return this;
	};
	$.fn.queueHtml = function(className) {
	    this.queue('fx', function(next) {
	        $(this).html(className);
	        next();
	    });
	    return this;
	};
	//OnLoad, make all 3 sortable lists sortable elements, then disable sorting.
	$( function() {
		$( "#sortableP, #sortableI, #sortableA" ).sortable();
		$( "#sortableP, #sortableI, #sortableA" ).sortable("disable");
	});
	//Toggle the sortability
	function sortToggle(iconid, listid) {
		if($(listid).sortable("option", "disabled")) {
			//sorting is disabled, enable it
			$(listid).sortable("enable");
			//Change the icon to green
			$(iconid).css('color', 'green');
			//Change the list items to green
			$(listid + '> .list-group-item').addClass('list-group-item-success');
			//Hide any links (edit/delete)
			$(listid + '> .list-group-item > a').hide();
		}
		else {
			//sorting is enabled, disable it
			$(listid).sortable("disable");
			$(iconid).css('color', 'black');
			$(listid + '> .list-group-item').removeClass('list-group-item-success');
			$(listid + '> .list-group-item > a').show();
		}
		return false;
	};
	//Update the sort order in the DB
	function updateSort(listid, iconid, buttonid) {
		$(buttonid).addClass('btn-primary').html('Saving...');
		var url = "<?php echo url('/admin/category/sort'); ?>";
		//get new sort order into array
		var sorted = $(listid).sortable("toArray");
		//AJAX call to method to update sort order in DB
		$.ajax({
    		type: 'POST',
	        url: url,
    		data: {sort : sorted},
    		//if successfull
    		success: function() {
    			//disable sorting, update save button to show success, then wait 2 sec and reset save button back to page load status
				$(listid).sortable("disable");
				$(iconid).css('color', 'black');
				$(listid + '> .list-group-item').removeClass('list-group-item-success');
				$(listid + '> .list-group-item > a').show();
				$(buttonid).html('Saved!').delay(2000).queueRemoveClass('btn-primary').queueHtml('Save');
    		}    
		});
	}
	/*
	*
	* ALL JQUERY FROM HERE IS FOR "PUBLIC" FILES
	*
	*/
	$('#sortp').click( function (e) {
		sortToggle('#spansortp', '#sortableP');
		e.preventDefault();
	});
	$('#updateP').click( function () {
		updateSort('#sortableP', '#spansortp', this);
	});
	/*
	*
	* ALL JQUERY FROM HERE IS FOR "INS" FILES
	*
	*/
	//When the sort enable/disable icon is clicked
	$('#sorti').click( function (e) {
		sortToggle('#spansorti', '#sortableI');
		e.preventDefault();
	});
	//When the save button is clicked
	$('#updateI').click( function () {
		updateSort('#sortableI', '#spansorti', this);
	});
	/*
	*
	* ALL JQUERY FROM HERE IS FOR "ADMIN" FILES
	*
	*/
	//When the sort enable/disable icon is clicked
	$('#sorta').click( function (e) {
		sortToggle('#spansorta', '#sortableA');
		e.preventDefault();
	});
	//When the save button is clicked
	$('#updateA').click( function () {
		updateSort('#sortableA', '#spansorta', this);
	});
</script>

@stop