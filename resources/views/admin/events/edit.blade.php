@extends('layouts.admin')

@section('title', 'Update Event')

@section('content')

<!-- File Replace Modal -->
<div id="file_modal" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Replace Image</h4>
      </div>
      <div class="modal-body">
      	<div style="padding: 20px;">
      		<form class="form-horizontal" action="{{ url('/admin/events/edit/file', $event->id) }}" method="POST" enctype="multipart/form-data">
      			{{csrf_field()}}
            	<div class="form-group">
    				<label for="img">Image</label>
    				<input type="file" name="img" id="img">
  				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-upload"></span> Upload</button>
				</div>
      		</form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Update Event - {{$event->title}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/events/edit', $event->id) }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
					<label for="title" class="control-label">Title:</label>
					<input type="text" name="title" id="title" placeholder="Title" value="{{$event->title}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="content" class="control-label">Content</label>
					<textarea name="content" class="form-control" rows="10">{{$event->content}}</textarea>
				</div>
				<div class="form-group">
                	<label for="time_start" class="control-label">Event Start Time</label>
                	<div class="input-group date form_datetime" data-link-field="time_start">
                    	<input class="form-control" name='time_start' type="text" value="{{ $event->time_start }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
				<div class="form-group">
                	<label for="time_end" class="control-label">Event End Time</label>
                	<div class="input-group date form_datetime" data-link-field="time_end">
                    	<input class="form-control" name='time_end' type="text" value="{{ $event->time_end }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
            	<div class="form-group">
    				<label for="img">Image</label><button data-toggle='modal' data-target='#file_modal' id='modalButton' class="pull-right btn btn-sm btn-primary">Replace Image</button>
    				<img class="center-block img-responsive" src="{{ URL::to('/') }}/storage/{{ $event->image }}" alt="{{ $event->title }}" style="margin-bottom: 15px; margin-top: 15px;" />
  				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>No HTML will be allowed in the body of your news post. Line breaks will be preserved.</p>
				</blockquote>
			</div>	
	</div>
</div>

<script type="text/javascript">
$('#modalButton').click( function (e) {

	e.preventDefault();

});


$('.form_datetime').datetimepicker({
        //language:  'fr',
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 0,
        format: 'yyyy-mm-dd hh:ii:ss'
    });
</script>



@stop