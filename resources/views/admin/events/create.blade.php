@extends('layouts.admin')

@section('title', 'Create Event')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Create Event</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/events') }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
					<label for="title" class="control-label">Title</label>
					<input type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Title" class="form-control">
				</div>
				<div class="form-group">
					<label for="body" class="control-label">Description</label>
					<textarea name="body" class="form-control" rows="10">{{ old('body') }}</textarea>
				</div>
				<div class="form-group">
                	<label for="time_start" class="control-label">Event Start Time</label>
                	<div class="input-group date form_datetime" data-link-field="time_start">
                    	<input class="form-control" name='time_start' type="text" value="{{ old('time_start') }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
				<div class="form-group">
                	<label for="time_end" class="control-label">Event End Time</label>
                	<div class="input-group date form_datetime" data-link-field="time_end">
                    	<input class="form-control" name='time_end' type="text" value="{{ old('time_end') }}">
						<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                	</div>
            	</div>
            	<div class="form-group">
    				<label for="img">Image</label>
    				<input type="file" name="img" id="img" value="{{ old('file') }}">
  				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>No HTML will be allowed in the body of your event description. Line breaks will be preserved.</p>
				</blockquote>
			</div>	
	</div>
</div>



<script type="text/javascript">
$('.form_datetime').datetimepicker({
        //language:  'fr',
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 0,
        format: 'yyyy-mm-dd hh:ii:ss'
    });
</script>

@stop