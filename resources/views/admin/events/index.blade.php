@extends('layouts.admin')

@section('title', 'All Events')

@section('content')

<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
        <div>
          Are you sure you wish to delete this event?<br />
            <br />
            This action is NOT reversable.<br /><br />
            <form action="{{ URL::to('admin/events/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="eventidDelete" name='eventid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this event</button>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Current Events</h4>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
        		    	<th>Title</th>
        		    	<th>Date Posted</th>
                  <th>Event Time</th>
        		    	<th>Poster</th>
        		    	<th>Options</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($events->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No events to display.</td>
              </tr>
            @endif
       			@foreach ($events as $item)
       				<tr>
       					<td>{{$item->title}}</td>
       					<td>{{$item->created_at->format('F j, Y')}}</td>
                <td>{{$item->time_start->format('F j, Y H:i')}} - {{$item->time_end->format('F j, Y H:i')}}</td>
       					<td>{{$item->user->fname}} {{$item->user->lname}}</td>
       					<td style="text-align: right;">
                        	<a class="nohover" title="Edit this event" href="{{ url('admin/events/edit', $item->id) }}">
                        		<span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-pencil"></span>
                        	</a>
                        	<a class="nohover cursor" title="Delete this event" data-eventid='{{$item->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
                        		<span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
                        	</a>
                        </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>

<script>
  
    //Modals
    $('#modalButton').click( function (e) {
     e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var eventid = $(this).data('eventid');
        $('#eventidDelete').val(eventid);
        
    });

</script>

@stop