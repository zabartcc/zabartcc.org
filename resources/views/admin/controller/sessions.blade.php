@extends('layouts.admin')

@section('title', 'Session List')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Session List - {{ $user->lname }}, {{ $user->fname }}</h4>
	</div>
	<div class="panel-body">
		Displaying all controlling sessions for controller. Rows highlighted in red are sessions less than 30 minutes.<br /><br />
		<table class="table table-condensed table-striped">
			<tr>
				<th>Position</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Controlling Time</th>
			</tr>
			@foreach($user->activity as $session)
				<tr @if($session->time_end->timestamp - $session->time_start->timestamp < 1800) style="background-color: #ff9999" @endif>
					<td>{{$session->position}}</td>
					<td>{{ $session->time_start }}</td>
					<td>{{ $session->time_end }}</td>
					<td>{{ sec2hms($session->time_end->timestamp - $session->time_start->timestamp) }}</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>

@stop