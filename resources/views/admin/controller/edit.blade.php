@extends('layouts.admin')

@section('title', 'Edit Controller')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Edit Controller - {{$controller->fname}} {{$controller->lname}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/controllers/edit') }}" method="POST">
			{{ csrf_field() }}
			<!-- Workaround for unchecked checkboxes not existing in POST array. -1 is a workaround for the method used to parse the $_POST variable, as in checking if a field is empty, the 0 value for a non-checked box causes the script to think the box is empty and exits. -->
            <input type="hidden" name="zab" value="-1" />
            <input type="hidden" name="p50app" value="-1" />
            <input type="hidden" name="app" value="-1" />
            <input type="hidden" name="p50twr" value="-1" />
            <input type="hidden" name="twr" value="-1" />
            <input type="hidden" name="p50gnd" value="-1" />
            <input type="hidden" name="gnd" value="-1" />
            <input type="hidden" name="atm" value="-1" />
            <input type="hidden" name="datm" value="-1" />
            <input type="hidden" name="ta" value="-1" />
            <input type="hidden" name="ec" value="-1" />
            <input type="hidden" name="wm" value="-1" />
            <input type="hidden" name="fe" value="-1" />
            <input type="hidden" name="ins" value="-1" />
            <input type="hidden" name="mtr" value="-1" />
            <input type="hidden" name="vis" value="-1" />
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-4">
				<div class="form-group">
					<label for="cid" class="control-label">Controller ID</label>
					<input type="text" name="cid" id="cid" value="{{$controller->cid}}" class="form-control" disabled="" />
					<input type="hidden" name="cid" id="cid" value="{{$controller->cid}}" />
				</div>
				<br />
				<div class="form-group">
					<label for="fname" class="control-label">Controller Name</label>
					<input type="text" name="fname" id="fname" value='{{$controller->fname}}' placeholder="First" class="form-control"><br/>
					<input type="text" name="lname" id="lname" value='{{$controller->lname}}' placeholder="Last" class="form-control">
				</div>
				<div class="form-group">
					<label for="email" class="control-label">Email</label>
					<input type="text" name="email" id="email" value="{{$controller->email}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="rating" class="control-label">Rating</label><br />
					<select name="rating" id="rating" class="form-control">
					@foreach ($ratings as $key => $rating)
						@if ($rating)
							@if($key+1 == $controller->rating)
								<option value="{{$key+1}}" selected="">{{$rating}}</option>
							@else
								<option value="{{$key+1}}">{{$rating}}</option>
							@endif
						@endif
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="oi" class="control-label">Operating Initials</label>
					<input type="text" name="oi" id="oi" value="{{ $controller->oi }}" class="form-control">
				</div>
				<div class="form-group">
					<label>Certifications</label>
					<table style="font-size: 0.8em;" class="table table-condensed table-striped table-bordered">
						<thead class="thead-inverse">
							<tr>
        					    <th colspan="4">Major (P50)</th>
        					    <th colspan="3">Minor</th>
        					</tr>
        					<tr>
        					    <th>ZAB</th>
        					    <th>APP</th>
        					    <th>TWR</th>
        					    <th>GND</th>
        					    <th>APP</th>
        					    <th>TWR</th>
        					    <th>GND</th>
        					</tr>
       					</thead>
       					<tbody style="text-align: center;">
       						<td><input type="checkbox" value="1" name="zab" @if ($controller->zab) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50app" @if ($controller->p50app) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50twr" @if ($controller->p50twr) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="p50gnd" @if ($controller->p50gnd) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="app" @if ($controller->app) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="twr" @if ($controller->twr) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="gnd" @if ($controller->gnd) checked="checked" @endif ></td>
            			</tbody>
					</table>
				</div>
				<div class="form-group">
					<label>Attributes</label>
					<table style="font-size: 0.8em;" class="table table-condensed table-striped table-bordered">
						<thead class="thead-inverse">
        					<tr>
        					    <th>ATM</th>
        					    <th>DATM</th>
        					    <th>TA</th>
        					    <th>EC</th>
        					    <th>WM</th>
        					    <th>FE</th>
        					    <th>MTR</th>
        					    <th>VIS</th>
        					</tr>
       					</thead>
       					<tbody style="text-align: center;">
       						<td><input type="checkbox" value="1" name="atm" @if ($controller->atm) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="datm" @if ($controller->datm) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="ta" @if ($controller->ta) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="ec" @if ($controller->ec) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="wm" @if ($controller->wm) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="fe" @if ($controller->fe) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="mtr" @if ($controller->mtr) checked="checked" @endif ></td>
       						<td><input type="checkbox" value="1" name="vis" @if ($controller->vis) checked="checked" @endif ></td>
            			</tbody>
					</table>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-7 col-sm-offset-1">
				<blockquote>
					<p>Here you can edit any property of a controller. If you need to update the CID, please email the <a href="mailto:web@zabartcc.org">webmaster</a>.</p>
				</blockquote>
			</div>	
	</div>
</div>

@stop