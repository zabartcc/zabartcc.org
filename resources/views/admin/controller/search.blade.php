@extends('layouts.admin')

@section('title', 'Search For a Controller')

@section('content')



<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
        <div>
            Are you sure you wish to delete this controller?<br />
            <br />
            This action is reversable.<br /><br />
            <form action="{{ URL::to('admin/controllers/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="cidDelete" name='cid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this controller</button>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Search For A Controller</h4>
	</div>
	<div class="panel-body">
		@if ($errors->all())
        	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
        @endif
		<div class="col-sm-4">
			<form class="form-horizontal" action="{{ url('admin/controllers/search') }}" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="search" class="control-label">Search</label>
					<input type="text" name="search" id="search" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
		<div class="col-sm-7 col-sm-offset-1">
			<blockquote>
				<p>You can search by CID, First Name, Last Name, Email or Operating Initials</p>
			</blockquote>
		</div>
	</div>
	@if (isset($search))
	<div class="panel-heading">
		<h4>Search Results</h4>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
        		    	<th rowspan="2">Name</th>
        		    	<th rowspan="2">CID</th>
        		    	<th rowspan="2">Rating</th>
                    	<th rowspan="2">Initials</th>
        		    	<th colspan="4">Major (P50)</th>
        		    	<th colspan="3">Minor</th>
        		    	<th rowspan="2">Options</th>
        			</tr>
        			<tr>
        		    	<th>ZAB</th>
        		    	<th>APP</th>
        		    	<th>TWR</th>
        		    	<th>GND</th>
        		    	<th>APP</th>
        		    	<th>TWR</th>
        		    	<th>GND</th>
        			</tr>
       			</thead>
       			<tbody>
       			@foreach ($controllers as $controller)
					<tr>
						<td><a href='{{ URL::to('/admin/controllers/edit', $controller->cid) }}'>{{ $controller->fname }} {{ $controller->lname }}</a></td>
						<td>{{ $controller->cid }}</td>
                        <td>{{ $controller->present()->textRating }}</td>
                        <td>{{ $controller->oi }}</td>
                        <td>{!! $controller->present()->displayzab !!}</td>
                        <td>{!! $controller->present()->displayp50app !!}</td>
                        <td>{!! $controller->present()->displayp50twr !!}</td>
                        <td>{!! $controller->present()->displayp50gnd !!}</td>
                        <td>{!! $controller->present()->displayapp !!}</td>
                        <td>{!! $controller->present()->displaytwr !!}</td>
                        <td>{!! $controller->present()->displaygnd !!}</td>
                        <td style="text-align: right;">
                            <a class="nohover" title="View controller activity" href="{{ url('admin/controllers/activity', $controller->cid) }}">
                                <span style="color: #002E7A; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-time"></span>
                            </a>
                            <a class="nohover" title="View controller feedback" href="{{ url('admin/feedback', $controller->cid) }}">
                                <span style="color: #002E7A; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-stats"></span>
                            </a>
                            <a class="nohover" title="Email this controller" href="mailto:{{ $controller->email }}">
                                <span style="color: #002E7A; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-envelope"></span>
                            </a>
                        	<a class="nohover" title="Edit this controller" href="{{ url('admin/controllers/edit', $controller->cid) }}">
                        		<span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-pencil"></span>
                        	</a>
                        	<a class="nohover cursor" title="Delete this controller" data-cid='{{$controller->cid}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
                        		<span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-lg glyphicon-trash"></span>
                        	</a>
                        </td>
					</tr>
				@endforeach
            	</tbody>
			</table>
		</div>
	</div>
	@endif
</div>

<script>
    
    //Modals
    $('#modalButton').click( function (e) {
       e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var cid = $(this).data('cid');
        $('#cidDelete').val(cid);
        
    });

</script>

@stop