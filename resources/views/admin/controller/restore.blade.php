@extends('layouts.admin')

@section('title', 'Restore Controller')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Restore Controller - {{ $user->fname }} {{ $user->lname }}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/controllers/restore', $user->cid) }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-4">
				<div class="form-group">
					<label for="cid" class="control-label">Controller ID</label>
					<div class="well form-control-static" style="padding: 5px 10px;">{{ $user->cid }}</div>
				</div>
				<br />
				<div class="form-group">
					<label for="fname" class="control-label">Controller Name</label>
					<div class="well form-control-static" style="padding: 5px 10px;">{{ $user->fname }}</div>
					<div class="well form-control-static" style="padding: 5px 10px;">{{ $user->lname }}</div>
				</div>
				<div class="form-group">
					<label for="email" class="control-label">Email</label>
					<input type="text" name="email" id="email" value="{{ $user->email }}" class="form-control">
				</div>
				<div class="form-group">
					<label for="rating" class="control-label">Rating</label><br />
					<select name="rating" id="rating" class="form-control">
					@foreach ($ratings as $key => $rating)
						@if ($rating)
							@if($key == $user->rating)
								<option value="{{$key}}" selected="">{{$rating}}</option>
							@else
								<option value="{{$key}}">{{$rating}}</option>
							@endif
						@endif
					@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="oi" class="control-label">Operating Initials</label>
					<input type="text" name="oi" id="oi" value="{{ $user->oi }}" class="form-control">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-7 col-sm-offset-1">
				<blockquote>
					<p>This controller is a past ZAB member. Retrieving old data.</p>
				</blockquote>
			</div>	
	</div>
</div>

@stop