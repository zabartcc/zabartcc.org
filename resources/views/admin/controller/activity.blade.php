@extends('layouts.admin')

@section('title', 'Activity Reports')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Activity Reports</h4>
	</div>
	<div class="panel-body">
		Displaying controller activity since {{ $chkdate }}.<br />
		<br />
		<table class="table table-condensed table-striped">
			<tr>
				<th>Controller</th>
				<th>CID</th>
				<th>Is Visitor</th>
				<th>Rating</th>
				<th>Controlling Time</th>
				<th>Join Date</th>
			</tr>
			@foreach($controlling_time as $cid => $ct)
				<tr @if($ct['protected']) style="background-color: #ccffcc" @elseif($ct['bad']) style="background-color: #ff9999" @endif>
					<td><a href="{{ url('/admin/controllers/activity', $cid) }}">{{ $ct['fname'] }} {{ $ct['lname'] }}</a></td>
					<td>{{ $cid }}</td>
					<td>{{ ($ct['vis']) ? "Y" : "N" }}</td>
					<td>{{ $ct['rating'] }}</td>
					<td>{{ sec2hms($ct['duration']) }}</td>
					<td>{{ $ct['created_at'] }}</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>

@stop