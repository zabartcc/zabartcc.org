@extends('layouts.admin')

@section('title', 'Email All Controllers')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Email All Controllers</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/controllers/email') }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
					<label for="subject" class="control-label">Subject</label>
					<input type="text" name="subject" id="subject" class="form-control"><br/>
				</div>
				<div class="form-group">
					<label for="email" class="control-label">Email</label>
					<textarea class="form-control" name="email" id="email" rows="10"></textarea>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>Used to send an email to all controllers on our roster. Will be prepended with "Hello, Controller Name". Only click "submit" once - it will take a few seconds to process (after all, you are sending 1 email per controller).</p>
				</blockquote>
			</div>	
	</div>
</div>

@stop