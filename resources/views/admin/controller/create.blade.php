@extends('layouts.admin')

@section('title', 'Create New Controller')

@section('content')

<script src="https://use.fontawesome.com/ed3556a1bf.js"></script>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Create A New Controller</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/controllers') }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-4">
				<div class="form-group">
					<label for="cid" class="control-label">Controller ID</label>
					<div class="input-group">
						<input type="text" value="{{ old('cid') }}" name="cid" id="cid" class="form-control" />
						<span class="input-group-btn">
        					<button class="btn btn-success" id='loadController' type="button">&nbsp;<i id="searchIcon" class="fa fa-search"></i>&nbsp;</button>
      					</span>
					</div>
				</div>
				<br />
				<div class="form-group">
					<label for="fname" class="control-label">Controller Name</label>
				<input type="text" name="fname" id="fname" value="{{ old('fname') }}" placeholder="First" class="form-control"><br/>
					<input type="text" name="lname" id="lname" value="{{ old('lname') }}" placeholder="Last" class="form-control">
				</div>
				<div class="form-group">
					<label for="email" class="control-label">Email</label>
					<input type="text" value="{{ old('email') }}" name="email" id="email" class="form-control">
				</div>
				<div class="form-group">
					<label for="rating" class="control-label">Rating</label><br />
					<select name="rating" id="rating" class="form-control">
						<option value="1">Observer</option>
						<option value="2">Student 1</option>
						<option value="3">Student 2</option>
						<option value="4">Student 3</option>
						<option value="5">Controller 1</option>
						<option value="7">Controller 3</option>
						<option value="8">Instructor</option>
						<option value="10">Senior Instructor</option>
						<option value="11">Supervisor</option>
						<option value="12">Administrator</option>
					</select>
				</div>
				<div class="form-group">
					<label for="oi" class="control-label">Operating Initials</label>
					<div class="input-group">
						<input type="text" value="{{ old('oi') }}" name="oi" id="oi" class="form-control" />
						<div class="input-group-addon"><i id="oi_icon" class="fa fa-question-circle"></i></div>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-7 col-sm-offset-1">
				<blockquote>
					<p>When you enter a Controller ID into the field and click the find button, the rest of the information will be fetched from VATSIM sources.</p>
				</blockquote>
				<blockquote>
					<p>If the controller you are adding is a visitor, you will have to manually specifiy the email.</p>
				</blockquote>
				<blockquote style="display: none;" id="bqinfo">
					<p id="pinfo"></p>
				</blockquote>
			</div>	
	</div>
</div>

<script>
$(document).ready(function(){
	$("#loadController").click(function(){
	   	$("#searchIcon").removeClass("fa-search").addClass("fa-spinner").addClass("fa-spin");
		$.ajax({
		    type: "POST",
		    url: "<?php echo url('/admin/fetchcontroller'); ?>/"+ $('#cid').val(),
		    dataType: 'text',
		    success: function(result){
		        var json = $.parseJSON(result);
		        if(json.restore) {
		            return window.location.replace("<?php echo url('/admin/controllers/restore'); ?>/"+ $('#cid').val())
		        }
		        $("#fname").val(json.fname);
		        $("#lname").val(json.lname);
		        $("#email").val(json.email);
		        $("#rating").val(json.rating);
		        $("#pinfo").html(json.srctext);
		        $('#bqinfo').show();
		    	$('#loadingGif').hide();
		    	$('#oi').focus();
				$("#searchIcon").addClass("fa-search").removeClass("fa-spinner").removeClass("fa-spin");
		    },
		    error: function(jqXHR, status, err) {
		            $("#pinfo").html("Error fetching controller from API sources. All data must be specified manually.");
					$('#bqinfo').show();
					$("#searchIcon").addClass("fa-search").removeClass("fa-spinner").removeClass("fa-spin");
		    }
		})
	});
	$("#oi").on('input', function(){
		if($('#oi').val().length < 2) {
			$('#oi_icon').removeClass().addClass('fa fa-question-circle');
			return;
		}
		$.ajax({
			type: "POST",
			url: "<?php echo url('/admin/fetchoi'); ?>/"+ $('#oi').val(),
			dataType: 'text',
			success: function(response) {
				if(response == 1) {
					$('#oi_icon').removeClass().addClass('fa fa-check')
				} else {
					$('#oi_icon').removeClass().addClass('fa fa-times')
				}
			}
		})
	});
});
</script>

@stop