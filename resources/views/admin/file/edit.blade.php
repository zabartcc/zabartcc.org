@extends('layouts.admin')

@section('title', 'Update File')

@section('content')

<!-- File Replace Modal -->
<div id="file_modal" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Replace File</h4>
      </div>
      <div class="modal-body">
      	<div style="padding: 20px;">
      		<form class="form-horizontal" action="{{ url('/admin/files/edit/file', $file->id) }}" method="POST" enctype="multipart/form-data">
      			{{csrf_field()}}
            	<div class="form-group">
    				<label for="file">File</label>
    				<input type="file" name="file" id="file">
  				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-upload"></span> Upload</button>
				</div>
      		</form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Update File - {{$file->name}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/files/edit', $file->id) }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
					<label for="name" class="control-label">Name</label>
					<input type="text" name="name" id="name" placeholder="Title" value="{{$file->name}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="description" class="control-label">Description</label>
					<textarea name="description" class="form-control" rows="10">{{$file->description}}</textarea>
				</div>
				<div class="form-group">
                    <button data-toggle='modal' data-target='#file_modal' id='modalButton' class="btn btn-primary">Replace File</button>
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>No HTML will be allowed in the body of your description. Line breaks will be preserved.</p>
				</blockquote>
			</div>
	</div>
</div>

<script type="text/javascript">
$('#modalButton').click( function (e) {

	e.preventDefault();

});
</script>



@stop
