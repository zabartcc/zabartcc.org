@extends('layouts.admin')

@section('title', 'Controller Feedback')

@section('content')


<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>All Feedback - {{ $user->fname }} {{ $user->lname }}</h4>
	</div>
	<div class="panel-body">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
                  <th>Date/Time</th>
        		    	<th>Rating</th>
        		    	<th>Submitter Details</th>
                  <th>Comments</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($feedback->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="4">No feedback to display.</td>
              </tr>
            @endif
       			@foreach ($feedback as $item)
       				<tr>
                <td>{{$item->created_at->format('F j, Y')}}<br />{{$item->created_at->format('H:i:s')}}</td>
       					<td>{{$ratings[$item->rating]}}</td>
                <td style="text-align: center;">{{$item->submitter_name}} - {{ $item->submitter_cid }}<br />
                    {{ $item->submitter_email }}<br />
                    {{ $item->submitter_ip }}<br />
                    {!! $item->anonymous ? '<strong>Anonymous</strong>' : '' !!}</td>
       					<td style="text-align: center;">{!! nl2br(e($item->comments)) !!}</td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>


<script>
  
    //Modals
    $('#modalButton').click( function (e) {
     e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var feedbackid = $(this).data('feedbackid');
        $('#feedbackidDelete').val(feedbackid);
        
    });

</script>

@stop