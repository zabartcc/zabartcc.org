@extends('layouts.admin')

@section('title', 'Unapproved Feedback')

@section('content')


<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
        <div>
          Are you sure you wish to delete this feedback? Please only do this if the feedback is spam. Poor feedback should not be deleted to protect the controller or boost ratings.<br />
            <br />
            This action is NOT reversable.<br /><br />
            <form action="{{ URL::to('admin/feedback/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="feedbackidDelete" name='feedbackid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this feedback</button>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Unapproved Feedback</h4>
	</div>
	<div class="panel-body">
      @if(Session::has('success'))
        <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
      @endif
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
        		    	<th>Controller</th>
        		    	<th>Rating</th>
        		    	<th>Submitter Details</th>
                  <th>Comments</th>
                  <th>Options</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($feedback->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="5">No unapproved feedback to display.</td>
              </tr>
            @endif
       			@foreach ($feedback as $item)
       				<tr>
                <td>{{$item->user->fname}} {{$item->user->lname}}</td>
       					<td>{{$ratings[$item->rating]}}</td>
                <td style="text-align: center;">{{$item->submitter_name}} - {{ $item->submitter_cid }}<br />
                    {{ $item->submitter_email }}<br />
                    {{ $item->submitter_ip }}<br />
                    {!! $item->anonymous ? '<strong>Anonymous</strong>' : '' !!}</td></td>
       					<td style="text-align: center;">{!! nl2br(e($item->comments)) !!}</td>
       					<td nowrap style="text-align: right;">
                        	<a class="nohover" title="Approve this feedback" href="{{ url('admin/feedback/approve', $item->id) }}">
                        		<span style="color: #5CB85C; padding: 0 4px;" class="glyphicon glyphicon-ok"></span>
                        	</a>
                        	<a class="nohover cursor" title="Delete this feedback" data-feedbackid='{{$item->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
                        		<span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
                        	</a>
                        </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>


<script>
  
    //Modals
    $('#modalButton').click( function (e) {
     e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var feedbackid = $(this).data('feedbackid');
        $('#feedbackidDelete').val(feedbackid);
        
    });

</script>

@stop