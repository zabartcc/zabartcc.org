@extends('layouts.admin')

@section('title', 'Manage Documents')

@section('content')

<script src="{{ URL::asset('js/ui.js') }}"></script>
<script src="{{ URL::asset('js/touch.js') }}"></script>

<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
      	<div>
      		Are you sure you wish to delete this document?<br />
            <br />
            This action is NOT reversable.<br /><br />
            <form action="{{ URL::to('/admin/documents/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="documentidDelete" name='fileid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this document</button>
            </form>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Manage Documents</h4>
	</div>
	<div class="panel-body">
			@if ($errors->all())
            	<div class="alert alert-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
		<div class="row">
			<div class="col-md-6">
				<h5>Create Document</h5>
				<div class="col-xs-12">
					<form class="form-horizontal" action="{{ URL::to('/admin/documents') }}" method='POST'>
						{{ csrf_field() }}
						<div class="form-group">
							<label for="name" class="control-label">Document Name</label>
							<input value="{{ old('name') }}" type="text" name="name" id="name" class="form-control">
						</div>
                        <div class="form-group">
							<label for="description" class="control-label">Document Description</label>
                            <textarea name="description" id="description" class="form-control">{{ old('description') }}</textarea>
						</div>
						<div class="form-group">
							<label for="category" class="control-label">Category</label>
							<select name="category" class="form-control">
                                <option disabled selected>Please Select...</option>
                                @foreach($categories as $item)
								    <option value="{{$item->id}}">{{ $item->name }}</option>
                                @endforeach
							</select>
						</div>
                        <div class="form-group">
							<label for="content" class="control-label">Document Content (Markdown)</label>
                            <textarea name="content" id="content" class="form-control">{{ old('content') }}</textarea>
  				        </div>
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>Any document added will be inserted at the top of the sort order. You can reorder the documents by clicking the reorder icon and then dragging them to the order you desire. If you wish to change the category that a document is under, click the edit icon.</p>
                    <p>No HTML is allowed in the document description. Line breaks will be preserved.</p>
					<p>The content of the file is written in Markdown.</p>
				</blockquote>
			</div>
		</div>
		<h5 style="padding-bottom: 2px;">Modify Documents</h5>
		@foreach($categories as $cat)
			@if($cat->document_count)
				<div class="row">
					<div class="col-md-12">
						<h6 style="border-bottom: 1px solid #ddd; padding-bottom: 3px;">{{ $cat->name }}
							<a class="nohover pull-right cursor" title="Enable/Disable Sort Mode" id="sort_toggle_{{ $cat->id }}" onclick="sortButton({{ $cat->id }})">
								<span id="icon_sort_{{ $cat->id }}" style="color: #000;" class="glyphicon glyphicon-th-list"></span>
							</a>
						</h6>
						<ul class="list-group" id="ul_sort_{{$cat->id}}">
							@foreach ($cat->document as $document)
								<li id="sort_{{ $document->id }}" class="list-group-item">
									{{ $document->name }}
									<a class="pull-right nohover cursor" title="Delete this document" data-fileid='{{$document->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
									<span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
									</a>
									<a class="pull-right nohover" title="Edit this document" href="{{ url('admin/documents/edit', $document->uri_slug) }}">
									<span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-pencil"></span>
									</a>
								</li>
							@endforeach
						</ul><button id="sort_save_{{$cat->id}}" onclick="saveList({{ $cat->id }})" class="btn btn-default">Save {{ $cat->name }}</button>
					</div>
				</div>
			@endif
        @endforeach
	</div>
</div>

<script>
    //Modals
    $('#modalButton').click( function (e) {
	   e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var fileid = $(this).data('fileid');
        $('#documentidDelete').val(fileid);
        
    });
	//Helper functions to allow queuing of class stuffs
	$.fn.queueAddClass = function(className) {
	    this.queue('fx', function(next) {
	        $(this).addClass(className);
	        next();
	    });
	    return this;
	};
	$.fn.queueRemoveClass = function(className) {
	    this.queue('fx', function(next) {
	        $(this).removeClass(className);
	        next();
	    });
	    return this;
	};
	$.fn.queueHtml = function(className) {
	    this.queue('fx', function(next) {
	        $(this).html(className);
	        next();
	    });
	    return this;
	};
	//OnLoad, make all 3 sortable lists sortable elements, then disable sorting.
	$( function() {
		$( "[id^=ul_sort]" ).sortable();
		$( "[id^=ul_sort]" ).sortable("disable");
	});
	//Toggle the sortability
	function sortToggle(iconid, listid) {
		if($(listid).sortable("option", "disabled")) {
			//sorting is disabled, enable it
			$(listid).sortable("enable");
			//Change the icon to green
			$(iconid).css('color', 'green');
			//Change the list items to green
			$(listid + '> .list-group-item').addClass('list-group-item-success');
			//Hide any links (edit/delete)
			$(listid + '> .list-group-item > a').hide();
		}
		else {
			//sorting is enabled, disable it
			$(listid).sortable("disable");
            //Change icon to black
			$(iconid).css('color', 'black');
            //Change list items back to white
			$(listid + '> .list-group-item').removeClass('list-group-item-success');
            //Show any hidden links
			$(listid + '> .list-group-item > a').show();
		}
		return false;
	};
	//Update the sort order in the DB
	function updateSort(listid, iconid, buttonid, cattext) {
		$(buttonid).addClass('btn-primary').html('Saving...');
		var url = "<?php echo url('/admin/document/sort'); ?>";
		//get new sort order into array
		var sorted = $(listid).sortable("toArray");
		//AJAX call to method to update sort order in DB
		$.ajax({
    		type: 'POST',
	        url: url,
    		data: {sort : sorted},
    		//if successfull
    		success: function() {
    			//disable sorting, update save button to show success, then wait 2 sec and reset save button back to page load status
				$(listid).sortable("disable");
				$(iconid).css('color', 'black');
				$(listid + '> .list-group-item').removeClass('list-group-item-success');
				$(listid + '> .list-group-item > a').show();
				$(buttonid).html('Saved!').delay(2000).queueRemoveClass('btn-primary').queueHtml(cattext);
    		}    
		});
	}
    
    function sortButton(catid) {
		sortToggle('#icon_sort_'+catid, '#ul_sort_'+catid);
        return false;
    }
    
    function saveList(catid) {
        updateSort('#ul_sort_'+catid, '#icon_sort_'+catid, '#sort_save_'+catid, $('#sort_save_'+catid).html());
        return false;
    }
</script>

@stop