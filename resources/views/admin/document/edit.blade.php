@extends('layouts.admin')

@section('title', 'Update Document')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Update Document - {{$doc->name}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/documents/edit', $doc->id) }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-12">
				<div class="form-group">
					<label for="name" class="control-label">Name</label>
					<input type="text" name="name" id="name" placeholder="Title" value="{{$doc->name}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="description" class="control-label">Description</label>
					<textarea name="description" class="form-control" rows="2">{{$doc->description}}</textarea>
				</div>
				<div class="form-group">
					<label for="content" class="control-label">Content (Markdown)</label>
					<textarea name="content" class="form-control" rows="50">{{$doc->content}}</textarea>
				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>
	</div>
</div>



@stop
