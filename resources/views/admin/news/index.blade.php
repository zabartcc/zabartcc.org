@extends('layouts.admin')

@section('title', 'All News Items')

@section('content')


<style>

    a.cursor:hover {
        cursor: pointer;
    }
    
</style>

<!-- Verify Delete Modal -->
<div id="verifyDelete" class="modal fade">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm Deletion</h4>
      </div>
      <div class="modal-body">
        <div>
          Are you sure you wish to delete this news item?<br />
            <br />
            This action is NOT reversable.<br /><br />
            <form action="{{ URL::to('admin/news/delete') }}" method='POST'>
                {{ csrf_field() }}
                <input type="hidden" id="newsidDelete" name='newsid' value='' />
                <button type="submit" class="btn btn-danger">I understand, delete this news item</button>
            </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close without deleting</button>
      </div>
    </div>

  </div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>All News Items</h4>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered">
				<thead class="thead-inverse">
					<tr>
        		    	<th>Title</th>
        		    	<th>Date Posted</th>
        		    	<th>Author</th>
        		    	<th>Options</th>
        			</tr>
       			</thead>
       			<tbody>
            @if($news->isEmpty())
              <tr>
                <td style="text-align: center;" colspan="4">No news items to display.</td>
              </tr>
            @endif
       			@foreach ($news as $item)
       				<tr>
       					<td>{{$item->title}}</td>
       					<td>{{$item->created_at->format('F j, Y')}}</td>
       					<td>{{$item->user->fname}} {{$item->user->lname}}</td>
       					<td style="text-align: right;">
                        	<a class="nohover" title="Edit this news item" href="{{ url('admin/news/edit', $item->id) }}">
                        		<span style="color: #002e7a; padding: 0 4px;" class="glyphicon glyphicon-pencil"></span>
                        	</a>
                        	<a class="nohover cursor" title="Delete this news item" data-newsid='{{$item->id}}' data-toggle='modal' data-target='#verifyDelete' id='modalButton'>
                        		<span style="color: #871600; padding: 0 4px;" class="glyphicon glyphicon-trash"></span>
                        	</a>
                        </td>
       				</tr>
       			@endforeach
            	</tbody>
			</table>
		</div>
	</div>
</div>


<script>
  
    //Modals
    $('#modalButton').click( function (e) {
     e.preventDefault();
    });
    $("a[data-toggle=modal]").click(function() 
    {   
        var newsid = $(this).data('newsid');
        $('#newsidDelete').val(newsid);
        
    });

</script>

@stop