@extends('layouts.admin')

@section('title', 'Update News Item')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>Update News Item - {{$news->title}}</h4>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" action="{{ url('admin/news/edit', $news->id) }}" method="POST">
			{{ csrf_field() }}
			@if ($errors->all())
            	<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{ $errors->first() }}</div>
            @endif
            @if(Session::has('success'))
            	<div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> {{ Session::get('success') }}</div>
			@endif
			<div class="col-sm-6">
				<div class="form-group">
					<label for="title" class="control-label">Title:</label>
					<input type="text" name="title" id="title" placeholder="Title" value="{{$news->title}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="body" class="control-label">Content</label>
					<textarea name="body" class="form-control" rows="10">{{$news->body}}</textarea>
				</div>
				<div class="form-group">
					<button class="btn btn-primary" type="submit">Submit</button>
				</div>
			</div>
		</form>

			<div class="col-sm-5 col-sm-offset-1">
				<blockquote>
					<p>No HTML will be allowed in the body of your news post. Line breaks will be preserved.</p>
				</blockquote>
			</div>	
	</div>
</div>
@stop